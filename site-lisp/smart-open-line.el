;;; smart-open-line.el --- Open line in vi-like style -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;;; Commentary:
;;
;;  Dragging region and parens to opened line stuff.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; This file is not part of Emacs
;;
;; Author: Yuriy Pitomets < pitometsu@gmail.com >
;; URL: https://bitbucket.org/Kamiel/.emacs.d/
;; Version: 0.1
;; Package-version: 0.1
;; Keywords: editing
;; Package-Requires: ((smartparens "0") (dash "2.6.0"))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Copyright 2013-2015 © Yuriy Pitomets
;;
;; "THE BEER-WARE LICENSE" (Revision 42):
;; < pitometsu@gmail.com > wrote this file.
;; As long as you retain this notice you can do whatever you want with this
;; stuff. If we meet some day, and you think this stuff is worth it, you can
;; buy me a beer in return.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; To Do:
;;
;; * DONE empty spaces at the begin/end of line
;; * TODO transfer selection only (or some characters from it)
;; * DONE transfer characters with negative ARG from other side of string
;; * TODO in some way get not from begin/end of line, but from cursor position
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Commentary:
;;
;; Samples: | region displayed as a subscript; █ - a point place
;; --------
;;  (
;;   (func-name `(some-long-█string)) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (func-name `(ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█) "some-text" ;; comment (with-parens)
;;   )
;;
;; smart-open-line-backward | like M-m C-o (with negative ARG like C-e C-o)
;; ---------------
;; 1. M-x smart-open-line-backward
;;  (
;;   █
;;   (func-name `(some-long-string)) "some-text" ;; comment (with-parens)
;;   )
;;
;;  ( ;; region displayed as subscript
;;   ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█
;;   (func-name `() "some-text" ;; comment (with-parens)
;;   )
;;
;; 2. C-u M-x smart-open-line-backward ; or C-1 M-x smart-open-line-backward
;;  (
;;   (█
;;    func-name `(some-long-string)) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█
;;    func-name `() "some-text" ;; comment (with-parens)
;;   )
;;
;; 3. C-u C-u M-x smart-open-line-backward ; or C-2 M-x smart-open-line-backward
;;  (
;;   (func-name `(█
;;                some-long-string)) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (func-name `(ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█
;;                some-long-string)) "some-text" ;; comment (with-parens)
;;   )
;;
;; 4. C-u - M-x smart-open-line-backward
;;  (
;;   (func-name `(some-long-string)) "some-text"█ ;; comment (with-parens)
;;
;;   )
;;
;;  (
;;   (func-name `() "some-text"ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█ ;; comment (with-parens)
;;
;;   )
;;
;; 5. C-u - 1 M-x smart-open-line-backward
;;  (
;;   (func-name `(some-long-string)█
;;                ) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (func-name `(ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█
;;                ) "some-text" ;; comment (with-parens)
;;   )
;;
;; 6. C-u - 2 M-x smart-open-line-backward
;;  (
;;   (func-name `(some-long-string█
;;                )) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (func-name `(ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█
;;                ) "some-text" ;; comment (with-parens)
;;   )
;;
;; smart-open-line-forward | like C-e RET (with negative ARG like M-m RET)
;; --------------
;; 1. M-x smart-open-line-forward
;;  (
;;   (func-name `(some-long-string)) "some-text" ;; comment (with-parens)
;;   █
;;   )
;;
;;  (
;;   (func-name `() "some-text" ;; comment (with-parens)
;;   ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█
;;   )
;;
;; 2. C-u M-x smart-open-line-forward ; or C-1 M-x smart-open-line-forward
;;  (
;;   (func-name `(some-long-string)
;;   █) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (func-name `(
;;                ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█) "some-text" ;; comment (with-parens)
;;   )
;;
;; 3. C-u C-u M-x smart-open-line-forward ; or C-2 M-x smart-open-line-forward
;;  (
;;   (func-name `(some-long-string
;;               █)) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (func-name `(
;;                ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█) "some-text" ;; comment (with-parens)
;;   )
;;
;; 4. C-u - M-x smart-open-line-forward
;;  (
;;
;;   █(func-name `(some-long-string)) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;
;;   ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█(func-name `() "some-text" ;; comment (with-parens)
;;   )
;;
;; 5. C-u - 1 M-x smart-open-line-forward
;;  (
;;   (func-name
;;   █`(some-long-string)) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (func-name
;;    ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█`() "some-text" ;; comment (with-parens)
;;   )
;;
;; 6. C-u - 2 M-x smart-open-line-forward
;;  (
;;   (func-name `(
;;               █some-long-string)) "some-text" ;; comment (with-parens)
;;   )
;;
;;  (
;;   (func-name
;;    ₛₒₘₑ₋ₗₒₙᵧ₋ₛₜᵣᵢₙᵧ₎█`() "some-text" ;; comment (with-parens)
;;   )
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:


;;;============================================================================
;;;
;;;  Requirements
;;;
;;;============================================================================

(defconst smart-open-line/version "0.1")

(require 'smartparens)
(require 'dash)

;; (defalias 'then 'progn)
;; (defalias 'else 'progn)
;; (defvaralias 'otherwise 't)

;; (font-lock-add-keywords 'emacs-lisp-mode
;;                         '(("(\\<\\(then\\)\\>" (1 'font-lock-keyword-face))
;; 						  ("(\\<\\(else\\)\\>" (1 'font-lock-keyword-face))
;; 						  ("(\\<\\(with-current-line\\)\\>"
;; 						   (1 'font-lock-keyword-face))
;; 						  ("(\\<\\(otherwise\\)\\>"
;; 						   (1 'font-lock-constant-face))))

;; (let ((indentation (get 'progn 'lisp-indent-function)))
;;   (put 'then 'lisp-indent-function indentation)
;;   (put 'else 'lisp-indent-function indentation)
;;   (put 'with-current-line 'lisp-indent-function indentation))


;;;============================================================================
;;;
;;;  Minor Mode Definition / Faces / Key Bindings
;;;
;;;============================================================================

(defgroup smart-open-line nil
  "Smart open line minor mode."
  :version "0.1"
  :group 'editor
  :prefix "sol-")

(defvar smart-open-line-mode-map (make-keymap)
  "Keymap for `smart-open-line-mode'.")

(define-minor-mode smart-open-line-mode
  "Toggle Smart open line mode.
See the command `smart-open-line'."
  :init-value nil
  :lighter " Sol"
  :keymap 'smart-open-line-mode-map
  :group 'smart-open-line)

(define-globalized-minor-mode global-smart-open-line-mode
  smart-open-line-mode
  (lambda () (smart-open-line-mode 1)))

(defcustom smart-open-line-indent t
  "If t, indent current and opened lines.
Modify behavior of smart-open-line-* functions to cause them to auto-indent."
  :type 'boolean
  :group 'smart-open-line)


;;;===========================================================================
;;;
;;;  Public Functions / Commands
;;;
;;;===========================================================================

;;;###autoload
(defun end-of-line-no-comments ()
  "Move point to end of current line, but ignore comments at the end of line."
  (interactive)
  (beginning-of-line)
  (when (comment-search-forward (line-end-position) t)
    (goto-char (match-beginning 0))
    (skip-syntax-backward " " (line-beginning-position))))

;;;###autoload
(defun smart-open-line-backward (&optional ARG)
  "Open line backward." ; TODO: indentation and drag region
  (interactive "*P")
  (smart-open-line nil ARG))

;;;###autoload
(defun smart-open-line-forward (&optional ARG)
  "Open line forward."
  (interactive "*P")
  (smart-open-line t ARG))


;;;===========================================================================
;;;
;;;  Private Functions
;;;
;;;===========================================================================

(defun smart-open-line (DIRECTION ARG)
  "Open line according to DIRECTION.
If DIRECTION is t, open line forward. Otherwise open it backward."
  (pcase-let ((`(,along . ,parens) (smart-open-line-arguments-to-cons ARG)))
	(if DIRECTION
		(if along
			(smart-open-line-forward-along parens)
          (smart-open-line-forward-apart parens))
	  (if along
		  (smart-open-line-backward-along parens)
		(smart-open-line-backward-apart parens)))))

(defun smart-open-line-forward-along (NUM)
  (if (zerop NUM)
      (then
        (end-of-line)
        (newline))
    (else
      (end-of-line-no-comments)
      (current-line-down-sexp (- NUM))
      (newline))))

(defun smart-open-line-forward-apart (NUM)
  (back-to-indentation)
  (current-line-down-sexp NUM)
  (newline))

(defun smart-open-line-backward-along (NUM)
  (back-to-indentation)
  (current-line-down-sexp NUM)
  (open-line))

(defun smart-open-line-backward-apart (NUM)
  (if (zepop NUM)
      (then
        (end-of-line)
        (open-line)
        (end-of-line-no-comments))
    (else
      (end-of-line-no-comments)
      (current-line-down-sexp (- NUM))
      (open-line))))

(defun smart-open-line-indent ()
  (when smart-open-line-indent
    (indent-according-to-mode)))

(defun current-line-down-sexp (NUM)
  (with-current-line
	(sp-down-sexp NUM)))

(defmacro with-current-line (&rest YIELD)
  `(save-restriction
	 (beginning-of-line)
	 (let ((start-point (point)))
	   (forward-line)
	   (narrow-to-region start-point (point))
	   ,@YIELD)))

(defun smart-open-line-arguments-to-cons (ARG)
  (cond
   ((equal ARG '-)
	(cons nil 0))
   ((not ARG)
	(cons t 0))
   ((listp ARG)
	(let ((num (round (log (apply '+ (-filter 'numberp ARG)) 4))))
	  (cons (< num 0) (abs num))))
   ((not (integerp ARG))
	(cons t 0))
   (otherwise
	(cons (< ARG 0) (abs ARG)))))

(provide-me)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; smart-open-line.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
