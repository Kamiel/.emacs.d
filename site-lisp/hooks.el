;;; hooks.el --- This file contain hooks for different modes -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;;; Commentary:
;;
;; Copyright (C) 2012-2015  Yuriy V. Pitomets < pitometsu@gmail.com >
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:

(add-hook 'after-init-hook 'ido-mode-off)

;; (defun electric-indent-mode-off ()
;;   (electric-indent-local-mode 0))

;; (add-hook 'after-change-major-mode-hook 'electric-indent-mode-off)

(defun init-prog-mode-hook ()
  (turn-on-eldoc-mode)
  (imenu-add-menubar-index)
  (display-line-numbers-mode 1)
  ;; (linum-mode)
  ;; (electric-indent-local-mode 0)
  ;; (setq electric-indent-inhibit t)
  (hl-line-mode 1)
  (turn-on-auto-fill)
  (hs-minor-mode 1)
  (outline-minor-mode 1)
  (turn-on-visual-line-mode)
  (make-local-variable 'write-file-functions)
  (local-set-key (kbd "C-c | s") 'w3mext-hacker-search) ;; move to separate mode depend on w3m
  (add-to-list 'write-file-functions 'delete-trailing-whitespace))

(add-hook 'prog-mode-hook 'init-prog-mode-hook)
(add-hook 'nxml-mode-hook 'init-prog-mode-hook)

(add-hook 'comint-mode 'turn-on-eldoc-mode)

(add-hook 'ruby-mode-hook 'turn-on-auto-fill)

(defun init-python-mode-hook ()
  (eval-after-load 'adaptive-wrap
    '(setq-local adaptive-wrap-extra-indent 1))
  (eval-after-load 'smart-newline '(smart-newline-mode 0)))

(add-hook 'python-mode-hook 'init-python-mode-hook)

(defun font-lock-dired-mode-hook ()
  (when (fboundp 'font-lock-refresh-defaults)
    (font-lock-refresh-defaults))) ;; hm.. Why it here?

;; (add-hook 'dired-mode-hook 'dired-extra-startup) ; TODO: check is it necessary
(add-hook 'dired-mode-hook 'font-lock-dired-mode-hook)
(add-hook 'dired-mode-hook 'hl-line-mode)

(defun init-emacs-lisp-mode-hook ()
  (setq-local fill-column 90)
  (local-unset-key (kbd "C-M-x"))
  (define-key checkdoc-minor-mode-map (kbd "C-M-x") nil))

(add-hook 'emacs-lisp-mode-hook 'init-emacs-lisp-mode-hook)
(add-hook 'emacs-lisp-mode-hook 'imenu-elisp-sections)
(add-hook 'emacs-lisp-mode-hook 'checkdoc-minor-mode) ;; (checkdoc-minor-mode 1)

(defun init-objc-mode-hook ()
  (c-toggle-auto-hungry-state -1)
  (make-local-variable 'auto-indent-assign-indent-level)
  ;; (setq auto-indent-assign-indent-level 4) ; TODO: customize cc-mode
  ;; (setq-local adaptive-wrap-extra-indent 4)
  (ac-cc-mode-setup)
  (setq-local ac-sources (append '(ac-source-clang ac-source-yasnippet) ac-sources))
  (define-key objc-mode-map (kbd "C-c w") 'xcdoc:ask-search)
  (define-key objc-mode-map (kbd "C-c C-h") 'c-hungry-delete-backwards))

(add-hook 'objc-mode-hook 'init-objc-mode-hook)

(add-hook 'grep-mode-hook 'grep-respect-path-directory)

;; (defun setup-custom-python-shell-interpreter ()
;;   "Fix inferior-python-mode."
;;   (make-local-variable 'python-shell--interpreter python-shell-interpreter)
;;   (make-local-variable 'python-shell--interpreter-args python-shell-interpreter-args))

;; (add-hook 'inferior-python-mode-hook 'setup-custom-python-shell-interpreter)

(eval-after-load 'python
  '(progn
     (defvar python-shell--interpreter python-shell-interpreter)
     (defvar python-shell--interpreter-args python-shell-interpreter-args)))

;; End there
(provide 'hooks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; hooks.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
