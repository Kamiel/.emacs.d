;;; keybindings.el --- This file contain some custom keybindings. See hacks.el also -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;;; Commentary:
;;
;; Copyright (C) 2012-2015  Yuriy V. Pitomets < pitometsu@gmail.com >
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:

;; touchpad horizontal scrolling
;; FIXME: scroll NOT in current buffer, BUT in buffer UNDER mouse pointer
;; (global-set-key (kbd "<mouse-6>") 'scroll-right)
;; (global-set-key (kbd "<mouse-7>") 'scroll-left)

;; more smooth sometimes just jump on same place instead of scrolling :(
;; (global-set-key (kbd "<mouse-7>") '(lambda ()
;;                                      (interactive)
;;                                      (scroll-left 1)))
;; (global-set-key (kbd "<mouse-6>") '(lambda ()
;;                                      (interactive)
;;                                      (scroll-right 1)))

;; better expand
;; (global-set-key (kbd "M-/") 'hippie-expand)
;; better buffer list

(global-set-key (kbd "C-x C-b") 'ibuffer)

(global-set-key (kbd "C-c k") 'kill-this-buffer)
(global-set-key (kbd "ESC ESC k") 'kill-this-buffer)
(global-set-key (kbd "<escape> k") 'kill-this-buffer)

;; windows navigation via Esc+arrows
(global-set-key (kbd "ESC ESC p") 'windmove-up)
(global-set-key (kbd "ESC ESC n") 'windmove-down)
(global-set-key (kbd "ESC ESC b") 'windmove-left)
(global-set-key (kbd "ESC ESC f") 'windmove-right)

(global-set-key (kbd "<escape> p") 'windmove-up)
(global-set-key (kbd "<escape> n") 'windmove-down)
(global-set-key (kbd "<escape> b") 'windmove-left)
(global-set-key (kbd "<escape> f") 'windmove-right)

(global-set-key (kbd "ESC ESC <up>") 'windmove-up)
(global-set-key (kbd "ESC ESC <down>") 'windmove-down)
(global-set-key (kbd "ESC ESC <left>") 'windmove-left)
(global-set-key (kbd "ESC ESC <right>") 'windmove-right)

(global-set-key (kbd "ESC M-<up>") 'windmove-up)
(global-set-key (kbd "ESC M-<down>") 'windmove-down)
(global-set-key (kbd "ESC M-<left>") 'windmove-left)
(global-set-key (kbd "ESC M-<right>") 'windmove-right)

(global-set-key (kbd "<escape> <up>") 'windmove-up)
(global-set-key (kbd "<escape> <down>") 'windmove-down)
(global-set-key (kbd "<escape> <left>") 'windmove-left)
(global-set-key (kbd "<escape> <right>") 'windmove-right)

;; переключение на следующий/предыдущий буффер
(global-set-key (kbd "C-x M-p") 'previous-buffer)
(global-set-key (kbd "C-s-<back>") 'previous-buffer)
(global-set-key (kbd "C-s-<left>") 'previous-buffer)
(global-set-key (kbd "C-x M-n") 'next-buffer)
(global-set-key (kbd "C-s-<forward>") 'next-buffer)
(global-set-key (kbd "C-s-<right>") 'next-buffer)
(global-set-key (kbd "s-M-{")   'previous-buffer)
(global-set-key (kbd "s-M-}")   'next-buffer)

;; scroll text by one line
(global-set-key (kbd "C-s-S-M-<down>") 'scroll-one-down)
(global-set-key (kbd "C-s-S-M-<up>")   'scroll-one-up)

;; search
(global-set-key (kbd "M-s-r") 'isearch-forward-regexp)
(global-set-key (kbd "M-s-F") 'anzu-query-replace-regexp)
(global-set-key (kbd "M-s-f") 'anzu-query-replace)

;; replace
(global-set-key (kbd "C-c h") 'replace-regexp)
(global-set-key (kbd "C-c c") 'replace-string)

;; quickrun
(global-set-key (kbd "C-M-|")    'quickrun-region)
(global-set-key (kbd "<escape> C-M-|") 'quickrun-replace-region)

;; thesaurus
(global-set-key (kbd "C-c u") 'thesaurus-choose-synonym-and-replace)

;; transpose
(global-set-key (kbd "C-x M-t") 'transpose-paragraphs)

;; duplication line
(global-set-key (kbd "C-c D")	'duplicate-line-or-region-above)
(global-set-key (kbd "S-C-M-d") 'duplicate-line-or-region-above)
(global-set-key (kbd "s-D")		'duplicate-line-or-region-above)
(global-set-key (kbd "C-c d")	'duplicate-line-or-region-below)
(global-set-key (kbd "S-C-d")	'duplicate-line-or-region-below)
(global-set-key (kbd "s-d")		'duplicate-line-or-region-below)

;; deleting blank lines
(global-set-key (kbd "M-RET") 'delete-blank-lines)

;; whitespace next to cursor
(global-set-key (kbd "S-SPC") 'insert-postfix-whitespace)

;; cycle spacing
(global-set-key (kbd "M-S-SPC") 'cycle-spacing)

;; winner mode
(global-set-key (kbd "ESC ESC d") 'winner-undo)
(global-set-key (kbd "ESC ESC u") 'winner-redo)

(global-set-key (kbd "<escape> d") 'winner-undo)
(global-set-key (kbd "<escape> u") 'winner-redo)

(global-set-key (kbd "C-c <left>") 'winner-undo)
(global-set-key (kbd "C-c <right>") 'winner-redo)

;; deleting
(global-set-key (kbd "M-DEL")                  'backward-kill-word)
(global-set-key (kbd "M-h")                    'backward-kill-word)
(global-set-key (kbd "C-h")                    'backward-delete-char)
(global-set-key (kbd "M-#")                    'kill-whole-line)
(global-set-key (kbd "C-s-DEL")                'kill-whole-line)
(global-set-key (kbd "C-;")                    'kill-start-of-line)
(global-set-key (kbd "s-DEL")                  'kill-start-of-line)
(global-set-key (kbd "s-<backspace>")          'kill-start-of-line)
(global-set-key (kbd "<backtab>")              'remove-indentation)
(global-set-key (kbd "C-M-<backspace>")        'backward-kill-sexp)
(global-set-key (kbd "<escape> <C-backspace>") 'kill-sexp)
(global-set-key (kbd "ESC C-k")                'kill-sexp)
(global-set-key (kbd "<escape> C-k")           'kill-sexp)
(global-set-key (kbd "s-M-d")                  'kill-sexp)
(global-set-key (kbd "S-s-M-d")                'backward-kill-sexp)

(define-key minibuffer-local-map (kbd "C-h") 'backward-delete-char)

;; ffat
(global-set-key (kbd "C-c o") 'find-file-at-point)
(global-set-key (kbd "C-c C-o") 'find-file-at-point)

;; navigation
(global-set-key (kbd "M-g o")   'occur)
(global-set-key (kbd "M-S-o")   'occur)
(global-set-key (kbd "M-s s")   'occur)
(global-set-key (kbd "M-g s")   'occur)
(global-set-key (kbd "C-M-o")   'multi-occur)
(global-set-key (kbd "C-M-S-o") 'multi-occur)
(global-set-key (kbd "M-s m")   'multi-occur)
(global-set-key (kbd "M-g m")   'multi-occur)

;; paragraph
(global-set-key (kbd "M-p")      'backward-paragraph)
(global-set-key (kbd "M-n")      'forward-paragraph)
(global-set-key (kbd "M-<up>")   'backward-paragraph)
(global-set-key (kbd "M-<down>") 'forward-paragraph)

;; subWords
(global-set-key (kbd "C-<right>")      'subword-forward)
(global-set-key (kbd "C-<left>")       'subword-backward)
(global-set-key (kbd "C-DEL")          'subword-backward-kill)
(global-set-key (kbd "C-M-DEL")        'subword-kill)
(global-set-key (kbd "C-<deletechar>") 'subword-kill)

;; downcase-word
(global-unset-key (kbd "M-l"))
(global-set-key (kbd "M-l") 'downcase-word)

;; windows managment
(global-set-key (kbd "s-0")   'delete-window)
(global-set-key (kbd "s-1")   'delete-other-windows)
(global-set-key (kbd "s-2")   'split-window-below)
(global-set-key (kbd "s-3")   'split-window-right)
(global-set-key (kbd "s-9")   'split-window-right)
(global-set-key (kbd "C-x 9") 'split-window-right)
(global-set-key (kbd "s-4")   'kill-buffer-and-window)

;; split & resize
(global-set-key (kbd "s-*")   'delete-window)
(global-set-key (kbd "s-M-w") 'delete-other-windows)
(global-set-key (kbd "s-(")   'delete-other-windows)
(global-set-key (kbd "s-)")   'split-window-below)
(global-set-key (kbd "s-}")   'split-window-right)
(global-set-key (kbd "s-|")   'split-window-horizontally)
(global-set-key (kbd "s-_")   'split-window-vertically)
(global-set-key (kbd "C-x {") 'shrink-window-horizontally)
(global-set-key (kbd "C-x |") 'split-window-horizontally)
(global-set-key (kbd "C-x _") 'split-window-vertically)
(global-set-key (kbd "C-x {") 'shrink-window-horizontally)
(global-set-key (kbd "C-x }") 'enlarge-window-horizontally)
(global-set-key (kbd "C-x ^") 'enlarge-window)

;; show recent
(global-set-key (kbd "C-x M-f") 'recentf-open-files)

;; global other-window
(global-set-key (kbd "C-x O") 'previous-multiframe-window)


;; mac os x style
(global-set-key (kbd "s-S")     'write-file)
(global-set-key (kbd "s-s")     'save-buffer)
(global-set-key (kbd "s-i")     'dired-jump-other-window)
(global-set-key (kbd "s-l")     'goto-line)
(global-set-key (kbd "s-q")     'save-buffers-kill-emacs)
(global-set-key (kbd "s-x")     'kill-region)
(global-set-key (kbd "s-c")     'kill-ring-save)
(global-set-key (kbd "s-v")     'yank)
(global-set-key (kbd "S-s-v")   'yank-pop-forward)
(global-set-key (kbd "S-C-s-v") 'yank-pop-backward)
(global-set-key (kbd "C-s-v")   'yank-pop-backward)
(global-set-key (kbd "s-a")     'mark-whole-buffer)
(global-set-key (kbd "s-f")     'isearch-forward)
(global-set-key (kbd "s-M-f")   'occur)
(global-set-key (kbd "s-g")     'isearch-repeat-forward)
(global-set-key (kbd "s-G")     'isearch-repeat-backward)
;; (global-set-key (kbd "s-m")     'iconify-frame)
(global-set-key (kbd "s-`")     'go-next-frame)
(global-set-key (kbd "s-~")     'go-previous-frame)
(global-set-key (kbd "s-n")     'make-frame-command)
(global-set-key (kbd "s-w")     'delete-frame)
(global-set-key (kbd "s-?")     'info)
(global-set-key (kbd "s-.")     'keyboard-quit)
(global-unset-key (kbd "s-<left>"))
(global-unset-key (kbd "s-<right>"))
(global-unset-key (kbd "s-<up>"))
(global-unset-key (kbd "s-<down>"))
(global-set-key (kbd "s-<left>")  'move-beginning-of-line)
(global-set-key (kbd "s-<right>") 'move-end-of-line)
(global-set-key (kbd "s-<up>")    'beginning-of-buffer)
(global-set-key (kbd "s-<down>")  'end-of-buffer)
(global-set-key (kbd "s-o")       'find-file)
(global-set-key (kbd "s-g")       'isearch-forward)
(global-set-key (kbd "s-G")       'isearch-backward)
(global-set-key (kbd "s-f")       'occur)
(global-set-key (kbd "s-u")       'revert-buffer)

;; fix home/end for macOS
(global-set-key (kbd "<home>") 'beginning-of-visual-line)
(global-set-key (kbd "<end>")  'end-of-visual-line)
;; TODO: C-home/end to horizontal-scroll-begin/end

;; kill window with buffer
(global-unset-key (kbd "s-W"))
(global-set-key (kbd "s-W")  ; 【⌘⇧W】
                'kill-buffer-and-window)

;; recent files
(global-set-key (kbd "C-x C-r") 'recentf-open-files)
(global-set-key (kbd "s-r")     'recentf-open-files)

;; one-key macros record
(global-unset-key (kbd "s-M"))
(global-set-key (kbd "s-M")   'call-last-kbd-macro)
(global-set-key (kbd "M-s-m") 'toggle-kbd-macro-recording-on)

;; help
(define-key help-map (kbd "z") 'view-lossage)

;; aligning code
(global-set-key (kbd "C-x a r") 'align-regexp)
(global-set-key (kbd "C-x M-r") 'align-regexp)

;; eval region
(global-set-key (kbd "C-x M-e") 'eval-region-and-unmark)

;; capitalize region
(global-set-key (kbd "C-x M-c") 'capitalize-region)

;; smart open line
(global-set-key (kbd "S-RET")             'open-next-line)
(global-set-key (kbd "S-<return>")        'open-next-line)
(global-set-key (kbd "C-<return>")        'open-previous-line)
(global-set-key (kbd "C-RET")             'open-previous-line)
(global-set-key (kbd "S-C-RET")           'open-next-line)
(global-set-key (kbd "S-C-<return>")      'open-next-line)
(global-set-key (kbd "S-s-RET")           'open-previous-line)
(global-set-key (kbd "S-s-<return>")      'open-previous-line)
(global-set-key (kbd "s-RET")             'open-next-line)
(global-set-key (kbd "s-<return>")        'open-next-line)
(global-set-key (kbd "ESC <return>")      'open-new-line)
(global-set-key (kbd "<escape> <return>") 'open-new-line)

;; comment-region
(global-set-key (kbd "s-/") 'comment-or-uncomment-region-or-line)
(global-set-key (kbd "s-#") 'comment-or-uncomment-region-or-line)
(global-set-key (kbd "s-;") 'comment-or-uncomment-region-or-line)

;; text scale
(global-set-key (kbd "s-=")
                '(lambda () (interactive)
                   (global-text-scale-adjust (- text-scale-mode-amount))
                   (global-text-scale-mode -1)))
(global-set-key (kbd "s-+")
                '(lambda () (interactive) (global-text-scale-adjust 1)))
(global-set-key (kbd "s--")
                '(lambda () (interactive) (global-text-scale-adjust -1)))

;; kill buffer
(global-set-key (kbd "s-k") 'kill-buffer)

;; kill buffer with frame
(global-set-key (kbd "s-K") 'server-edit)

;; ergonomic M-x
(global-set-key (kbd "C-c C-m") 'execute-extended-command)
(global-set-key (kbd "s-e")     'execute-extended-command)

;; grep
(global-set-key (kbd "C-c C-r") 'rgrep)
(global-set-key (kbd "C-c r")	'rgrep)
(global-set-key (kbd "M-g r")	'rgrep)

;; aligning code
(global-set-key (kbd "C-x a r") 'align-regexp)
(global-set-key (kbd "C-x M-r") 'align-regexp)

;; marks
(global-set-key (kbd "C-`") 'push-mark-no-activate)
(global-set-key (kbd "M-`") 'jump-to-mark)

;; el-get
(global-set-key (kbd "C-c e l") 'el-get-list-packages)
(global-set-key (kbd "C-c e i") 'el-get-install)
(global-set-key (kbd "C-c e r") 'el-get-reinstall)
(global-set-key (kbd "C-c e d") 'el-get-remove)
(global-set-key (kbd "C-c e h") 'el-get-describe)
(global-set-key (kbd "C-c e s") 'el-get-sync)
(global-set-key (kbd "C-c e u") 'el-get-update)
(global-set-key (kbd "C-c e U") 'el-get-self-update)
(global-set-key (kbd "C-c e C-i") 'el-get-invalidate-autoloads)

;; show buffer file name
(define-key help-map (kbd "o") 'expand-buffer-file-name)

;; org mode
(global-set-key (kbd "ESC ESC l") 'org-store-link)
(global-set-key (kbd "ESC ESC c") 'org-capture)
(global-set-key (kbd "ESC ESC a") 'org-agenda)
(global-set-key (kbd "ESC ESC o") 'org-switchb)

(global-set-key (kbd "<escape> l") 'org-store-link)
(global-set-key (kbd "<escape> c") 'org-capture)
(global-set-key (kbd "<escape> a") 'org-agenda)
(global-set-key (kbd "<escape> o") 'org-switchb)

;; end of line no comments
(global-set-key (kbd "C-c C-e") 'end-of-line-no-comments)

;; browse-apropos-url
(global-set-key (kbd "<f5>") 'browse-apropos-url)
(global-set-key (kbd "C-<f5>") 'rgr/google-search-auto)
(global-set-key (kbd "M-<f5>") 'rgr/google-search-prompt)

;; command for cat
(global-set-key (kbd "C-M-s-:") 'cat-command)
(global-set-key (kbd "C-M-s-Z") 'cat-command)

;; Scroll PDF in other buffer
(global-set-key (kbd "<f8>") 'wenshan-other-docview-buffer-scroll-down)
(global-set-key (kbd "<f9>") 'wenshan-other-docview-buffer-scroll-up)

;; Emacs equivalent to VIM's `%`
(define-key minibuffer-local-map (kbd "C-c n") 'insert-filename-or-buffername)

;; eshell behavior
(add-hook 'eshell-mode-hook ;; for some reason this needs to be a hook
          '(lambda () (define-key eshell-mode-map (kbd "C-a") 'eshell-bol)))

;; ediff
(eval-after-load 'ediff-mode
  '(progn
     (define-key ediff-mode-map (kbd "<down>") 'ediff-next-difference)
     (define-key ediff-mode-map (kbd "<up>") 'ediff-previous-difference)))

;; Esc in TTY
(global-unset-key [?\e ?\e ?\e])
(global-set-key [escape escape escape] 'keyboard-escape-quit)

;; Turn on horizontal scrolling with mouse wheel
(global-set-key (kbd "C-x >") 'hscroll-left)
(global-set-key (kbd "C-x <") 'hscroll-right)

(global-set-key (kbd "C-<next>") 'hscroll-left)
(global-set-key (kbd "C-<prior>") 'hscroll-right)

(global-set-key [wheel-right] 'horizontal-mscroll-left)
(global-set-key [wheel-left] 'horizontal-mscroll-right)

(global-set-key (kbd "<mouse-6>") 'horizontal-mscroll-right)
(global-set-key (kbd "<mouse-7>") 'horizontal-mscroll-left)

;; zap up to char M-z
(global-set-key [zap-to-char] 'zap-up-to-char)

;; End there
(provide 'keybindings)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; keybindings.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
