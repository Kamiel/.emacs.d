;;; hacks.el --- This file contain some hacks that still haven't become a plugins -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;;; Commentary:
;;
;; Copyright (C) 2012-2015  Yuriy V. Pitomets < pitometsu@gmail.com >
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:

;; Fix emoji on Ubuntu
(add-to-list 'face-ignored-fonts "Noto Color Emoji")

(defun ido-mode-off ()
  "Turn off option `ido-mode'."
  (ido-mode 0))

;; All “yes” or “no” questions are aliased to “y” or “n”
(fset 'yes-or-no-p 'y-or-n-p)

;; Do not display default startup message
(fset 'display-startup-echo-area-message 'ignore)

;; Should be able to kill processes without asking which is achieved in the second expression
(setq kill-buffer-query-functions
      (remq 'process-kill-buffer-query-function
            kill-buffer-query-functions))

;; nice little alternative visual bell; Miles Bader <miles /at/ gnu.org>
(defcustom echo-area-bell-string "♪" ;"*DING* " ;"♪"
  "Message displayed in mode-line by `echo-area-bell' function."
  :group 'user)
(defcustom echo-area-bell-delay 0.1
  "Number of seconds `echo-area-bell' displays its message."
  :group 'user)
;; internal variables
(defvar echo-area-bell-cached-string nil)
(defvar echo-area-bell-propertized-string nil)
(defun echo-area-bell ()
  "Briefly display a highlighted message in the echo-area.
     The string displayed is the value of `echo-area-bell-string',
     with a red background; the background highlighting extends to the
     right margin.  The string is displayed for `echo-area-bell-delay'
     seconds.
     This function is intended to be used as a value of `ring-bell-function'."
  (unless (equal echo-area-bell-string echo-area-bell-cached-string)
    (setq echo-area-bell-propertized-string
          (propertize
           (concat
            (propertize
             "beep"
             'display
             `(space :align-to (- right ,(+ 2 (length echo-area-bell-string)))))
            echo-area-bell-string)
           'face '(:background "#e8e8e8")))
    (setq echo-area-bell-cached-string echo-area-bell-string))
  (message echo-area-bell-propertized-string)
  (sit-for echo-area-bell-delay)
  (message ""))
(setq-default ring-bell-function 'echo-area-bell)

;; 8bit Meta
;; (set-input-mode t nil t 7)

;; use the predefined fontset and custom for cyrillic: пример кириллицы
(when (fboundp 'set-fontset-font)
  (set-fontset-font t
                    'unicode
                    '("Source Code Pro" . "iso10646-1"))
  (set-fontset-font t
                    'korean-ksc5601
                    '("Source Has Sans KR" . "iso10646-1"))
  (set-fontset-font t
                    'chinese-big5-1
                    '("Source Has Sans TW" . "iso10646-1"))
  (set-fontset-font t
                    'chinese-big5-2
                    '("Source Has Sans TW" . "iso10646-1"))
  (set-fontset-font t
                    'chinese-gb2312
                    '("Source Has Sans CN" . "iso10646-1"))
  (set-fontset-font t
                    'japanese-jisx0208
                    '("Source Has Sans JP" . "iso10646-1"))
  (set-fontset-font t
                    'japanese-jisx0212
                    '("Source Has Sans JP" . "iso10646-1"))
  (set-fontset-font t
                    'mule-unicode-0100-24ff
                    (font-spec :inherit nil))
  (set-fontset-font t
                    'cyrillic-iso8859-5
                    (font-spec :inherit nil))
  (set-fontset-font t
                    'greek-iso8859-7
                    (font-spec :inherit nil))
  (set-fontset-font t
                    'ascii
                    (font-spec :inherit nil))
  (set-fontset-font t
                    'symbol
                    (font-spec :inherit nil)))

;; чтобы не πодтверждать закрытие буфера
(defun kill-current-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))

;; imenu for emacs-lisp customizations
(defun imenu-elisp-sections ()
  (setq imenu-prev-index-position-function nil)
  (add-to-list 'imenu-generic-expression '("Sections" "^\n+;+ \\(.+\\)$" 1) t))

;; duplication line
(defun duplicate-line-or-region-above (&optional reverse)
  "Duplicate current line or region above.
By default, duplicate current line above.
If mark is activate, duplicate region lines above.
Default duplicate above, unless option REVERSE is non-nil."
  (interactive)
  (let ((origianl-column (current-column))
        duplicate-content)
    (if mark-active
        ;; If mark active.
        (let ((region-start-pos (region-beginning))
              (region-end-pos (region-end)))
          ;; Set duplicate start line position.
          (setq region-start-pos (progn
                                   (goto-char region-start-pos)
                                   (line-beginning-position)))
          ;; Set duplicate end line position.
          (setq region-end-pos (progn
                                 (goto-char region-end-pos)
                                 (line-end-position)))
          ;; Get duplicate content.
          (setq duplicate-content (buffer-substring region-start-pos region-end-pos))
          (if reverse
              ;; Go to next line after duplicate end position.
              (progn
                (goto-char region-end-pos)
                (forward-line +1))
            ;; Otherwise go to duplicate start position.
            (goto-char region-start-pos)))
      ;; Otherwise set duplicate content equal current line.
      (setq duplicate-content (buffer-substring
                               (line-beginning-position)
                               (line-end-position)))
      ;; Just move next line when `reverse' is non-nil.
      (and reverse (forward-line 1))
      ;; Move to beginning of line.
      (beginning-of-line))
    ;; Open one line.
    (open-line 1)
    ;; Insert duplicate content and revert column.
    (insert duplicate-content)
    (move-to-column origianl-column t)))

(defun duplicate-line-or-region-below ()
  "Duplicate current line or region below.
By default, duplicate current line below.
If mark is activate, duplicate region lines below."
  (interactive)
  (duplicate-line-or-region-above t))

(defun duplicate-line-above-comment (&optional reverse)
  "Duplicate current line above, and comment current line."
  (interactive)
  (if reverse
      (duplicate-line-or-region-below)
    (duplicate-line-or-region-above))
  (save-excursion
    (if reverse
        (forward-line -1)
      (forward-line +1))
    (comment-or-uncomment-region+)))

(defun duplicate-line-below-comment ()
  "Duplicate current line below, and comment current line."
  (interactive)
  (duplicate-line-above-comment t))

;; one-key macros record
(defun toggle-kbd-macro-recording-on ()
  "One-key keyboard macros: turn recording on."
  (interactive)
  (define-key
    global-map
    (this-command-keys)
    'toggle-kbd-macro-recording-off)
  (start-kbd-macro nil))

(defun toggle-kbd-macro-recording-off ()
  "One-key keyboard macros: turn recording off."
  (interactive)
  (define-key
    global-map
    (this-command-keys)
    'toggle-kbd-macro-recording-on)
  (end-kbd-macro))

;; scroll text by one line
(defun scroll-one-down ()
  (interactive)
  (scroll-down 1)
  (previous-line))

(defun scroll-one-up ()
  (interactive)
  (scroll-up 1)
  (next-line))

(defun line-up ()
  (interactive)
  (scroll-up 1))

(defun line-down ()
  (interactive)
  (scroll-down 1))

;; kill whole line
(defun kill-start-of-line ()
  "kill from point to start of line."
  (interactive)
  (kill-line 0))

;; remove indentation
(defun remove-indentation ()
  "Remove indentation of current line."
  (interactive)
  (save-excursion
    (back-to-indentation)
    (kill-start-of-line)))

;; eval region
(defun eval-region-and-unmark (START END &optional PRINTFLAG READ-FUNCTION)
  "Interactive execute the region as Lisp code and unmark this region.
See `eval-region'."
  (interactive "r")
  (eval-region START END PRINTFLAG READ-FUNCTION)
  (push-mark))


;; global text scale mode
(define-globalized-minor-mode global-text-scale-mode
  text-scale-mode
  (lambda () (text-scale-mode 1)))

(defun global-text-scale-adjust (inc) (interactive)
       (text-scale-set 1)
       (kill-local-variable 'text-scale-mode-amount)
       (setq-default text-scale-mode-amount (+ text-scale-mode-amount inc))
       (global-text-scale-mode 1))



;; Dim parentheses
(defface paren-face
  '((t
     (:foreground "grey35" :weight bold)))
  "Face used to dim parentheses."
   :group 'basic-faces)

(defface lisp-paren-face
  '((t
     (:foreground "grey55" :weight bold)))
  "Face used to dim parentheses."
   :group 'basic-faces)


(defface ruby-paren-face
  '((t
     (:foreground "DimGray" :weight bold)))
  "Face used to dim parentheses."
  :group 'basic-faces)

(defface eol-face
  '((t
     (:foreground "#CBCBCB" :weight bold)))
  "Face used to dim EOL symbol. See `fill-column-indicator'."
  :group 'basic-faces)

(defun dim-paren-lisp (mode)
  "Dim parentheses in lisp-style code.

Add `lisp-paren-face' to MODE."
  (font-lock-add-keywords mode
                          '(("(\\|)" . 'lisp-paren-face))))

(defun dim-paren-c (mode)
  "Dim parentheses in c-style code.

Add `paren-face' to MODE."
  (font-lock-add-keywords mode
                          '(("(\\|)\\|\\[\\|\\]\\|{\\|}" . 'paren-face))))

(defun dim-paren-ruby (mode)
  "Dim parentheses in ruby code.

Add `ruby-paren-face' to MODE."
  (font-lock-add-keywords mode
                          '(("(\\|)\\|\\[\\|\\]\\|{\\|}\\|,\\|\\.\\|=>" . 'ruby-paren-face))))

(defun dim-paren-json (mode)
  "Dim punctuation in json code.

Add `paren-face' to MODE."
  (font-lock-add-keywords mode
                          '(("{\\|}\\|\\,\\|\\.\\|\\:\\|\\;\\|=" . 'paren-face))))

(defun font-lock-json (mode)
  "Dim punctuation in json code.

Add JSON punctuation font lock to MODE."
  (font-lock-add-keywords mode
                          '(("\\(\\,\\|^\\|[ \n\t]\\|{\\)\\(\"[^\" \n\t]*\"\\)[ \n\t]*:\\|=" (2 'font-lock-builtin-face prepend))) t))

(defun dim-eol (mode)
  "Dim EOL symbol in your code.  See `fill-column-indicator'.

Add `eol-face' to MODE."
  (font-lock-add-keywords mode
                          '(("¬" . 'eol-face))))

(dim-paren-lisp 'emacs-lisp-mode)
(dim-paren-c    'objc-mode)
(dim-paren-c    'sclang-mode)
(dim-paren-c    'haskell-mode)
;; (dim-paren-c    'tuareg-mode)
(dim-paren-ruby 'ruby-mode)
(dim-paren-ruby 'enh-ruby-mode)
(dim-paren-json 'js-mode)
(dim-paren-json 'restclient-mode)
(font-lock-json 'js-mode)
(font-lock-json 'restclient-mode)


;; marks
(defun push-mark-no-activate ()
  "Pushes `point' to `mark-ring' and does not activate the region
Equivalent to \\[set-mark-command] when \\[transient-mark-mode] is disabled"
  (interactive)
  (push-mark (point) t nil)
  (message "Pushed mark to ring"))

(defun jump-to-mark ()
  "Jumps to the local mark, respecting the `mark-ring' order.
This is the same as using \\[set-mark-command] with the prefix argument."
  (interactive)
  (set-mark-command 1))

(defun exchange-point-and-mark-no-activate ()
  "Identical to \\[exchange-point-and-mark] but will not activate the region."
  (interactive)
  (exchange-point-and-mark)
  (deactivate-mark nil))

;; insert postfix whitespace
(defun insert-postfix-whitespace ()
  "Just insert SPC symbol next to point."
  (interactive)
  (save-excursion
    (insert ?\s)
    (backward-char)))

;;show buffer file name
(defun expand-buffer-file-name ()
  "Show the full path to the current file in the minibuffer."
  (interactive)
  (let ((file-name (buffer-file-name)))
    (if file-name
        (progn
          (message file-name)
          (kill-new file-name))
      (error "Buffer not visiting a file"))))

;; search at both stackoverflow and google code
(defun w3mext-hacker-search ()
  "search word under cursor in google code search and stackoverflow.com"
  (interactive)
  (require 'w3m)
  (let ((keyword (w3m-url-encode-string (thing-at-point 'symbol))))
    (browse-url-generic (concat "http://code.google.com/codesearch?q=" keyword))
    (browse-url-generic (concat "http://www.google.com.au/search?hl=en&q=" keyword "+site:stackoverflow.com"))))

;; -----------------------------------------------------------------------------
;;;  browse-apropos-url
;;   from http://www.emacswiki.org/emacs/BrowseAproposURL

(setq apropos-url-alist
      '(("^gw?:? +\\(.*\\)" . ;; Google Web
         "http://www.google.com/search?q=\\1")

        ("^g!:? +\\(.*\\)" . ;; Google Lucky
         "http://www.google.com/search?btnI=I%27m+Feeling+Lucky&q=\\1")

        ("^gl:? +\\(.*\\)" .  ;; Google Linux
         "http://www.google.com/linux?q=\\1")

        ("^gi:? +\\(.*\\)" . ;; Google Images
         "http://images.google.com/images?sa=N&tab=wi&q=\\1")

        ("^gg:? +\\(.*\\)" . ;; Google Groups
         "http://groups.google.com/groups?q=\\1")

        ("^gd:? +\\(.*\\)" . ;; Google Directory
         "http://www.google.com/search?&sa=N&cat=gwd/Top&tab=gd&q=\\1")

        ("^gn:? +\\(.*\\)" . ;; Google News
         "http://news.google.com/news?sa=N&tab=dn&q=\\1")

        ;; Google Translate URL
        ("^gt:? +\\(\\w+\\)|? *\\(\\w+\\) +\\(\\w+://.*\\)" .
         "http://translate.google.com/translate?langpair=\\1|\\2&u=\\3")

        ("^gt:? +\\(\\w+\\)|? *\\(\\w+\\) +\\(.*\\)" . ;; Google Translate Text
         "http://translate.google.com/translate_t?langpair=\\1|\\2&text=\\3")

        ("^/\\.$" . ;; Slashdot
         "http://www.slashdot.org")

        ("^/\\.:? +\\(.*\\)" . ;; Slashdot search
         "http://www.osdn.com/osdnsearch.pl?site=Slashdot&query=\\1")

        ("^fm$" . ;; Freshmeat
         "http://www.freshmeat.net")

        ("^ewiki:? +\\(.*\\)" . ;; Emacs Wiki Search
         "http://www.emacswiki.org/cgi-bin/wiki?search=\\1")

        ("^ewiki$" . ;; Emacs Wiki
         "http://www.emacswiki.org")))

(defun browse-apropos-url (text &optional new-window)
  (interactive (browse-url-interactive-arg "Location: "))
  (let ((text (replace-regexp-in-string
               "^ *\\| *$" ""
               (replace-regexp-in-string "[ \t\n]+" " " text))))
    (let ((url (assoc-default
                text apropos-url-alist
                '(lambda (a b) (let () (setq __braplast a) (string-match a b)))
                text)))
      (browse-url (replace-regexp-in-string __braplast url text) new-window))))

;; -----------------------------------------------------------------------------
;;;   Here is some wrapper code to facilitate access to the apropos browse using
;;    a region/word at point or a prompt.

(defun rgr/region-then-thing-at-point()
  "Function to return the currently selected region.
If no region is selected then return the word at the cursor."
  (if mark-active
      (buffer-substring-no-properties (region-beginning)(region-end))
    (progn
      (let ((word (current-word)))
        (if (zerop (length word))
            (setq word "default"))
        word))))

(defun rgr/google (term)
  "Call google search for the specified term.
Do not call if string is zero length."
  (if (not (zerop (length term)))
      (browse-apropos-url (concat "gw: " term))
    (browse-apropos-url  "http://www.google.com ")))

(defun rgr/google-search-auto ()
  (interactive)
  (let ((word (rgr/region-then-thing-at-point)))
    (rgr/google word)))

(defun rgr/google-search-prompt(term)
  "Prompt user to query google search for term.
Term is word at point or the selcted region"
  (interactive (list (unless (eq current-prefix-arg 0)
                       (read-string "Google for word : "
                                    (rgr/region-then-thing-at-point)))))
  (rgr/google term))

;; command for cat
(defun cat-command ()
  "A command for cats."
  (interactive)
  (require 'animate)
  (let ((mouse "
___00
~~/____'>
\"  \"
")
        (h-pos (floor (/ (window-height) 2)))
        (contents (buffer-string))
        (mouse-buffer (generate-new-buffer "*mouse*")))
    (save-excursion
      (switch-to-buffer mouse-buffer)
      (insert contents)
      (setq truncate-lines t)
      (animate-string mouse h-pos 0)
      (dotimes (_ (window-width))
        (sit-for 0.01)
        (dotimes (n 3)
          (goto-line (+ h-pos n 2))
          (move-to-column 0)
          (insert " "))))
    (kill-buffer mouse-buffer)))

;; use Hyper as Super and AltGr as Meta on Linux
(setq x-alt-keysym 'meta)
(setq x-hyper-keysym 'super)
;; setup modifers for Mac
(setq mac-option-modifier 'meta)
(setq mac-command-modifier 'super)

;; check auto-mode-alist for just created buffers also

;; hack that enable major mode for newly created buffer according to `auto-mode-alist'
(defadvice set-buffer-major-mode (after new-buffer-auto-mode activate compile)
  "Select major mode for newly created buffer.

Compare the `buffer-name' the entries in `auto-mode-alist'."
  (with-current-buffer (ad-get-arg 0)
    (if (and (buffer-name) (not buffer-file-name))
        (let ((name (buffer-name)))
          ;; Remove backup-suffixes from file name.
          (setq name (file-name-sans-versions name))
          ;; Do not handle service buffers
          (while (and name (not (string-match "^\\*.+\\*$" name)))
            ;; Find first matching alist entry.
            (let ((mode (if (memq system-type '(windows-nt cygwin))
                            ;; System is case-insensitive.
                            (let ((case-fold-search t))
                              (assoc-default name auto-mode-alist
                                             'string-match))
                          ;; System is case-sensitive.
                          (or
                           ;; First match case-sensitively.
                           (let ((case-fold-search nil))
                             (assoc-default name auto-mode-alist
                                            'string-match))
                           ;; Fallback to case-insensitive match.
                           (and auto-mode-case-fold
                                (let ((case-fold-search t))
                                  (assoc-default name auto-mode-alist
                                                 'string-match)))))))
              (if (and mode
                       (consp mode)
                       (cadr mode))
                  (setq mode (car mode)
                        name (substring name 0 (match-beginning 0)))
                (setq name nil))
              (when mode
                (set-auto-mode-0 mode t))))))))

;; Emacs equivalent to VIM's `%`
(defun insert-filename-or-buffername (&optional arg)
  "If the buffer has a file, insert the base name of that file.
  Otherwise insert the buffer name.  With prefix argument, insert the full file name."
  (interactive "P")
  (let* ((buffer (window-buffer (minibuffer-selected-window)))
         (file-path-maybe (buffer-file-name buffer)))
    (insert (if file-path-maybe
                (if arg
                    file-path-maybe
                  (file-name-nondirectory file-path-maybe))
              (buffer-name buffer)))))

;; alias for usage in eshell
(defalias 'w3m 'w3m-find-file)
(defalias 'open 'find-file)
(defalias 'openo 'find-file-other-window)

;; This makes Eshell’s ‘ls’ file names RET-able. Yay!
(eval-after-load 'em-ls
  '(progn
     (defun ted-eshell-ls-find-file-at-point (point)
       "RET on Eshell's `ls' output to open files."
       (interactive "d")
       (find-file (buffer-substring-no-properties
                   (previous-single-property-change point 'help-echo)
                   (next-single-property-change point 'help-echo))))

     (defun pat-eshell-ls-find-file-at-mouse-click (event)
       "Middle click on Eshell's `ls' output to open files.
 From Patrick Anderson via the wiki."
       (interactive "e")
       (ted-eshell-ls-find-file-at-point (posn-point (event-end event))))

     (let ((map (make-sparse-keymap)))
       (define-key map (kbd "RET")      'ted-eshell-ls-find-file-at-point)
       (define-key map (kbd "<return>") 'ted-eshell-ls-find-file-at-point)
       (define-key map (kbd "<mouse-2>") 'pat-eshell-ls-find-file-at-mouse-click)
       (defvar ted-eshell-ls-keymap map))

     (defadvice eshell-ls-decorated-name (after ted-electrify-ls activate)
       "Eshell's `ls' now lets you click or RET on file names to open them."
       (add-text-properties 0 (length ad-return-value)
                            (list 'help-echo "RET, mouse-2: visit this file"
                                  'mouse-face 'highlight
                                  'keymap ted-eshell-ls-keymap)
                            ad-return-value)
       ad-return-value)))


;; OpenNextLine
;; TODO: remove it when done with smart-open-line

;; TODO: if arg > 0 then back arg else forward arg

;; Behave like vi's o command
(defun open-next-line (arg)
  "Move to the next line and then opens a line.
    See also `newline-and-indent'."
  (interactive "p")
  ;; TODO: use drag-region to drag selection here
  (end-of-line)
  ;; TODO: if arg > 0 then back arg else forward arg
  (open-line arg) ;; must be 1 here
  (next-line 1)
  (when newline-and-indent
    (indent-according-to-mode)))

;; Behave like vi's O command
(defun open-previous-line (arg)
  "Open a new line before the current one.
     See also `newline-and-indent'."
  (interactive "p")
  (beginning-of-line)
  (open-line arg)
  (when newline-and-indent
    (indent-according-to-mode)))

;; Autoindent open-*-lines
(defvar newline-and-indent t
  "Modify the behavior of the open-*-line functions to cause them to autoindent.")

(defun open-new-line (&optional arg)
  "`open-line', then `newline'.
      Use same prefix for both."
  (interactive "p")
  (progn
    (when (/= (point) (line-end-position))
      (open-line arg))
    (call-interactively 'newline arg)))

;; Scroll PDF in other buffer
(defun wenshan-other-docview-buffer-scroll-down ()
  "There are two visible buffers, one for taking notes and one
for displaying PDF, and the focus is on the notes buffer. This
command moves the PDF buffer forward."
  (interactive)
  (other-window 1)
  (doc-view-scroll-up-or-next-page)
  (other-window 1))

(defun wenshan-other-docview-buffer-scroll-up ()
  "There are two visible buffers, one for taking notes and one
for displaying PDF, and the focus is on the notes buffer. This
command moves the PDF buffer backward."
  (interactive)
  (other-window 1)
  (doc-view-scroll-down-or-previous-page)
  (other-window 1))

;; exchange region
(defvar exchange-pending-overlay nil)
(defvar exchange-pending-face 'cursor)

(defadvice keyboard-quit (after exchange-complete activate)
  (delete-overlay exchange-pending-overlay)
  (setq exchange-pending-overlay nil))

(defadvice cua-paste (around exchange-start activate)
  (if (and (called-interactively-p 'interactive)
           (eq last-command 'kill-region))
      (progn
        (setq exchange-pending-overlay
              (make-overlay (point) (1+ (point))))
        (overlay-put exchange-pending-overlay
                     'face exchange-pending-face))
    (progn
      (when exchange-pending-overlay
        (delete-overlay exchange-pending-overlay)
        (setq exchange-pending-overlay nil))
      ad-do-it)))

(defadvice kill-region (around exchange-exec activate)
  (if (not (and (called-interactively-p 'interactive) transient-mark-mode mark-active
                exchange-pending-overlay))
      ad-do-it
    (let* ((str (buffer-substring (region-beginning) (region-end)))
           (pending-pos (overlay-start exchange-pending-overlay))
           (pos (+ (region-beginning)
                   (if (< pending-pos (point)) (length str) 0))))
      (delete-region (region-beginning) (region-end))
      (goto-char (overlay-start exchange-pending-overlay))
      (delete-overlay exchange-pending-overlay)
      (setq exchange-pending-overlay nil)
      (insert str)
      (goto-char pos)
      (setq this-command 'kill-region))))

;; computing Grep Regexp More intelligently

(require 'grep)

;; grep -nH || beagrep -e
(setq my-grep-command "grep -nH pat") ;; should not put it into custom, the custom will be read every time and so the `(let ((grep-command ..' scheme will fail
(defcustom bhj-grep-dir nil "The default directory for grep")
(defvar ajoke--marker-ring (make-ring 32)
  "Ring of markers which are locations from which ajoke was invoked.")

(defun grep-default-command ()
  "Compute the default grep command for C-u M-x grep to offer."
  (let ((tag-default (grep-shell-quote-argument (bhj-grep-tag-default)))
        ;; This a regexp to match single shell arguments.
        ;; Could someone please add comments explaining it?
        (sh-arg-re "\\(\\(?:\"\\(?:\\\\\"\\|[^\"]\\)*\"\\|'[^']+'\\|\\(?:\\\\.\\|[^\"' \\|><\t\n]\\)\\)+\\)")

        (grep-default (or (car grep-history) my-grep-command)))
    ;; In the default command, find the arg that specifies the pattern.
    (when (or (string-match
               (concat "[^ ]+\\s +\\(?:-[^ ]+\\s +\\)*"
                       sh-arg-re "\\(\\s +\\(\\S +\\)\\)?")
               grep-default)
              ;; If the string is not yet complete.
              (string-match "\\(\\)\\'" grep-default))
      ;; Maybe we will replace the pattern with the default tag.
      ;; But first, maybe replace the file name pattern.

      ;; Now replace the pattern with the default tag.
      (replace-match tag-default t t grep-default 1))))


(defun grep-shell-quote-argument (argument)
  "Quote ARGUMENT for passing as argument to an inferior shell."
  (cond
   ((and (boundp 'no-grep-quote)
         no-grep-quote)
    (format "\"%s\"" argument))
   ((equal argument "")
    "\"\"")
   (t
    ;; Quote everything except POSIX filename characters.
    ;; This should be safe enough even for really weird shells.
    (let ((result "") (start 0) end)
      (while (string-match "[].*[^$\"\\]" argument start)
        (setq end (match-beginning 0)
              result (concat result (substring argument start end)
                             (let ((char (aref argument end)))
                               (cond
                                ((eq ?$ char)
                                 "\\\\\\")
                                ((eq ?\\  char)
                                 "\\\\\\")
                                (t
                                 "\\"))) (substring argument end (1+ end)))
              start (1+ end)))
      (concat "\"" result (substring argument start) "\"")))))

(defun bhj-grep-tag-default ()
  (let ((tag (grep-tag-default)))
    (cond
     ((region-active-p)
      tag)
     ((string-match "/res/.*\.xml\\|AndroidManifest.xml" (or (buffer-file-name) ""))
      (replace-regexp-in-string "</\\w+>\\|^<\\|^.*?/" "" tag))
     (t
      (replace-regexp-in-string "^<\\|>$" "" tag)))))

;;;###autoload
(defun grep-bhj-dir ()
  (interactive)
  (let ((default-directory
          (if bhj-grep-dir
              (expand-file-name bhj-grep-dir)
            default-directory))
        (compilation-buffer-name-function (lambda (_ign) (if (boundp 'grep-buffer-name)
                                                             grep-buffer-name
                                                           "*grep*"))))
    (call-interactively 'grep)))

(defun bhj-grep ()
  (interactive)
  (let ((current-prefix-arg 4)
        ;; (default-directory (eval bhj-grep-default-directory))
        ;; (grep-use-null-device nil) ; uncomment it for beagrep only
        )
    (nodup-ring-insert ajoke--marker-ring (point-marker))
    (call-interactively 'grep-bhj-dir)))

(defun nodup-ring-insert (ring obj)
  (unless (and (not (ring-empty-p ring))
               (equal (ring-ref ring 0) obj))
    (ring-insert ring obj)))

(define-key goto-map "r" 'bhj-grep)

(defun open-external (&optional file)
  "Open the current file or dired marked files in external app.

The app is chosen from your OS's preference."
  (interactive)
  (let (doIt
        (myFileList
         (cond
          ((string-equal major-mode "dired-mode") (dired-get-marked-files))
          ((not file) (list (buffer-file-name)))
          (file (list file)))))

    (setq doIt (if (<= (length myFileList) 5) ; TODO: defcustom
                   t
                 (y-or-n-p "Open more than 5 files? ") ) )

    (when doIt
      (cond
       ((string-equal system-type "windows-nt")
        (mapc (lambda (fPath) (w32-shell-execute "open" (replace-regexp-in-string "/" "\\" fPath t t)) ) myFileList))
       ((string-equal system-type "darwin")
        (mapc (lambda (fPath) (shell-command (format "open \"%s\"" fPath)) )  myFileList) )
       ((string-equal system-type "gnu/linux")
        (mapc (lambda (fPath) (let ((process-connection-type nil)) (start-process "" nil "xdg-open" fPath)) ) myFileList))))))

;; grep respect directory path

(defun grep-respect-path-directory ()
  "Show grep output relative to search directory."
  (setq compilation-directory-matcher
        (default-value 'compilation-directory-matcher)))

;; In shift selection, retain the mark if it's already active

;; (defadvice handle-shift-selection
;;     (around handle-shift-selection-better-integration activate)
;;   (cond
;;    ((not shift-select-mode)) ;; do nothing if shift-select-mode is off
;;    ((eq (car-safe transient-mark-mode) 'only)
;;     ;; If the original mechanism is in progress, use it
;;     ad-do-it)
;;    ((and mark-active
;;          (not this-command-keys-shift-translated)
;;          (or (symbolp last-command-event)
;;              (eq (car-safe transient-mark-mode) 'only)))
;;     ;; On a non-shifted event, deactivate the mark either if it had been set
;;     ;; by a shift-selection command or if the event was a cursor key or
;;     ;; other function key.
;;     (deactivate-mark))
;;    ((and (not mark-active)
;;          this-command-keys-shift-translated)
;;     ;; On a shifted event, set the mark to the current location and activate
;;     ;; it..
;;     (push-mark nil t t))
;;    ;; Note that we never set the mark to the current location if it was
;;    ;; already active..
;;    ))

(defadvice handle-shift-selection
    (around leave-mark-intact activate preactivate compile)
  (cond ((and shift-select-mode this-command-keys-shift-translated)
         (unless mark-active
           (setq transient-mark-mode
                 (cons 'only
                       (unless (eq transient-mark-mode 'lambda)
                         transient-mark-mode)))
           (push-mark nil nil t)))
        ((eq (car-safe transient-mark-mode) 'only)
         (setq transient-mark-mode (cdr transient-mark-mode))
         (deactivate-mark))))

;; tmux integration

(defadvice terminal-init-screen
    ;; The advice is named `tmux', and is run before `terminal-init-screen' runs.
    (before tmux activate)
  ;; Docstring.  This describes the advice and is made available inside emacs;
  ;; for example when doing C-h f terminal-init-screen RET
  "Apply xterm keymap, allowing use of keys passed through tmux.
Use xterm default colors in tmux."
  ;; This is the elisp code that is run before `terminal-init-screen'.
  ;; (when (getenv "TMUX")
  ;; Use the xterm color initialization code
  (xterm-register-default-colors)
  (tty-set-up-initial-frame-faces)
  ;; use xterm keymap for ctrl + shift
  (let ((map (copy-keymap xterm-function-map)))
    (set-keymap-parent map (keymap-parent input-decode-map))
    (set-keymap-parent input-decode-map map)))

;; Esc in TTY

(defvar evil-esc-mode nil
  "Non-nil if `evil-esc-mode' is enabled.")

(defcustom evil-intercept-esc 'always
  "Whether evil should intercept the ESC key.
In terminal, a plain ESC key and a meta-key-sequence both
generate the same event.  In order to distinguish both evil
modifies `input-decode-map'.  This is necessary in terminal but
not in X mode.  However, the terminal ESC is equivalent to C-[, so
if you want to use C-[ instead of ESC in X, then Evil must
intercept the ESC event in X, too.  This variable determines when
Evil should intercept the event."
  :type '(radio (const :tag "Never" :value nil)
                (const :tag "In terminal only" :value t)
                (const :tag "Always" :value always))
  :group 'evil)

(defvar evil-inhibit-esc nil
  "If non-nil, the \\e event will never be translated to 'escape.")

(defcustom evil-esc-delay 0.01
  "Time in seconds to wait for another key after ESC."
  :type 'number
  :group 'evil)

(defun evil-esc-mode (&optional arg)
  "Toggle interception of \\e (escape).
Enable with positive ARG and disable with negative ARG.

When enabled, `evil-esc-mode' modifies the entry of \\e in
`input-decode-map'.  If such an event arrives, it is translated to
a plain 'escape event if no further event occurs within
`evil-esc-delay' seconds.  Otherwise no translation happens and
the ESC prefix map (i.e. the map originally bound to \\e in
`input-decode-map`) is returned."
  (cond
   ((or (null arg) (eq arg 0))
    (evil-esc-mode (if evil-esc-mode -1 +1)))
   ((> arg 0)
    (unless evil-esc-mode
      (setq evil-esc-mode t)
      (add-hook 'after-make-frame-functions #'evil-init-esc)
      (mapc #'evil-init-esc (frame-list))))
   ((< arg 0)
    (when evil-esc-mode
      (remove-hook 'after-make-frame-functions #'evil-init-esc)
      (mapc #'evil-deinit-esc (frame-list))
      (setq evil-esc-mode nil)))))

(defun evil-init-esc (frame)
  "Update `input-decode-map' in terminal with FRAME."
  (with-selected-frame frame
    (let ((term (frame-terminal frame)))
      (when (and
             (or (eq evil-intercept-esc 'always)
                 (and evil-intercept-esc
                      (eq (terminal-live-p term) t))) ; only patch tty
             (not (terminal-parameter term 'evil-esc-map)))
        (let ((evil-esc-map (lookup-key input-decode-map [?\e])))
          (set-terminal-parameter term 'evil-esc-map evil-esc-map)
          (define-key input-decode-map [?\e]
            `(menu-item "" ,evil-esc-map :filter ,#'evil-esc)))))))

(defun evil-deinit-esc (frame)
  "Restore `input-decode-map' in terminal with FRAME."
  (with-selected-frame frame
    (let ((term (frame-terminal frame)))
      (when (terminal-live-p term)
        (let ((evil-esc-map (terminal-parameter term 'evil-esc-map)))
          (when evil-esc-map
            (define-key input-decode-map [?\e] evil-esc-map)
            (set-terminal-parameter term 'evil-esc-map nil)))))))

(defun evil-esc (map)
  "Translate \\e to 'escape if no further event arrives in MAP.
This function is used to translate a \\e event either to 'escape
or to the standard ESC prefix translation map.  If \\e arrives,
this function waits for `evil-esc-delay' seconds for another
event.  If no other event arrives, the event is translated to
'escape, otherwise it is translated to the standard ESC prefix
map stored in `input-decode-map'.  If `evil-inhibit-esc' is
non-nil or if evil is in Emacs state, the event is always
translated to the ESC prefix.

The translation to 'escape happens only if the current command
has indeed been triggered by \\e.  In other words, this will only
happen when the keymap is accessed from `read-key-sequence'.  In
particular, if it is access from `define-key' the returned
mapping will always be the ESC prefix map."
  (if (and (not evil-inhibit-esc)
           ;; (or evil-local-mode (evil-ex-p))
           ;; (not (evil-emacs-state-p))
           (let ((keys (this-single-command-keys)))
             (and (> (length keys) 0)
                  (= (aref keys (1- (length keys))) ?\e)))
           (sit-for evil-esc-delay))
      (prog1 [escape]
        (when defining-kbd-macro
          (end-kbd-macro)
          (setq last-kbd-macro (vconcat last-kbd-macro [escape]))
          (start-kbd-macro t t)))
    map))

(evil-esc-mode 1)

;; FIX: remove scroll bars in new frames
(defun disable-all-scroll-bars (frame)
  "Disable all the scroll bars for FRAME."
  (modify-frame-parameters frame
                           '((vertical-scroll-bars . nil)
                             (horizontal-scroll-bars . nil))))
(add-hook 'after-make-frame-functions 'disable-all-scroll-bars)

;; workaround for smooth horizontal scrolling
;; TODO: C-u prefix, not jump to scrolling window
(defun horizontal-scroll-left ()
  "Scroll left one step."
  (let ((step
         (if (zerop hscroll-step)
             nil
           hscroll-step)))
    (scroll-left step t)))

(defun hscroll-left ()
  "Scroll left one step."
  (interactive)
  (horizontal-scroll-left))


(defun horizontal-mscroll-left (event)
  "Scroll left one step according to the EVENT."
  (interactive (list last-input-event))
  (with-current-buffer (window-buffer
                        (if mouse-wheel-follow-mouse
                            (prog1
                                (selected-window)
                              (select-window (mwheel-event-window event)))))
    (horizontal-scroll-left)))

(defun horizontal-scroll-right ()
  "Scroll right one step."
  (let ((step
         (if (zerop hscroll-step)
             nil
           hscroll-step)))
    (scroll-right step t)))

(defun hscroll-right ()
  "Scroll right one step."
  (interactive)
  (horizontal-scroll-right))

(defun horizontal-mscroll-right (event)
  "Scroll right one step according to the EVENT."
  (interactive (list last-input-event))
  (with-current-buffer (window-buffer
                        (if mouse-wheel-follow-mouse
                            (prog1
                                (selected-window)
                              (select-window (mwheel-event-window event)))))
    (horizontal-scroll-right)))

;; Make `hl-line-mode' highlight just one line
(defun visual-line-line-range ()
  "Range of visible part of line."
  (save-excursion
    (cons (progn (vertical-motion 0) (point))
          (progn (vertical-motion 1) (point)))))

(setq hl-line-range-function 'visual-line-line-range)
;; TODO: refresh `hl-line-mode' font lock on morphing current line after changing window/frame size with `visual-line-mode'.

;; macOS style frame navigation
(defun go-next-frame ()
  "Move focus to the `next-frame'."
  (interactive)
  (select-frame-set-input-focus (next-frame)))

(defun go-previous-frame ()
  "Move focus to the `next-frame'."
  (interactive)
  (select-frame-set-input-focus (previous-frame)))

;; End there
(provide 'hacks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; hacks.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
