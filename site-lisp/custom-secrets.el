;; custom-secrets.el --- This file load encrypted customization -*-
;;lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;;; Commentary:
;;
;; Copyright (C) 2012-2018  Yuriy V. Pitomets < pitometsu@gmail.com >
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:

(when (functionp 'exec-path-from-shell-initialize)
  (init-exec-path-from-shell))
(load-library "custom-secrets.el.gpg")
(provide 'custom-secrets)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; custom-secrets.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
