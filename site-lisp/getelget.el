;;; getelget --- el-get boostrap script
;;
;;; Commentary:
;;
;; Checks to see if el-get has been checked out, and bootstraps it if
;; it has not. After bootstrapping, calls el-get to load specified
;; packages.
;;
;; el-get-packages should be defined before including this file. Any
;; definitions from el-get-sources will be appended to el-get-packages.
;;
;; Written in 2011 by Nathan R. Yergler <nathan@yergler.net>
;; Improved in 2014 by Yuriy V. Pitomets <pitometsu@gmail.com>
;;
;; To the extent possible under law, the person who associated CC0 with
;; getelget has waived all copyright and related or neighboring rights
;; to getelget.
;;
;; You should have received a copy of the CC0 legalcode along with this
;; work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

;; add a hook listener for post-install el-get
;;
;;; Code:

(require 'el-get nil 'noerror)

(defun post-install-hook (pkg)
  "After installing el-get, load the local package list.
PKG - accepted package name in hook."
  (when (string-equal pkg "el-get")
    ;; update recipes from emaccswiki
    ;; (el-get-emacswiki-refresh nil t)
    (el-get-sync)))

;; load packages after bootstraping
(add-hook 'el-get-post-install-hooks 'post-install-hook)

;; add the el-get directory to the load path
(add-to-list 'load-path
             (concat (file-name-as-directory user-emacs-directory)
                     (file-name-as-directory "el-get")
                     "el-get"))

(defun el-get-sync ()
  "Install all user's packages from `el-get-packages' variable."
  (interactive)
  (let ((emacs-lisp-mode-hook nil)
        (el-get-git-shallow-clone t)
        ;; (el-get-is-lazy t)
        (el-get-user-package-directory (concat
                                        (file-name-as-directory user-emacs-directory)
                                        "inits"))
        ;; (el-get-growl-notify-path "/usr/local/bin/growlnotify")
        )
    (el-get 'sync el-get-packages)))

(defun el-get-clean ()
  "Cleanup unnecessary packages.  See `el-get-cleanup'."
  (interactive)
  (el-get-cleanup el-get-packages))

;; try to require el-get
(if (require 'el-get nil 'noerror)
    ;; then
    (progn
      ;; load and cleanup packages
      (el-get-sync)
      (el-get-clean))
  ;; else
  (with-current-buffer
      ;; bootstrap
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (let (el-get-master-branch
          el-get-install-skip-emacswiki-recipes)
      (goto-char (point-max))
      (eval-print-last-sexp))))

;; End there
(provide 'getelget)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; getelget.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
