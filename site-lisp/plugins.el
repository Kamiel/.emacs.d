;;; plugins.el --- This file contain plugins packages management. See getelget.el also -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;;; Commentary:
;;
;; Copyright (C) 2012-2015  Yuriy V. Pitomets < pitometsu@gmail.com >
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:

(eval-when-compile
  (require 'cl-lib))

;; custom additional packages sources
(setq el-get-sources
      '(
        ;; currently none

        ;; but some:
        (:name helm-ispell
               :description "ispell completion with helm interface"
               :type github
               :pkgname "syohex/emacs-helm-ispell"
               :depends helm)
        (:name ac-inf-ruby
               :description "An Emacs auto-complete source for use in inf-ruby sessions"
               :type github
               :pkgname "purcell/ac-inf-ruby"
               :depends (inf-ruby auto-complete))
        (:name helm-ack
               :description "App::ack with helm interface"
               :type github
               :pkgname "syohex/emacs-helm-ack"
               :depends helm)
        (:name inertial-scroll
               :description "Inertial scrolling for emacs"
               :type github
               :pkgname "kiwanami/emacs-inertial-scroll"
               :depends deferred
               :prepare (autoload 'inertias-global-minor-mode "inertial-scroll"))
        (:name smart-indent-rigidly
               :description "Indentation minor-mode for whitespace-sensitve languages (haml, sass, etc)"
               :type github
               :pkgname "re5et/smart-indent-rigidly")
        (:name ac-ghc-mod
               :description "Smart auto-complete sources for haskell mode"
               :type github
               :pkgname "Pitometsu/ac-ghc-mod"
               :depends (auto-complete ghc-mod))
        (:name import-popwin
               :description "Pop up buffer near by import statements with popwin"
               :type github
               :pkgname "syohex/emacs-import-popwin"
               :depends popwin)
        (:name splitjoin
               :description "Emacs port of splitjoin.vim"
               :type github
               :pkgname "syohex/emacs-splitjoin")
        (:name utop
               :description "Improved toplevel (i.e., Read-Eval-Print Loop) for OCaml"
               :type http
               :url "https://raw.githubusercontent.com/ocaml-community/utop/master/src/top/utop.el")
        (:name merlin
               :description "Context sensitive completion for OCaml"
               :type svn
               :url "https://github.com/ocaml/merlin/trunk/emacs"
               :prepare (progn
                          (autoload 'merlin-imenu-create-index "merlin-imenu" nil t)
                          (autoload 'merlin--document-pure "merlin")))
        (:name tuareg-mode
               :type github
               :description "an Emacs OCaml mode"
               :load-path
               (".")
               :pkgname "ocaml/tuareg"
               :prepare
               (progn
                 (autoload
                   (quote tuareg-mode)
                   "tuareg" "Major mode for editing Caml code" t)
                 (autoload
                   (quote camldebug)
                   "camldebug" "Run the Caml debugger" t)
                 (dolist
                     (ext
                      (quote
                       (".cmo" ".cmx" ".cma" ".cmxa" ".cmi")))
                   (add-to-list
                    (quote completion-ignored-extensions)
                    ext))
                 (add-to-list
                  (quote auto-mode-alist)
                  (quote
                   ("\\.ml[iylp]?" . tuareg-mode))))
               :depends caml-mode)
        (:name caml-mode
               :description "OCaml code editing commands for Emacs"
               :type github
               :pkgname "ocaml/caml-mode"
               :prepare
               (progn
                 (autoload
                   (quote caml-mode)
                   "caml" "Major mode for editing OCaml code." t)
                 (autoload
                   (quote run-caml)
                   "inf-caml" "Run an inferior OCaml process." t)
                 (autoload
                   (quote camldebug)
                   "camldebug" "Run ocamldebug on program." t)
                 (add-to-list
                  (quote auto-mode-alist)
                  (quote
                   ("\\.ml[iylp]?$" . caml-mode)))
                 (add-to-list
                  (quote interpreter-mode-alist)
                  (quote
                   ("ocamlrun" . caml-mode)))
                 (add-to-list
                  (quote interpreter-mode-alist)
                  (quote
                   ("ocaml" . caml-mode)))))
        (:name hare
               :description "The Haskell Refactoring Tool"
               :type svn
               :url "https://github.com/alanz/HaRe/trunk/elisp")
        (:name hindent
               :description "Extensible Haskell pretty printer"
               :type svn
               :url "https://github.com/commercialhaskell/hindent/trunk/elisp")
        (:name sclang
               :website "http://supercollider.sourceforge.net/"
               :description "SuperCollider/Emacs interface"
               :type svn
               :url "https://github.com/supercollider/supercollider/trunk/editors/scel/el"
               :depends emacs-w3m
               :prepare (progn
                          (autoload 'sclang-mode "sclang-mode" nil t)
                          (add-to-list 'auto-mode-alist
                                       '("\\.\\(sc\\|scd\\)$" . sclang-mode))
                          (add-to-list 'interpreter-mode-alist
                                       '("sclang" . sclang-mode))))
        (:name structured-haskell-mode
               :description "Structured Haskell editing operations."
               :type github
               :pkgname "chrisdone/structured-haskell-mode"
               :depends (haskell-mode)
               :load-path "elisp"
               :post-init
               (setq shm-program-name
                     (shell-command-to-string
                      "which structured-haskell-mode")))
        (:name helm-flx
               :description "Flx-based fuzzy matching for helm"
               :type github
               :pkgname "PythonNut/helm-flx"
               :depends (helm flx))
        (:name helm-fuzzier
               :description "Better Fuzzy Matching for emacs Helm"
               :type github
               :pkgname "EphramPerdition/helm-fuzzier"
               :depends (helm helm-flx))
        (:name whitespace-cleanup-mode
               :description "In Emacs, intelligently call whitespace-cleanup on save"
               :type github
               :pkgname "purcell/whitespace-cleanup-mode")
        (:name mode-line-in-header
               :description "Minor mode to display mode line in current buffer's header"
               :type github
               :pkgname "larstvei/mode-line-in-header")
        (:name auto-complete-ruby
               :description "Auto-complete sources for Ruby"
               :type http
               :url "http://www.cx4a.org/pub/auto-complete-ruby.el"
               :depends (auto-complete rcodetools)
               :prepare (autoload 'ac-ruby-init "auto-complete-ruby"))
        (:name nlinum
               :description "Show line numbers in the margin"
               :type github
               :pkgname "emacsmirror/nlinum")
        (:name linum+
               :description "Show line numbers in the margin"
               :type github
               :pkgname "emacsmirror/linum-plus")
        (:name polymode
               :description "Object oriented framework for multiple emacs modes based on indirect buffers"
               :type github
               :pkgname "polymode/polymode")
        (:name poly-org
               :description "Polymode for org-mode"
               :type github
               :pkgname "polymode/poly-org")
        (:name poly-markdown
               :description "Polymode for markdown-mode"
               :type github
               :pkgname "polymode/poly-markdown"
               :depends markdown-mode)
        (:name smooth-scrolling
               :description "Make emacs scroll smoothly, keeping the point away from the top and bottom of the current buffer's window in order to keep lines of context around the point visible as much as possible, whilst avoiding sudden scroll jumps which are visually confusing."
               :type github
               :pkgname "Rotsor/smooth-scrolling"
               :branch "negative-line-count")
        (:name projectile-rails-robe
               :description "Separate robe servers for every single rails project."
               :type github
               :pkgname "lululau/projectile-rails-robe"
               :depends (robe-mode projectile-rails))
        (:name dumb-jump
               :description "jump to definition using ag."
               :type github
               :pkgname "jacktasia/dumb-jump"
               :depends (f s dash popup)
               :prepare (progn
                          (autoload 'dumb-jump-go "dumb-jump" nil t)
                          (autoload 'dumb-jump-back "dumb-jump" nil t)
                          (autoload 'dumb-jump-quick-look "dumb-jump" nil t)))
        (:name flymd
               :description "On the fly markdown preview."
               :type github
               :pkgname "mola-T/flymd")
        (:name ranger
               :description "Bringing the goodness of ranger to dired!"
               :type github
               :pkgname "ralesi/ranger.el"
               :depends (diminish))
        (:name xterm-extras
               :description "define additional function key sequences for recent versions of xterm"
               :type github
               :pkgname "yyr/xterm-extras.el"
               :prepare (autoload 'xterm-extra-keys "xterm-extras"))
        (:name xterm-keybinder
               :description "Let you extra keybinds in xterm/urxvt."
               :type github
               :pkgname "yuutayamada/xterm-keybinder-el")
        (:name ac-emmet
               :description "auto-complete sources for emmet-mode's snippets."
               :type github
               :pkgname "yasuyk/ac-emmet"
               :depends (auto-complete emmet-mode))
        ;; https://github.com/chrisdone/chrisdone-emacs/blob/master/packages/align-by-current-symbol/align-by-current-symbol.el
        (:name wicd
               :description "Wicd client for Emacs."
               :type github
               :pkgname "rafoo/wicd-mode.el"
               :prepare (autoload 'wicd "wicd-mode" nil t))
        (:name whole-line-or-region
               :description "Operate on current line if region undefined"
               :type github
               :pkgname "purcell/whole-line-or-region")
        (:name rrc
               :description "Replace Recent Character"
               :type http
               :url "https://raw.githubusercontent.com/rodimius/emacs-config/master/rrb.el"
               :prepare
               (autoload
                 (quote replace-recent-character)
                 "rrc" "" t))
        ;; (:name ac-anaconda
        ;;        :description "Anaconda backend for auto-complete-mode."
        ;;        :type github
        ;;        :pkgname "proofit404/ac-anaconda"
        ;;        :depends
        ;;        (anaconda-mode auto-complete)
        ;;        :post-init
        ;;        (eval-after-load 'auto-complete
        ;;          '(add-to-list 'ac-sources 'ac-source-anaconda)))
        (:name clj-refactor
               :description "A collection of Clojure refactoring functions for Emacs."
               :type github
               :pkgname "clojure-emacs/clj-refactor.el"
               :depends (cider edn clojure-mode))
        (:name flycheck-tip
               :type github
               :pkgname "yuutayamada/flycheck-tip"
               :description "show you error by popup-tip"
               :depends
               (flycheck popup)
               :prepare
               (autoload 'flycheck-tip-use-timer "flycheck-tip"))
        (:name company-sourcekit
               :type github
               :pkgname "nathankot/company-sourcekit"
               :description "Completion for Swift projects via SourceKit with the help of SourceKitten."
               :depends (company-mode json dash))
        (:name flycheck-swift
               :type github
               :pkgname "swift-emacs/flycheck-swift"
               :description "Flycheck extension for Apple's Swift programming language."
               :depends flycheck)
        (:name intero
               :type svn
               :url "https://github.com/commercialhaskell/intero/trunk/elisp"
               :description "Complete interactive development program for Haskell"
               :depends (flycheck company-mode))
        (:name hlint-refactor
               :type github
               :pkgname "mpickering/hlint-refactor-mode"
               :description "Emacs bindings for hlint's --refactor option")
        (:name highlight-indent-guides
               :type github
               :pkgname "DarthFennec/highlight-indent-guides"
               :description "Emacs minor mode to highlight indentation.")
        (:name helm-xref
               :type github
               :pkgname "brotzeit/helm-xref"
               :description "Helm interface for xref results"
               :depends helm
               :prepare (autoload 'helm-xref-show-xrefs "helm-xref")
               :post-init (setq xref-show-xrefs-function 'helm-xref-show-xrefs))
        (:name sql-indent
               :type github
               :pkgname "alex-hhh/emacs-sql-indent"
               :description "Syntax based indentation for SQL.")
        ;;(:name: counsel-osx-app)
        ;; (:name hangups)
        (:name persp-mode
               :type github
               :pkgname "Bad-ptr/persp-mode.el"
               :description "named perspectives (set of buffers/window configs)")
        (:name persp-mode-projectile-bridge
               :type github
               :pkgname "Bad-ptr/persp-mode-projectile-bridge.el"
               :description "persp-mode projectile integration"
               :depends (persp-mode projectile))
        (:name olivetti
               :type github
               :pkgname "rnkn/olivetti"
               :description "Olivetti is a simple Emacs minor mode for a nice writing environment.")
        (:name pipenv
               :type github
               :pkgname "pwalsh/pipenv.el"
               :description "Manage Pythons with Pipenv in Emacs."
               :depends (s f))
        (:name isend-mode
               :type github
               :pkgname "ffevotte/isend-mode.el"
               :description "Interactively send parts of an Emacs buffer to an interpreter")
        (:name flycheck-mypy
               :type github
               :pkgname "lbolla/emacs-flycheck-mypy"
               :description "FlyCheck interface to MyPy."
               :depends flycheck)
        (:name pytest
               :type github
               :pkgname "ionrock/pytest-el"
               :description "Run py.test on testing functions, classes, modules and entire suites in Emacs."
               :depends s)
        (:name nix-mode
               :type github
               :pkgname "NixOS/nix-mode"
               :description "Major mode for editing Nix expressions")
        (:name magit
               :website "https://github.com/magit/magit#readme"
               :description "It's Magit! An Emacs mode for Git."
               :type github
               :pkgname "magit/magit"
               :branch "master"
               :minimum-emacs-version "24.4"
               :depends
               (dash emacs-async ghub let-alist magit-popup with-editor transient)
               :prepare (autoload 'magit-status "magit-status")
               :info "Documentation"
               :load-path "lisp/"
               :compile "lisp/"
               :build
               (\`
                (("make"
                  (\,
                   (format "EMACSBIN=%s" el-get-emacs))
                  "docs")
                 ("touch" "lisp/magit-autoloads.el")))
               :build/berkeley-unix
               (\`
                (("gmake"
                  (\,
                   (format "EMACSBIN=%s" el-get-emacs))
                  "docs")
                 ("touch" "lisp/magit-autoloads.el")))
               :build/windows-nt
               (with-temp-file "lisp/magit-autoloads.el" nil))
        (:name deadgrep
               :type github
               :pkgname "Wilfred/deadgrep"
               :description "fast, friendly searching with ripgrep and Emacs"
               :depends (s dash spinner projectile))
        (:name ripgrep
               :type github
               :pkgname "nlamirault/ripgrep.el"
               :description "Emacs front-end for ripgrep, a command line search tool"
               :depends wgrep)
        (:name ocamlformat
               :description "utility functions to format ocaml code"
               :type http
               :url "https://raw.githubusercontent.com/ocaml-ppx/ocamlformat/master/emacs/ocamlformat.el")
        (:name merlin-eldoc
               :description "Type and doc on hover for OCaml and Reason in emacs "
               :type http
               :url "https://raw.githubusercontent.com/Khady/merlin-eldoc/master/merlin-eldoc.el"
               :depends tuareg-mode
               :post-init (add-hook 'tuareg-mode-hook 'merlin-eldoc-setup))
        (:name direnv
               :description "direnv integration for emacs"
               :type http
               :url "https://raw.githubusercontent.com/wbolster/emacs-direnv/master/direnv.el"
               :depends (dash))
        (:name nix-haskell-mode
               :description "Automatic Haskell setup in Emacs, for Nix users"
               :type github
               :pkgname "matthewbauer/nix-haskell-mode")
        (:name nix-buffer
               :description "nix-shell for emacs buffers"
               :type github
               :pkgname "shlevy/nix-buffer")
        (:name nix-emacs
               :description "A set of useful Emacs modes and functions for users of Nix and Nix OS"
               :type github
               :pkgname "travisbhartwell/nix-emacs")
        (:name jsonrpc
               :description "JSON-RPC library"
               :type http
               :url "http://git.savannah.gnu.org/cgit/emacs.git/plain/lisp/jsonrpc.el")
        (:name eglot
               :description "A client for Language Server Protocol servers"
               :type github
               :pkgname "joaotavora/eglot"
               :depends jsonrpc)
        (:name lsp-mode
               :website "https://github.com/emacs-lsp/lsp-mode"
               :description "Emacs client/library for the Language Server Protocol"
               :depends (hydra dash f ht spinner)
               :type github
               :pkgname "emacs-lsp/lsp-mode")
        (:name lsp-ivy
               :description "Ivy lsp integration"
               :type http
               :url "https://raw.githubusercontent.com/emacs-lsp/lsp-ivy/master/lsp-ivy.el"
               :depends (swiper dash lsp-mode))
        (:name lsp-haskell
               :description "An Emacs Lisp library for interacting with a Haskell language server such as haskell-language-server or ghcide using Microsoft's Language Server Protocol."
               :type http
               :url "https://raw.githubusercontent.com/emacs-lsp/lsp-haskell/master/lsp-haskell.el"
               :depends (haskell-mode lsp-mode projectile))
	(:name hsnippets
	       :description "Emacs YASnippet snippets for Haskell."
	       :type github
	       :pkgname "mightybyte/hsnippet"
	       :post-init (eval-after-load 'yasnippet
			    `(progn
			       (add-to-list 'yas-snippet-dirs ,default-directory t)
			       (yas-load-directory ,default-directory)))
	       :depends yasnippet)
	;; (:name opam-mode
	;;        :description "Major mode for editing opam files"
	;;        :type github
	;;        :pkgname "sagotch/opam-mode.el")
    (:name undo-tree
           :description "Treat undo history as a tree"
           :website "https://www.dr-qubit.org/undo-tree.html"
           :type git
           :url "https://gitlab.com/tsc25/undo-tree.git/")
        ))

;; packages customizations
(defvar el-get-packages
  '(
    ;; ~ LIB ~
    ;; iterator
    ;; defered ; async in elisp
    ;; ov ; regions overlays
    f
    s
    dash
    ;; names ;; ?
    ;; hexrgb

    ;; ~ ENVIRONMENT ~
    ;; notify
    ;; alert
    ;; edit-server
    xclip
    ;; xterm-color
    ;; xterm-extras
    xterm-keybinder

    ;; ~ SYS ~
    nix-mode
    ;; nix-buffer
    nix-emacs
    exec-path-from-shell

    ;; ~ GAMES ~
    ;; flappymacs
    ;; 2048

    ;; ~ ACCESSORY ~
    ;; powerline
    ;; diminish
    ;; stripe-buffer
    ;; rebox2
    ;; pastie
    ;; dictem
    ;; google-translate
    ;; thesaurus
    ;; quickrun
    ;; emacs-sos
    myrth-theme
    ;; soundklaus

    ;; ~ UTILITIES ~
    edbi
    ;; multi-term
    ;; regex-tool
    ;; typing
    ;; shell-switcher
    ;; howdoi
    ;; helm-google
    ;; helm-proc
    ;; csv-mode
    emacs-jabber
    dropbox
    ;; emacs-sos
    sx
    ;; realgud ;; ?
    know-your-http-well
    jquery-doc
    flymd
    wicd
    prodigy
    ;; realgud ; TODO: FIX recipe

    ;; ~ GIT ~
    magit ; ; (when (executable-find "git") ...)
    ;; git-gutter-fringe
    git-gutter+
    git-gutter-fringe+
    helm-ls-git
    ;; helm-ghq
    helm-git-grep
    git-timemachine
    ;; gitignore
    ;; github
    ;; git-modes
    ;; github-clone.el
    ;; git-messenger
    ;; git-blame
    ;; magit-tramp
    ;; dired-k

    ;; Mercurial
    monky

    ;; ~ DEVOPS ~
    docker
    dockerfile-mode

    ;; ~ ARDUINO ~
    ;; arduino-mode ;; ?

    ;; ~ WORKFLOW ~
    smartparens
    smart-newline
    anzu
    auto-indent-mode
    clean-aindent
    aggressive-indent-mode
    col-highlight
    ;; highline
    lacarte
    pp-c-l
    textmate
    undo-tree
    whole-line-or-region
    yasnippet
    yasnippets
    swiper
    ;; swiper-helm
    smex
    flx
    ;;-> counsel-osx-app
    helm
    helm-xref
    ;; helm-flx
    ;; helm-fuzzier
    auto-complete
    company-mode
    ;; pos-tip
    ac-capf
    goto-last-change
    phi-search ; phi-search phi-rectangle
    deadgrep
    ripgrep
    ;; highlight-indentation
    highlight-indent-guides
    helm-c-yasnippet
    helm-descbinds
    helm-cmd-t
    helm-swoop
    helm-ispell
    helm-ag
    helm-ack
    smooth-scrolling
    inertial-scroll
    jump-char
    projectile
    counsel-projectile
    helm-projectile
    ag
    wgrep
    fastnav
    ;; flymake-cursor
    ;; zencoding-mode
    emmet-mode
    ac-emmet
    multiple-cursors
    highlight-symbol
    expand-region
    drag-stuff
    anchored-transpose
    rrc
    volatile-highlights
    ;; ace-jump-mode
    avy
    ;; ace-isearch
    thing-cmds
    quick-jump
    ;; yank-pop-summary
    delim-pad
    ;; ergoemacs-mode
    ;; guide-key-tip
    guide-key
    manage-minor-mode
    ;; discover-my-major ;; ?
    visual-regexp
    seqential-command
    nrepl-eval-sexp-fu
    ;; fixmee
    electric-case
    srep
    smart-mode-line
    ;; fancy-narrow
    orgbox
    ;; import-popwin
    flycheck
    flycheck-tip
    ;; flycheck-pos-tip
    flyspell-lazy
    change-inner
    vimish-fold
    ;; evil-nerd-commenter
    whitespace-cleanup-mode
    ;; mode-line-in-header
    ;; nlinum
    ;; linum+
    ;; swiper ;; ?
    ;; smart-scan
    olivetti
    isend-mode
    direnv
    with-editor
    eglot
    lsp-mode
    lsp-ui

    ;; ~ WINDOWS ~
    ;; window-number
    windsize
    ;; framemove
    buffer-move
    ;; sunrise-commander
    dired+
    ranger
    popwin
    ;; shackle
    ;; sr-speedbar
    workgroups2
    ;; persp-mode
    ;; persp-mode-projectile-bridge
    direx
    ;; nterm
    ;; emacs-nav
    ace-window

    ;; ~ ERGONOMIC ~
    keyfreq
    ;; free-keys

    ;; ~ DB ~
    sql-indent

    ;; ~ OBJC ~
    ;; objc-font-lock ; (when (executable-find "clang") ...) ;; ?
    ;; flydoc ;; ?
    ;; auto-complete-clang-objc ;; ?
    adaptive-wrap
    visual-fill-column
    ;; cedit ;; ?
    swift-mode ; (when (executable-find "swift") ...) ;; ?
    flycheck-swift
    company-sourcekit

    ;; ~ HASKELL ~
    haskell-mode ; (when (executable-find "ghc") ...)
    ;; nix-haskell-mode
    ;; intero
    ;; dante
    ;; shime
    hindent
    hlint-refactor
    ;; haskell-mode-exts
    ;; ghc-mod ; (when (executable-find "ghc-mod") ...)
    ;; ac-ghc-mod
    hare
    ;; ac-ghc-mod ; not stable yet, use build-in ghc-mod ac source
    ;; hsnippets
    flycheck-haskell
    ;; ????????????>>>>>>> flycheck-stack
    ;; flycheck-hdevtools ; currently unmaintained
    ;; structured-haskell-mode
    helm-haskell-import
    ghci-completion
    ;; hs-lint
    ;; hayoo
    ;; smart-indent-rigidly
    hi2 ; indentation mode
    lsp-haskell

    ;; OCaml
    ;; opam-mode
    tuareg-mode
    merlin-eldoc
    ocp-indent
    ocamlformat
    utop
    merlin ; iedit???

    ;; ~ R ~
    ;; ess
    ;; ess-smart-underscore ;; ?

    ;; ~ OCTAVE ~
    ;; ac-octave ;; ?

    ;; ~ PYTHON ~
    ;; jedi
    ;; jedi-direx
    ein
    ;; helm-ipython
    ob-ipython
    anaconda-mode
    ;; ac-anaconda '; TODO: migrate on `company-anaconda'
    toml-mode
    pipenv
    flycheck-mypy
    pytest

    ;; ~ RUBY ~
    ;; rcodetools
    ;; auto-complete-ruby
    inf-ruby
    ac-inf-ruby
    rdoc-mode
    robe-mode
    helm-robe
    ;; rvm ; (when (executable-find "rvm") ...)
    rubocop
    pry
    ;; ruby-complexity
    enh-ruby-mode
    ruby-tools
    rspec-mode
    feature-mode
    ;; ruby-end
    ruby-hash-syntax
    ruby-refactor
    splitjoin
    ;; flymake-ruby
    yari
    bundler
    yard-mode
    ;; dbgr ; common debug mode
    ;; emacs-refactor
    dumb-jump

    ;; ~ RAILS ~
    ;; helm-rails ; (when (executable-find "rails") ...)
    projectile-rails
    ;; projectile-rails-robe
    rails-new
    ;; TODO:
    ;; rails-log-mode
    ;; omniref
    ;; realgud

    ;; ~ SCALA ~
    scala-mode
    sbt-mode
    ensime

    ;; Groovy
    groovy-emacs-mode

    ;; ~ CLOJURE ~
    clojure-mode
    clojure-snippets
    cider
    ac-cider
    helm-cider
    ;; clj-refactor
    edn
    flycheck-clojure

    ;; ~ COMMON LISP ~
    ;; slime
    ;; ac-slime

    ;; ~ WEB ~
    ac-html
    ;; protobuf-mode ;; ?
    restclient
    json-mode
    markdown-mode
    poly-markdown
    poly-org
    ;; mmm-mode
    ;; poly-erb
    ;; poly-slim
    js2-mode
    coffee-mode
    yaml-mode
    haml-mode
    slim-mode
    sass-mode
    rainbow-mode
    rainbow-delimiters
    htmlize
    emacs-w3m ; (when (executable-find "w3m") ...)
    web-mode)
  "List of packages to sync.")

;; local recipes
(eval-after-load 'el-get
  '(add-to-list 'el-get-recipe-path "~/.emacs.d/recipes"))

;; bootstrap el-get if necessary
(require 'getelget)

;; End there
(provide 'plugins)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; plugins.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
