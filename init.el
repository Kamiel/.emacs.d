;;; init.el --- The EMACS initialization file -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;;; Commentary:
;;
;; Copyright (C) 2012-2015  Yuriy V. Pitomets < pitometsu@gmail.com >
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)


;; Setup load path

(defvar user-emacs-directory "~/.emacs.d/" "Path to init EMACS directory.")
(add-to-list 'load-path
             (concat
              (file-name-as-directory user-emacs-directory) "site-lisp"))


;; Load configuration

;; plugins
(require 'plugins)

;; hacks
(require 'hacks)

;; hooks
(require 'hooks)

;; encrypted customization
;;; (require 'custom-secrets)


;; User configuration

;; keep emacs custom-settings in separate file
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)


;; Load libs | TODO: speedup -- maybe replace with autoloads?

;; subWords
(subword-mode 1)

;; turn off annoying electric
;; (setq-default electric-indent-inhibit t)

;; unique buffer names
(require 'uniquify)

;; dired-x
(require 'dired-x)

;; gcopy-from-above-command
(require 'misc)

;; second virtual cursor
(require 'vcursor)

;; keybindings
(require 'keybindings)

;; All done
(require 'done)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; init.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
