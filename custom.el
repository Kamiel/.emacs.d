;;; custom.el --- user customizations -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;;; Commentary:
;;
;; Copyright (C) 2012-2015  Yuriy V. Pitomets < pitometsu@gmail.com >
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:


;; Faces

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:height 113 :family "Roboto Mono" :weight light))))
 '(bold ((t (:weight regular))))
 '(ensime-breakpoint-face ((t (:background "#eecccc"))))
 '(ensime-implicit-highlight ((t (:inherit italic))))
 '(ensime-writable-value-face ((t (:inherit bold))))
 '(rainbow-delimiters-base-face ((t (:inherit bold)))))


;; Variables

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-show-menu 0.07)
 '(ac-auto-start 1)
 '(ac-delay 0.01)
 '(ac-dictionary-directories
   '("~/.emacs.d/el-get/auto-complete/dict" "~/.emacs.d/ac-dict"))
 '(ac-max-width 0.5)
 '(ac-menu-height 12)
 '(ac-modes
   '(emacs-lisp-mode lisp-mode lisp-interaction-mode slime-repl-mode c-mode cc-mode c++-mode java-mode malabar-mode clojure-mode scala-mode scheme-mode sclang-mode ocaml-mode tuareg-mode haskell-mode perl-mode cperl-mode python-mode ruby-mode enh-ruby-mode ecmascript-mode javascript-mode js-mode js2-mode php-mode css-mode makefile-mode sh-mode fortran-mode f90-mode ada-mode xml-mode sgml-mode ts-mode objc-mode web-mode cider-mode cider-repl-mode))
 '(ac-quick-help-delay 0.01)
 '(ac-quick-help-prefer-pos-tip t)
 '(ack-and-a-half-arguments '(" --column --smart-case "))
 '(ack-and-a-half-executable "ack")
 '(ack-and-a-half-use-environment t)
 '(aggressive-indent-excluded-modes
   '(inf-ruby-mode makefile-mode makefile-gmake-mode text-mode yaml-mode haskell-mode haskell-interactive-mode python-mode sass-mode slim-mode markdown-mode scala-mode))
 '(align-to-tab-stop nil)
 '(ansi-color-names-vector
   ["#032F2F" "#cc252a" "#1a7b17" "#9d5a1d" "#206db0" "#b522ba" "#157676" "#b7c2b6"])
 '(anything-ack-command "ack --nogroup --nocolor --column --smart-case")
 '(apropos-do-all t)
 '(auto-hscroll-mode t)
 '(auto-indent-assign-indent-level 2)
 '(auto-indent-assign-indent-level-variables nil)
 '(auto-indent-backward-delete-char-behavior 'untabify)
 '(auto-indent-blank-lines-on-move nil)
 '(auto-indent-disabled-modes-list
   '(compilation-mode conf-windows-mode diff-mode inferior-ess-mode dired-mode eshell-mode markdown-mode fundamental-mode log-edit-mode makefile-gmake-mode org-mode snippet-mode texinfo-mode text-mode wl-summary-mode haskell-mode haskell-interactive-mode nil))
 '(auto-indent-home-is-beginning-of-indent nil)
 '(auto-indent-key-for-end-of-line-insert-char-then-newline "")
 '(auto-indent-key-for-end-of-line-then-newline "")
 '(auto-indent-kill-line-at-eol nil)
 '(auto-indent-kill-remove-extra-spaces nil)
 '(auto-indent-known-indent-level-variables
   '(c-basic-offset lisp-body-indent sgml-basic-offset python-indent python-indent-offset))
 '(auto-indent-known-text-modes
   '(text-mode message-mode fundamental-mode texinfo-mode conf-windows-mode LaTeX-mode latex-mode TeX-mode tex-mode outline-mode nroww-mode sclang-mode))
 '(auto-indent-next-pair-timer-interval
   '((Man-mode 1.5)
     (workspace-controller-mode 1.5)
     (js-mode 1.5)
     (w3m-mode 1.5)
     (el-get-package-menu-mode 1.5)
     (wl-summary-mode 1.5)
     (egg-status-buffer-mode 1.5)
     (ack-and-a-half-mode 1.5)
     (debugger-mode 1.5)
     (fundamental-mode 1.5)
     (apropos-mode 1.5)
     (org-mode 1.5)
     (help-mode 1.5)
     (sclang-mode 1.5)
     (markdown-mode 1.5)
     (Custom-mode 1.5)
     (objc-mode 1.5)
     (nxml-mode 1.5)
     (emacs-lisp-mode 1.5)
     (default 0.0005)))
 '(auto-indent-untabify-on-save-file nil)
 '(auto-indent-use-text-boundaries nil)
 '(blank-style '(color))
 '(blink-cursor-mode nil)
 '(browse-url-browser-function 'w3m-goto-url-new-session)
 '(browse-url-generic-program "firefox")
 '(c-auto-align-backslashes t)
 '(c-default-style
   '((objc-mode . "linux")
     (java-mode . "java")
     (awk-mode . "awk")
     (other . "gnu")))
 '(cc-other-file-alist
   '(("\\.cc\\'"
      (".hh" ".h"))
     ("\\.hh\\'"
      (".cc" ".C"))
     ("\\.c\\'"
      (".h"))
     ("\\.h\\'"
      (".c" ".cc" ".C" ".CC" ".cxx" ".cpp" ".m" ".mm" ".M" ".MM"))
     ("\\.m\\'"
      (".h"))
     ("\\.M\\'"
      (".H" ".h"))
     ("\\.mm\\'"
      (".h"))
     ("\\.MM\\'"
      (".H" ".h"))
     ("\\.C\\'"
      (".H" ".hh" ".h"))
     ("\\.H\\'"
      (".C" ".CC" ".M" ".MM"))
     ("\\.CC\\'"
      (".HH" ".H" ".hh" ".h"))
     ("\\.HH\\'"
      (".CC"))
     ("\\.c\\+\\+\\'"
      (".h++" ".hh" ".h"))
     ("\\.h\\+\\+\\'"
      (".c++"))
     ("\\.cpp\\'"
      (".hpp" ".hh" ".h"))
     ("\\.hpp\\'"
      (".cpp"))
     ("\\.cxx\\'"
      (".hxx" ".hh" ".h"))
     ("\\.hxx\\'"
      (".cxx"))))
 '(character-set "utf-8")
 '(cider-repl-display-help-banner nil)
 '(cider-repl-history-recenter t)
 '(coding-system "utf-8")
 '(col-highlight-period 0.6)
 '(column-number-mode t)
 '(comint-prompt-read-only t)
 '(comint-scroll-show-maximum-output nil)
 '(comment-auto-fill-only-comments t)
 '(company-idle-delay 0.1)
 '(company-minimum-prefix-length 1)
 '(company-selection-wrap-around t)
 '(company-show-numbers nil)
 '(company-sourcekit-verbose nil)
 '(company-tooltip-flip-when-above t)
 '(company-tooltip-idle-delay 0.1)
 '(company-tooltip-limit 16)
 '(company-tooltip-offset-display 'lines)
 '(confirm-nonexistent-file-or-buffer nil)
 '(counsel-mode t)
 '(counsel-mode-override-describe-bindings t)
 '(counsel-yank-pop-separator "
")
 '(cua-enable-cua-keys nil)
 '(cua-mode nil nil (cua-base))
 '(current-language-environment "UTF-8")
 '(custom-buffer-done-kill t)
 '(custom-enabled-themes '(myrth))
 '(custom-safe-themes
   '("31bcdc6a88c28e9449a1ff621e090c65cefa8daf37b350665f0ebb12d3a14d7a" "b6e9a17454dba68ae20f47a0cf02a96370bacc2f37f9de58044a381a18e7de90" "c0ae50d71a1eab3b4283ffa0921ad6df1f3d42dc02c36ae6192d38aff16f7472" "3bd817617038d8fdb5f940b7ea55df94d084c7a735eb06b383a5136350e8b6d3" "6a9abdd46cc547cfd2d4236e173bae6c93dc14a6c5b323bfba1f8742103f8738" "b289baccc2e92df2bd624d5c887bb295e5bc9423cd5f1a2ca639ee05426b7854" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "e7cf00bac6f782273b4e2d46fb2e3884b273720b29d5a7616e4f0ff63648cfb3" default))
 '(custom-theme-directory "~/.emacs.d/themes")
 '(default-frame-alist '((width . 100) (height . 50)))
 '(default-indicate-empty-lines t t)
 '(default-input-method "utf-8")
 '(deft-directory "~/Dropbox/Org")
 '(deft-extension "org")
 '(deft-text-mode 'org-mode)
 '(deft-use-filename-as-title t)
 '(delete-by-moving-to-trash t)
 '(delete-selection-mode t)
 '(delim-pad-mode t)
 '(develock-ruby-font-lock-keywords
   '((develock-find-long-lines
      (1 'develock-long-line-1 t nil)
      (2 'develock-long-line-2 t nil))
     (develock-find-tab-or-long-space
      (1 'develock-whitespace-2)
      (2 'develock-whitespace-3 nil t))
     ("[^
 ]\\([   ]+\\)$"
      (1 'develock-whitespace-1 t nil))
     ("\\( +\\)\\(  +\\)"
      (1 'develock-whitespace-1 t nil)
      (2 'develock-whitespace-2 t nil))
     ("\\(  \\)     "
      (1 'develock-whitespace-2 append nil))
     ("^[    ]+$"
      (0 'develock-whitespace-2 append nil))))
 '(dired-details-hidden-string "")
 '(dired-details-hide-link-targets nil)
 '(dired-dwim-target t)
 '(dired-listing-switches "--group-directories-first -alh")
 '(dired-omit-files "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\.")
 '(dired-recursive-copies 'always)
 '(dired-recursive-deletes 'always)
 '(direx:closed-icon "▸ " t)
 '(direx:leaf-icon "○ " t)
 '(direx:open-icon "▾ " t)
 '(dirtree-windata '(frame left 0.25 delete))
 '(display-buffer-reuse-frames t)
 '(display-line-numbers-type 'relative)
 '(drag-stuff-global-mode t)
 '(ecb-options-version "2.40")
 '(echo-keystrokes 0.7)
 '(ediff-grab-mouse t)
 '(ediff-highlight-all-diffs t)
 '(ediff-split-window-function 'split-window-horizontally)
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(egg-enable-tooltip t)
 '(ein:use-auto-complete nil)
 '(ein:use-auto-complete-superpack nil)
 '(eldoc-idle-delay 1.2)
 '(electric-indent-mode t)
 '(elmo-mime-display-as-is-coding-system 'utf8)
 '(elscreen-buffer-to-nickname-alist
   '(("[Ss]hell" . "shell")
     ("compilation" . "compile")
     ("-telnet" . "telnet")
     ("dict" . "OnlineDict")
     ("*WL:Message*" . "Wanderlust")
     ("*Deft*" . "Deft")))
 '(elscreen-default-buffer-initial-major-mode 'deft-mode)
 '(elscreen-default-buffer-name "*Deft*")
 '(elscreen-display-tab nil)
 '(enable-recursive-minibuffers t)
 '(enh-ruby-extra-keywords
   '("debugger" "private" "public" "protected" "raise" "next" "unless"))
 '(ensime-completion-style 'auto-complete)
 '(ensime-outline-mode-in-events-buffer t)
 '(ensime-overlays-use-font-lock t)
 '(ensime-sbt-command "sbt -jvm-debug 5008")
 '(ensime-search-interface 'ivy)
 '(ensime-startup-notification nil)
 '(ergoemacs-keyboard-layout "programmer-dv")
 '(ergoemacs-mode-used "5.13.12-4")
 '(ergoemacs-theme "guru")
 '(escreen-new-screen-default-buffer "*Messages*")
 '(eshell-prompt-function
   (lambda nil
     (let
         ((user-uid-face
           (if
               (zerop
                (user-uid))
               '((t
                  (:inherit
                   (eshell-ls-missing eshell-prompt bold))))
             '((t
                (:inherit
                 (eshell-ls-directory eshell-prompt bold)))))))
       (concat
        (propertize
         (or
          (ignore-errors
            (format "[%s] "
                    (vc-call-backend
                     (vc-responsible-backend default-directory)
                     'mode-line-string default-directory)))
          "")
         'face
         '((t
            (:inherit
             (eshell-ls-clutter eshell-prompt)))))
        (propertize
         (eshell/pwd)
         'face
         '((t
            (:inherit
             (eshell-prompt)))))
        (propertize
         (format-time-string " | %Y-%m-%d %H:%M "
                             (current-time))
         'face
         '((t
            (:inherit
             (org-hide eshell-prompt)))))
        (propertize "
" 'face
'((t
   (:inherit eshell-prompt))))
        (propertize user-login-name 'face
                    '((t
                       (:inherit
                        (eshell-ls-symlink eshell-prompt)))))
        (propertize "@" 'face user-uid-face)
        (propertize system-name 'face
                    '((t
                       (:inherit
                        (eshell-ls-executable eshell-prompt)))))
        (if
            (=
             (user-uid)
             0)
            (propertize " ■" 'face user-uid-face)
          (propertize " ●" 'face user-uid-face))
        " "))))
 '(eshell-prompt-regexp "^[^■●]* [■●] ")
 '(eshell-visual-commands
   '("vi" "screen" "top" "less" "more" "lynx" "ncftp" "pine" "tin" "trn" "elm" "htop" "mc"))
 '(etm-dont-activate t)
 '(etm-use-goto-line t)
 '(evil-intercept-esc t)
 '(exec-path-from-shell-variables '("PATH" "MANPATH" "LC_ALL"))
 '(explicit-shell-file-name "bash")
 '(fancy-splash-image "~/.emacs.d/splash.png")
 '(fast-lock-cache-directories '("~/.emacs.d/emacs-flc") t)
 '(fci-always-use-textual-rule nil)
 '(fci-blank-char 8202)
 '(fci-eol-char 8201)
 '(fci-handle-truncate-lines t)
 '(fci-rule-color "#CBCBCB")
 '(fci-rule-use-dashes nil)
 '(fci-rule-width 2)
 '(fic-highlighted-words '("FIXME" "FIXED" "TODO" "BUG" "OMG" "WTF" "!!!" "???"))
 '(fill-column 80)
 '(flycheck-automatically-display-error-at-point nil)
 '(flycheck-command-wrapper-function 'identity)
 '(flycheck-disabled-checkers '(ruby-rubylint))
 '(flycheck-ghc-stack-use-nix t)
 '(flycheck-markdown-markdownlint-cli-executable nil)
 '(flycheck-scala-scalastyle-executable "sbt scalastyle")
 '(flymake-cursor-auto-enable t)
 '(flymake-gui-warnings-enabled nil)
 '(flymake-no-changes-timeout 0.5)
 '(flyspell-abbrev-p t)
 '(frame-background-mode 'light)
 '(fringe-mode nil nil (fringe))
 '(ghc-core-program "stack ghc")
 '(git--timer-sec 0.4)
 '(global-ace-isearch-mode t)
 '(global-anzu-mode t)
 '(global-discover-mode t)
 '(global-eldoc-mode nil)
 '(global-git-gutter+-mode t)
 '(global-git-gutter-mode t)
 '(global-mark-ring-max 256)
 '(global-smart-tab-mode t)
 '(global-widen-window-mode t)
 '(golden-ratio-exclude-buffer-names nil)
 '(golden-ratio-exclude-modes '("speedbar-mode"))
 '(golden-ratio-extra-commands
   '(windmove-left windmove-right windmove-down windmove-up mouse-drag-region))
 '(golden-ratio-inhibit-functions nil)
 '(golden-ratio-mode t)
 '(google-translate-default-source-language "en")
 '(google-translate-default-target-language "ru")
 '(google-translate-enable-ido-completion nil)
 '(google-translate-show-phonetic t)
 '(grep-command "egrep -s --colour=auto")
 '(grep-o-matic-search-patterns
   '("*.cpp" "*.c" "*.h" "*.awk" "*.sh" "*.py" "*.pl" "[Mm]akefile" "*.el" "*.md"))
 '(guide-key-mode t)
 '(guide-key-tip/enabled t)
 '(guide-key/guide-key-sequence
   '("M-s" "C-x" "C-c" "ESC" "<escape>" "M-ESC" "M-g" "<f1>" "s-p" "s-l" "s-m"))
 '(guide-key/idle-delay 0.9)
 '(guide-key/popup-window-position 'bottom)
 '(guide-key/recursive-key-sequence-flag t)
 '(haskell-check-command "hlint")
 '(haskell-compile-cabal-build-alt-command
   "cd %s && stack clean && stack build --ghc-options=-ferror-spans")
 '(haskell-compile-cabal-build-command "cd %s && stack build --ghc-options=-ferror-spans")
 '(haskell-completing-read-function 'helm--completing-read-default)
 '(haskell-doc-show-global-types t)
 '(haskell-doc-use-inf-haskell nil)
 '(haskell-font-lock-symbols nil)
 '(haskell-hoogle-command "hoogle")
 '(haskell-hoogle-url "http://hoogle.haskell.org/?hoogle=%s&scope=set%3Astackage")
 '(haskell-indent-offset 2)
 '(haskell-indentation-ifte-offset 2)
 '(haskell-indentation-layout-offset 2)
 '(haskell-indentation-left-offset 2)
 '(haskell-indentation-where-post-offset 2)
 '(haskell-indentation-where-pre-offset 2)
 '(haskell-interactive-mode-eval-mode 'haskell-mode)
 '(haskell-interactive-prompt "λ > ")
 '(haskell-package-manager-name "stack exec -- ghc-pkg")
 '(haskell-process-args-cabal-repl
   '("--ghc-option=-ferror-spans" "--ghci-options=\" -ghci-script .ghci-script\"" "--with-ghc=ghci"))
 '(haskell-process-args-stack-ghci
   '("--ghc-options=-ferror-spans" "--ghci-options=\" -ghci-script .ghci-script\"" "--with-ghc=ghci"))
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-path-cabal "cabal")
 '(haskell-process-path-ghci "ghci")
 '(haskell-process-suggest-haskell-docs-imports t)
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-process-type 'stack-ghci)
 '(haskell-program-name
   "stack ghci --ghci-options=\" -ghci-script .ghci_conf\" --with-ghc=ghci")
 '(haskell-stylish-on-save nil)
 '(haskell-tags-on-save nil)
 '(helm-M-x-fuzzy-match t)
 '(helm-adaptive-history-length 128)
 '(helm-adaptive-mode t nil (helm-adaptive))
 '(helm-ag-base-command "ag")
 '(helm-ag-command-option "--nocolor --nogroup --all-text --")
 '(helm-ag-insert-at-point 'symbol)
 '(helm-always-two-windows nil)
 '(helm-apropos-fuzzy-match t)
 '(helm-buffer-max-length nil)
 '(helm-buffers-favorite-modes
   '(lisp-interaction-mode emacs-lisp-mode text-mode org-mode objc-mode))
 '(helm-buffers-fuzzy-matching t)
 '(helm-candidate-number-limit 128)
 '(helm-candidate-separator "")
 '(helm-default-external-file-browser "open")
 '(helm-ff-auto-update-initial-value nil)
 '(helm-ff-lynx-style-map nil)
 '(helm-for-files-preferred-list
   '(helm-c-source-ffap-line helm-c-source-ffap-guesser helm-c-source-buffers-list helm-c-source-recentf helm-c-source-bookmarks helm-c-source-file-cache helm-c-source-files-in-current-dir helm-c-source-locate helm-c-source-buffer-not-found))
 '(helm-fuzzier-mode t)
 '(helm-home-url "http://www.google.com")
 '(helm-lisp-fuzzy-completion t)
 '(helm-locate-command "locate %s -r %s")
 '(helm-mode nil)
 '(helm-mode-fuzzy-match t)
 '(helm-mp-matching-method 'multi3)
 '(helm-quick-update t)
 '(helm-reuse-last-window-split-state nil)
 '(helm-scroll-amount 1)
 '(helm-split-window-default-side 'below)
 '(helm-split-window-inside-p t)
 '(helm-time-zone-home-location "Kiev")
 '(helm-top-command "env COLUMNS=%s top -s1 -o cpu -R -F -l1 ")
 '(hi2-show-indentations-after-eol nil)
 '(highlight-indent-guides-method 'column)
 '(highlight-indentation-offset 2)
 '(highlight-symbol-idle-delay 0.5)
 '(highlight-symbol-on-navigation-p t)
 '(highline-line nil)
 '(highline-priority 0)
 '(history-delete-duplicates t)
 '(history-length 100)
 '(hs-lint-command "stack exec -- hlint")
 '(hs-lint-replace-with-suggestions t)
 '(hscroll-step 1)
 '(icicle-mode t)
 '(ido-auto-merge-work-directories-length -1)
 '(ido-create-new-buffer 'always)
 '(ido-enable-flex-matching nil)
 '(ido-enable-prefix nil)
 '(ido-enable-regexp t)
 '(ido-everywhere nil)
 '(ido-hacks-mode t)
 '(ido-save-directory-list-file "~/.emacs.d/ido.last")
 '(ido-ubiquitous-function-exceptions '(grep-read-files ediff))
 '(ido-ubiquitous-mode nil)
 '(ido-yes-or-no-mode t)
 '(imenu-auto-rescan t)
 '(imenu-auto-rescan-maxout 600000)
 '(imenu-sort-function nil)
 '(imenu-tree-auto-update t)
 '(indent-tabs-mode nil)
 '(indicate-buffer-boundaries '((top . left) (bottom . left) (up . left) (down . left)))
 '(inferior-haskell-find-project-root t)
 '(inferior-haskell-wait-and-jump t)
 '(inhibit-startup-echo-area-message "user")
 '(inhibit-startup-screen t)
 '(initial-buffer-choice nil)
 '(initial-frame-alist '((width . 100) (height . 50)))
 '(initial-scratch-message
   ";;
;;      `~NETSU~'          `-=EMACS=-'
;;
;;
;;        (\"`-'  '-/\") .___..--' ' \"`-._
;;         ` *_ *  )    `-.   (      ) .`-.__. `)
;;         (_Y_.) ' ._   )   `._` ;  `` -. .-'
;;      _.. '--'_..-_/   /-~' _ .' ,4
;;   ( i l ),-''  ( l i),'  ( ( ! .-'
;;
;;

")
 '(ivy-height 12)
 '(ivy-mode t)
 '(ivy-use-selectable-prompt t)
 '(jiralib-host "")
 '(jiralib-url "https://menswearhouse.atlassian.net")
 '(json-reformat:indent-width 2)
 '(jump-char-backward-key "<")
 '(jump-char-forward-key ">")
 '(keyfreq-autosave-mode t)
 '(keyfreq-file "~/.emacs.d/keyfreq")
 '(keyfreq-mode t)
 '(linum+-dynamic-format "  %%%dd")
 '(linum+-smart-format "  %%%dd")
 '(linum-delay nil)
 '(linum-disabled-modes-list
   '(eshell-mode wl-summary-mode compilation-mode org-mode text-mode dired-mode minibuffer-inactive-mode workspace-controller-mode))
 '(longlines-wrap-follows-window-size t)
 '(ls-lisp-dirs-first t)
 '(lsp-enable-semantic-highlighting t)
 '(lsp-haskell-server-path "haskell-language-server")
 '(lsp-haskell-server-wrapper-function 'identity)
 '(lsp-modeline-code-action-fallback-icon "[!]")
 '(lsp-ui-doc-alignment 'window)
 '(lsp-ui-sideline-actions-icon nil)
 '(lsp-ui-sideline-show-code-actions t)
 '(lsp-ui-sideline-show-diagnostics t)
 '(lsp-ui-sideline-show-hover nil)
 '(lsp-ui-sideline-show-symbol t)
 '(mac-input-method-mode nil)
 '(magit-completing-read-function 'ivy-completing-read)
 '(mail-signature t)
 '(mail-signature-file "~/.emacs.d/wanderlust/signature")
 '(major-mode 'text-mode)
 '(make-backup-files nil)
 '(mark-ring-max 128)
 '(markdown-command "pandoc")
 '(max-lisp-eval-depth 10000)
 '(max-specpdl-size 32768)
 '(menu-bar-mode nil)
 '(merlin-ac-setup t)
 '(merlin-locate-focus-new-window t)
 '(merlin-locate-in-new-window 'never)
 '(minibuffer-frame-alist '((width . 80) (height . 2)))
 '(minimap-always-recenter t)
 '(minimap-hide-fringes t)
 '(minimap-update-delay 0.2)
 '(minimap-width-fraction 0.14)
 '(minimap-window-location 'right)
 '(mk-proj-ack-cmd "ack")
 '(mk-proj-use-ido-selection t)
 '(mouse-wheel-scroll-amount
   '(1
     ((shift)
      . 4)
     ((control))
     ((meta)
      . 0.5)
     ((alt)
      . 0.5)
     ((super))
     ((hyper))))
 '(mu-worlds
   '(["\320\241\321\204\320\265\321\200\320\260 \320\234\320\270\321\200\320\276\320\262" "sow.igrohost.ru" 4662 "netsu" "mudLan47ternus"]))
 '(mud-history-max 125)
 '(multi-term-program "bash")
 '(nix-buffer-root-file "shell.nix")
 '(nix-haskell-flycheck t)
 '(nxml-child-indent 4)
 '(org-completion-use-ido t)
 '(org-default-notes-file "~/Dropbox/Org/tasks.org")
 '(org-directory "~/Dropbox/Org")
 '(org-export-html-style "")
 '(org-export-html-style-extra
   "
<style type=\"text/css\">
<!--/*--><![CDATA[/*><!--*/

html {
    color: #475b62;
    background-color: #eeeeee;
    font-family: \"Droid Sans\", Helvetica, Verdana, Arial;
}

h1, h2, h3, h4, h5, h6,
h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
    font-family: \"Droid Sans\", Helvetica, Verdana, Arial;
}

#text-table-of-contents a {
    font-family: \"Droid Sans\", Helvetica, Verdana, Arial;
}

h3, h4, h5, h6 {
    font-family: \"Lucida Grande\", Georgia, \"Droid Serif\", \"Times New Roman\", Times;
}

h1 span, h2 span, h3 span, h4 span, h5 span, h6 span {
    font-family: \"Ubuntu Mono\", \"Inconsolata-LGC\", Inconsolata, \"Droid Sans Mono\", Menlo, Consolas, \"Liberation Mono\", \"DejaVu Sans Mono\", Monospace, Monaco, Terminus, \"Courier New\", Courier;
    font-weight: normal;
}

pre {
    border: 1pt solid #A7A6AA;
    background-color: #eeeee0;
    padding: 5pt;
    font-family: \"Ubuntu Mono\", \"Inconsolata-LGC\", Inconsolata, \"Droid Sans Mono\", Menlo, Consolas, \"Liberation Mono\", \"DejaVu Sans Mono\", Monospace, Monaco, Terminus, \"Courier New\", Courier;
    font-size: 90%;
    overflow: auto;
}

body {
    font-size: 14px;
    line-height: 1.5em;
    padding: 0;
    margin: 0;
}
h1 {
    margin: 0;
    font-size: 1.6666666666666667em;
    line-height: 0.9em;
    margin-bottom: 0.9em;
    color: #2E3C3C;
}
h2 {
    margin: 0;
    font-size: 1.5em;
    line-height: 1em;
    margin-bottom: 1em;
}
h3 {
    margin: 0;
    font-size: 1.3333333333333333em;
    line-height: 1.125em;
    margin-bottom: 1.125em;
}
h4 {
    margin: 0;
    font-size: 1.1666666666666667em;
    line-height: 1.2857142857142858em;
    margin-bottom: 1.2857142857142858em;
}
p, ul, blockquote, pre, td, th, label {
    margin: 0;
    font-size: 1em;
    line-height: 1.5em;
    margin-bottom: 1.5em;
}
p.small, #postamble {
    margin: 0;
    font-size: 0.8333333333333334em;
    line-height: 1.8em;
    margin-bottom: 1.8em;
}
table {
    border-collapse: collapse;
    margin-bottom: 1.5em;
}

#content {
    width: 70em;
    margin-left: auto;
    margin-right: auto;
}

#header {
    height: 10em;
}

#table-of-contents {
    color: #475b62;
    width: 15em;
    float: left;
    overflow: auto;
}

div.outline-2 {
        width: 52em;
    float: right;
    position: relative;
}

#postamble {
    color: #7f7f7f;
    clear: both;
    text-align: center;
}

div.outline-2 pre {
    width: 40em;
    overflow: auto;
}

h1.title {
    margin-top: 10px;
    text-align: center;
}

h1.title {
    font-size: 4em;
    font-weight: bold;
    letter-spacing: -0.1em;
    margin-bottom: 0.2em;
}

.todo {
    color: red;
}

.done {
    color: green;
}

.tag {
    color: blue;
    text-transform: lowercase;
    background: #fff;
    border: none;
}

.timestamp {
}

.timestamp-kwd  {

}

.target {

}

#table-of-contents h2 {
    letter-spacing: -0.1em;
}

#table-of-contents ul,
#table-of-contents ol {
    padding-left: 1em;
}

.outline-2 h2 {
    color: #2E3C3C;
    background: #c1cdc1;
    border: none;
}

.outline-2 h2, .outline-2 h3 {
    letter-spacing: -0.05em;
}

.outline-2 {
    padding: 5px;
}

td {
    border: 1px solid #ccc;
}

h1 span, h2 span, h3 span, h4 span, h5 span, h6 span {
    background-color: #E2E1D5;
    padding: 2px;
    border: none;
}

.outline-1, .outline-2, .outline-3, .outline-4, .outline-5, .outline-6 {
    margin-left: 2em;
}

a {
    text-decoration: none;
    color: #57d;
}

a:hover {
    border-bottom: 1px dotted #57d;
}

#postamble p {
    margin: 0px;
}

  /*]]>*/-->
</style>
")
 '(org-export-htmlize-output-type 'inline-css)
 '(org-export-with-section-numbers nil)
 '(org-hide-leading-stars t)
 '(org-log-done 'time)
 '(org-modules
   '(org-bbdb org-bibtex org-docview org-gnus org-info org-irc org-mhe org-rmail org-w3m org-mouse))
 '(org-replace-disputed-keys t)
 '(org-src-fontify-natively t)
 '(org-support-shift-select 'always)
 '(org-todo-keyword-faces
   '(("TODO" . "Red2")
     ("PROGRESS" . "Orange2")
     ("SUSPEND" . "Magenta2")
     ("CANCEL" . "Dark Cyan")
     ("DONE" . "Dark Green")))
 '(org-todo-keywords
   '((sequence "TODO(t)" "PROGRESS(p)" "SUSPEND(s)" "|" "CANCEL(c)" "DONE(d)")))
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa" . "https://melpa.org/packages/")
     ("ELPA" . "https://tromey.com/elpa/")
     ("SC" . "https://joseito.republika.pl/sunrise-commander/")))
 '(package-selected-packages '(rainbow-mode inflections adaptive-wrap))
 '(paren-highlight-offscreen t)
 '(paren-message-show-linenumber 'sexp)
 '(persp-keymap-prefix "P")
 '(persp-nil-name "emacs")
 '(persp-use-workgroups t)
 '(popwin-mode t)
 '(popwin:adjust-other-windows t)
 '(popwin:popup-window-height 12)
 '(popwin:reuse-window 'current)
 '(popwin:special-display-config
   '(("*Async Shell Command*" :regexp nil)
     ("\\*ansi-term\\*.*" :regexp t)
     (eshell-mode :regexp nil)
     (term-mode :regexp nil)
     (inferior-python-mode :regexp nil)
     (comint-mode :regexp nil)
     (anaconda-mode-view-mode :regexp nil)
     ("*Org Links*" :regexp nil)
     ("*Flycheck error messages*" :regexp nil)
     ("*GHC Error*" :regexp nil)
     ("*Intero-Help*" :regexp nil)
     ("*manage-minor-mode*" :regexp nil)
     ("*yank-pop*" :regexp nil)
     (speedbar-mode :regexp nil :position left :dedicated t)
     (direx:direx-mode :regexp nil :width 40 :position left)
     ("^\\*helm.*\\*$" :regexp t)
     ("*Kill Ring*")
     (help-mode)
     (completion-list-mode :noselect t)
     (compilation-mode :noselect t)
     (ripgrep-search-mode :regexp nil)
     (grep-mode :noselect t)
     (occur-mode :noselect t)
     ("*Pp Macroexpand Output*" :noselect t)
     ("*Shell Command Output*")
     ("*vc-diff*")
     ("*vc-change-log*")
     (" *undo-tree*" :width 60 :position right)
     ("^\\*anything.*\\*$" :regexp t)
     ("*slime-apropos*")
     ("*slime-macroexpansion*")
     ("*slime-description*")
     ("*slime-compilation*" :noselect t)
     ("*slime-xref*")
     (sldb-mode :stick t)
     (slime-repl-mode)
     (slime-connection-list-mode)
     ("*Buffer List*" :regexp nil)
     ("*el-get packages*" :regexp nil)
     ("*Packages*" :regexp nil)))
 '(powerline-default-separator nil)
 '(powerline-text-scale-factor 1.2)
 '(pp^L-^L-string "
")
 '(pp^L-^L-string-pre "")
 '(predictive-main-dict 'dict-english)
 '(pretty-control-l-mode t)
 '(prj-autotracking nil)
 '(prj-keybindings
   '(([f5]
      eproject-setup-toggle always)
     ([s-C-right]
      eproject-nextfile nil)
     ([s-C-left]
      eproject-prevfile nil)
     ([C-f5]
      eproject-dired nil)))
 '(prj-set-framepos t)
 '(project-mode t)
 '(project-proj-files-dir "~/.emacs.d/projects")
 '(project-search-exclusion-regexes-default
   '("~$" "#$" "^\\." "^Builds$" "^Doc$" "\\.git" "\\.xcdatamodeld$" "\\.xib$" "\\.storyboard$" "\\.png$" "\\.gif$" "\\.jpg$" "\\.jpeg$" "\\.tmp$" "\\bTAGS\\b"))
 '(project-tags-form-default '(".*" ('ignore)))
 '(projectile-completion-system 'ivy)
 '(projectile-enable-caching t)
 '(projectile-global-mode t)
 '(projectile-project-root-functions
   '(projectile-root-local projectile-root-top-down projectile-root-bottom-up projectile-root-top-down-recurring))
 '(projectile-rails-expand-snippet t)
 '(projectile-tags-command "etags  --declarations -a %s -o %s")
 '(projectile-use-git-grep t)
 '(pytest-cmd-flags "-s -v --pdbcls=IPython.terminal.debugger:Pdb")
 '(pytest-global-name "pipenv run pytest")
 '(pytest-project-root-files '("setup.py" ".hg" ".git" "Pipfile" "tests"))
 '(python-indent-offset 4)
 '(python-shell-interpreter "ipython")
 '(python-shell-interpreter-args "-i --simple-prompt")
 '(python-shell-interpreter-interactive-arg "-i --simple-prompt")
 '(rake-completion-system 'helm)
 '(realgud:ipdb-command-name
   "python -c \"from IPython import start_ipython; from sys import argv; start_ipython(['--simple-prompt', '-i', '-c', '%run -d {}'.format(' '.join(argv[1:]))])\"")
 '(recentf-auto-cleanup 'never)
 '(recentf-exclude '("ido.last" ".ido.last"))
 '(recentf-max-menu-items 48)
 '(recentf-max-saved-items 48)
 '(recentf-mode t)
 '(recentf-save-file "~/.emacs.d/recentf")
 '(regex-tool-backend 'perl)
 '(require-final-newline 'ask)
 '(revert-without-query '(".*.m" ".*.h" ".*.plist" ".*.mm"))
 '(rm-blacklist
   '(" hl-p" " mate" " WLR" " UT" " VHl" " hl" " Helm" " Anzu" " Projectile"))
 '(rm-excluded-modes
   '(" hl-p" " mate" " WLR" " UT" " VHl" " hl" " Helm" " Anzu" " Projectile"))
 '(rsense-home "/home/yuriy/.emacs.d/el-get/rsense")
 '(ruby-block-delay 0)
 '(ruby-block-highlight-face 'highlight-symbol-face)
 '(ruby-block-highlight-toggle t)
 '(rvm-interactive-completion-function 'helm--completing-read-default)
 '(rvm-interactive-find-file-function 'helm-find-files)
 '(safe-local-variable-values
   '((eval progn
           (setq merlin-command
                 (let
                     ((project-root
                       (or
                        (when
                            (require 'projectile)
                          (projectile-project-root))
                        (file-name-directory
                         (let
                             ((d
                               (dir-locals-find-file ".")))
                           (if
                               (stringp d)
                               d
                             (car d))))
                        (pwd))))
                   (concat project-root "_opam/bin/ocamlmerlin")))
           (setq ocp-indent-path
                 (let
                     ((project-root
                       (or
                        (when
                            (require 'projectile)
                          (projectile-project-root))
                        (file-name-directory
                         (let
                             ((d
                               (dir-locals-find-file ".")))
                           (if
                               (stringp d)
                               d
                             (car d))))
                        (pwd))))
                   (concat project-root "_opam/bin/ocp-indent"))))
     (eval progn
           (setq ocamlformat-command
                 (let
                     ((project-root
                       (or
                        (when
                            (require 'projectile)
                          (projectile-project-root))
                        (file-name-directory
                         (let
                             ((d
                               (dir-locals-find-file ".")))
                           (if
                               (stringp d)
                               d
                             (car d))))
                        (pwd))))
                   (concat project-root "_opam/bin/ocamlformat"))))
     (eval progn
           (setq ocaml-format
                 (let
                     ((project-root
                       (or
                        (when
                            (require 'projectile)
                          (projectile-project-root))
                        (file-name-directory
                         (let
                             ((d
                               (dir-locals-find-file ".")))
                           (if
                               (stringp d)
                               d
                             (car d))))
                        (pwd))))
                   (concat project-root "_opam/bin/ocamlformat"))))
     (eval progn
           (setq ocp-indent-path
                 (let
                     ((project-root
                       (or
                        (when
                            (require 'projectile)
                          (projectile-project-root))
                        (file-name-directory
                         (let
                             ((d
                               (dir-locals-find-file ".")))
                           (if
                               (stringp d)
                               d
                             (car d))))
                        (pwd))))
                   (concat project-root "_opam/bin/ocp-indent"))))
     (eval setq haskell-w3m-haddock-dirs
           `(,(shell-command-to-string "stack path --local-doc-root")
             ,(shell-command-to-string "stack path --snapshot-doc-root")))
     (eval when
           (require 'projectile)
           (setq cider-repl-history-file
                 (concat
                  (projectile-project-root)
                  ".lein-repl-history")))
     (\,
      (cider-repl-history-file concat
                               (projectile-project-root)
                               ".cider-repl-history"))
     (cider-repl-history-file \,
                              (concat
                               (projectile-project-root)
                               ".cider-repl-history"))
     (cider-repl-history-file concat
                              (projectile-project-root)
                              ".cider-repl-history")
     (cider-repl-history-file
      (concat
       (projectile-project-root)
       ".cider-repl-history"))
     (cider-repl-history-file
      (concat projectile-project-root ".cider-repl-history"))
     (flycheck-disabled-checkers
      (clojure-cider-typed))))
 '(save-interprogram-paste-before-kill nil)
 '(save-place t nil (saveplace))
 '(save-place-file "~/.emacs.d/emacs-places")
 '(savehist-autosave-interval nil)
 '(savehist-mode t)
 '(sbt:program-name "sbt -jvm-debug 5008")
 '(scala-indent:align-parameters t)
 '(sclang-auto-scroll-post-buffer t)
 '(sclang-eval-line-forward nil)
 '(sclang-help-directory "/media/Applications/SuperCollider/Help")
 '(sclang-help-path '("/media/Applications/SuperCollider/Help"))
 '(sclang-runtime-directory "~/.sclang/")
 '(sclang-show-workspace-on-startup nil)
 '(sclang-source-directory "~/.sclang/")
 '(scroll-bar-mode nil)
 '(scroll-conservatively 101)
 '(scroll-margin 6)
 '(scroll-step 1)
 '(semanticdb-default-save-directory "~/.emacs.d/semanticdb/")
 '(send-mail-function 'smtpmail-send-it)
 '(server-mode t)
 '(sh-basic-offset 2)
 '(sh-indentation 2)
 '(shell-file-name "bash")
 '(shell-switcher-mode t)
 '(shift-select-mode t)
 '(shm-program-name "structured-haskell-mode" t)
 '(shm-use-hdevtools t)
 '(show-paren-mode nil)
 '(show-smartparens-global-mode t)
 '(show-trailing-whitespace nil)
 '(size-indication-mode t)
 '(smartparens-global-mode t)
 '(smex-flex-matching nil)
 '(smex-history-length 512)
 '(smex-save-file "~/.emacs.d/smex-items")
 '(sml/col-number-format "%2c")
 '(sml/line-number-format "%2l")
 '(sml/modified-char " X ")
 '(sml/name-width 44)
 '(sml/position-percentage-format " %p")
 '(sml/replacer-regexp-list
   '((":DB:Org/" ":Org:")
     ("^~/Projects/" ":Proj:")
     ("^~/\\.emacs\\.d/" ":ED:")
     ("^/sudo:.*:" ":SU:")
     ("^~/Documents/" ":Doc:")
     ("^~/Dropbox/" ":DB:")
     ("^:\\([^:]*\\):Documento?s/" ":\\1/Doc:")
     ("^~/[Gg]it/" ":Git:")
     ("^~/[Gg]it[Hh]ub/" ":Git:")
     ("^~/[Gg]it-?[Pp]rojects/" ":Git:")))
 '(sml/shorten-modes t)
 '(sml/show-client nil)
 '(sml/show-eol nil)
 '(sml/show-file-name nil)
 '(sml/theme 'respectful)
 '(smooth-scroll-margin 6)
 '(smooth-scroll-strict-margins t)
 '(sourcekit-verbose nil)
 '(sp-base-key-bindings nil)
 '(sp-wrap-repeat-last 2)
 '(sp-wrap-respect-direction t)
 '(special-display-frame-alist '((height . 14) (width . 80) (unsplittable . t)))
 '(speedbar-fetch-etags-arguments
   '("-d" "--declarations" "--globals" "--members" "-t" "-T" "--regex='/^[ \\n\\t]*#pragma^[ \\n\\t]*mark/'" "-I" "-o" "-"))
 '(speedbar-tag-hierarchy-method '(speedbar-prefix-group-tag-hierarchy))
 '(speedbar-use-images nil)
 '(sr-speedbar-max-width 32)
 '(sr-speedbar-right-side nil)
 '(sr-speedbar-width-console 32)
 '(sr-speedbar-width-x 32)
 '(sr-terminal-program "multi-term")
 '(standard-indent 2)
 '(swift-mode:basic-offset 2)
 '(swift-mode:switch-case-offset 2)
 '(tab-always-indent t)
 '(tab-width 4)
 '(temp-buffer-resize-mode t)
 '(term-buffer-maximum-size 10000)
 '(tls-checktrust t)
 '(tool-bar-mode nil)
 '(trash-directory "~/.Trash/emacs")
 '(tuareg-opam-insinuate t)
 '(undo-tree-enable-undo-in-region nil)
 '(undo-tree-mode-lighter " UT")
 '(undo-tree-visualizer-diff t)
 '(undo-tree-visualizer-timestamps t)
 '(unicode-whitespace-newline-mark-modes
   '(indented-text-mode makefile-automake-mode makefile-bsdmake-mode makefile-gmake-mode makefile-imake-mode makefile-makepp-mode makefile-mode markdown-mode org-mode snippet-mode text-mode emacs-lisp-mode ruby-mode))
 '(uniquify-buffer-name-style 'post-forward nil (uniquify))
 '(utop-command "opam config exec -- utop -emacs")
 '(viper-mode nil)
 '(visible-bell t)
 '(visual-fill-column-width nil)
 '(w3m-charset-coding-system-alist
   '((utf-8 . undecided)
     (x-sjis . shift_jis)
     (x-shift_jis . shift_jis)
     (x-shift-jis . shift_jis)
     (x-euc-jp . euc-japan)
     (shift-jis . shift_jis)
     (x-unknown . undecided)
     (unknown . undecided)
     (windows-874 . tis-620)
     (us_ascii . raw-text)
     (koi8-r . undecided)
     (windows-1251 . undecided)
     (windows-1252 . undecided)))
 '(w3m-coding-system 'utf-8)
 '(w3m-command nil)
 '(w3m-default-coding-system 'utf-8)
 '(w3m-default-display-inline-images t)
 '(w3m-default-save-directory "~/Downloads/w3m")
 '(w3m-file-coding-system 'utf-8)
 '(w3m-file-name-coding-system 'utf-8)
 '(w3m-home-page "google.com")
 '(w3m-init-file "~/.emacs.d/emacs-w3m")
 '(w3m-key-binding 'info)
 '(w3m-new-session-url "google.com")
 '(w3m-pop-up-windows nil)
 '(w3m-session-load-crashed-sessions t)
 '(w3m-session-load-last-sessions t)
 '(w3m-terminal-coding-system 'utf-8)
 '(w3m-toggle-inline-images-permanently nil)
 '(w3m-use-cookies t)
 '(w3m-use-header-line nil)
 '(w3m-use-tab nil)
 '(w3m-use-tab-menubar t)
 '(w3m-use-toolbar t)
 '(warning-minimum-level :error)
 '(warning-minimum-log-level :error)
 '(wg-control-frames nil)
 '(wg-default-session-file "~/.emacs.d/emacs_workgroups")
 '(wg-first-wg-name "emacs")
 '(wg-mode-line-use-faces t)
 '(wg-restore-scroll-bars nil)
 '(wg-session-file "~/.emacs.d/emacs_workgroups")
 '(wget-download-directory "~/Downloads")
 '(which-func-format
   '((:propertize which-func-current local-map
                  (keymap
                   (mode-line keymap
                              (mouse-3 . end-of-defun)
                              (mouse-2 . narrow-to-defun)
                              (mouse-1 . beginning-of-defun)))
                  face which-func mouse-face mode-line-highlight help-echo "mouse-1: go to beginning
mouse-2: toggle rest visibility
mouse-3: go to end")))
 '(which-func-modes
   '(c-mode c++-mode perl-mode cperl-mode python-mode makefile-mode sh-mode fortran-mode f90-mode ada-mode diff-mode objc-mode emacs-lisp-mode haskell-mode sclang-mode ruby-mode))
 '(which-function-mode t)
 '(whitespace-display-mappings
   '((space-mark 32
                 [183]
                 [46])
     (space-mark 160
                 [164])
     (newline-mark 10
                   [172 10])
     (tab-mark 9
               [183 9]
               [164 9]
               [92 9])))
 '(whitespace-line-column 80)
 '(whitespace-style
   '(face spaces indentation tabs newline space-before-tab space-after-tab indentation space-mark tab-mark newline-mark))
 '(whole-line-or-region-extensions-alist
   '((copy-region-as-kill whole-line-or-region-copy-region-as-kill nil)
     (kill-region whole-line-or-region-kill-region nil)
     (kill-ring-save whole-line-or-region-kill-ring-save nil)
     (yank whole-line-or-region-yank nil)
     (comment-or-uncomment-region whole-line-or-region-comment-dwim-2 nil)
     (ns-copy-including-secondary whole-line-or-region-kill-ring-save nil)))
 '(windmove-wrap-around t)
 '(winner-mode t nil (winner))
 '(wl-address-file "~/.emacs.d/wanderlust/addresses")
 '(wl-alias-file "~/.emacs.d/wanderlust/im/Aliases")
 '(wl-default-folder "%Inbox")
 '(wl-folder-window-width 25)
 '(wl-folders-file "~/.emacs.d/wanderlust/folders")
 '(wl-from "")
 '(wl-init-file "~/.emacs.d/wanderlust/wl")
 '(wl-local-domain "gmail.com")
 '(wl-message-ignored-field-list '("^.*"))
 '(wl-message-visible-field-list
   '("^From:" "^Organization:" "^To:" "^Cc:" "^Date:" "^Subject:" "^User-Agent:" "^X-Mailer:"))
 '(wl-summary-line-format "%n%T%P %W %D-%M-%Y %h:%m %t%[%c %f% %] %s")
 '(wl-summary-weekday-name-lang "ru")
 '(wl-summary-width 80)
 '(workgroups-mode t)
 '(x-gtk-use-system-tooltips nil)
 '(yas-prompt-functions
   '(yas-completing-prompt yas-x-prompt yas-dropdown-prompt yas-ido-prompt yas-no-prompt)))


;; Advanced flags

;; open subdir in same buffer
(put 'dired-find-alternate-file 'disabled nil)

;; downcase whole region
(put 'downcase-region 'disabled nil)

;; horisontal scroll
(put 'scroll-left 'disabled nil)

;; upcase whole region
(put 'upcase-region 'disabled nil)

;; folding the rest of code
(put 'narrow-to-defun  'disabled nil)
(put 'narrow-to-page   'disabled nil)
(put 'narrow-to-region 'disabled nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; custom.el ends here
;;; **************************************************************************
;;;; *****  EOF  *****  EOF  *****  EOF  *****  EOF  *****  EOF  *************
