;; This file is automatically generated by the multiple-cursors extension.
;; It keeps track of your preferences for running commands with multiple cursors.

(setq mc/cmds-to-run-for-all
      '(
        auto-indent-delete-char
        mouse-yank-primary
        ))

(setq mc/cmds-to-run-once
      '(
        helm-confirm-and-exit-minibuffer
        ))
