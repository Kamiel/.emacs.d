#!/bin/bash
echo start
# test args
if [ ! ${#} -ge 3 ]; then
    echo 1>&2 "Usage: ${0} LOCAL REMOTE MERGED BASE"
    echo 1>&2 "       (LOCAL, REMOTE, MERGED, BASE can be provided by \`git mergetool'.)"
    exit 1
fi
echo 1
# tools
_EMACSCLIENT=/Applications/Emacs.app/Contents/MacOS/Emacs
_BASENAME=opt/local/libexec/gnubin/basename
_CP=/opt/local/libexec/gnubin/cp
_EGREP=/opt/local/bin/egrep
_MKTEMP=/opt/local/libexec/gnubin/mktemp
echo 2
# args
_LOCAL=${1}
_REMOTE=${2}
_MERGED=${3}
if [ -r ${4} ] ; then
    _BASE=${4}
    _EDIFF=ediff-merge-files-with-ancestor
    _EVAL="${_EDIFF} \"${_LOCAL}\" \"${_REMOTE}\" \"${_BASE}\" nil \"${_MERGED}\""
else
    _EDIFF=ediff-merge-files
    _EVAL="${_EDIFF} \"${_LOCAL}\" \"${_REMOTE}\" nil \"${_MERGED}\""
fi
echo 3
# console vs. X
if [ "${TERM}" = "linux" ]; then
    unset DISPLAY
    _EMACSCLIENTOPTS="-t"
else
    _EMACSCLIENTOPTS=""
fi
echo 4
# run emacsclient
${_EMACSCLIENT} ${_EMACSCLIENTOPTS} -e "(${_EVAL})"
echo ${_EMACSCLIENT}
echo ${_EMACSCLIENTOPTS}
echo ${_EVAL}
exit 0

# check modified file
if [ ! $(egrep -c '^(<<EOF
<<EOF
<<EOF
<|=======|>>>>>>>|####### Ancestor)' ${_MERGED}) = 0 ]; then
    _MERGEDSAVE=$(${_MKTEMP} --tmpdir `${_BASENAME} ${_MERGED}`.XXXXXXXXXX)
    ${_CP} ${_MERGED} ${_MERGEDSAVE}
    echo 1>&2 "Oops! Conflict markers detected in $_MERGED."
    echo 1>&2 "Saved your changes to ${_MERGEDSAVE}"
    echo 1>&2 "Exiting with code 1."
    exit 1
fi

exit 0
