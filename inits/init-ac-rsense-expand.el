(defun init-ac-rsense-mode-hook ()
  (setq ac-sources (append '(ac-source-rsense-yas)
                           ac-sources)))

(add-hook 'ruby-mode-hook 'init-ac-rsense-mode-hook)
