(eval-after-load 'init-multiple-cursors
  '(progn
     (define-key mc-mode-map (kbd "C-s") 'phi-search)
     (define-key mc-mode-map (kbd "s") 'phi-search)))
