(setq auto-mode-alist
      (append '(("README\\.md\\'" . gfm-mode)
                ("\\.\\(md\\|mdt\\|mkdn\\|mdwn\\|mdown\\|markdown\\|text\\|textile\\)\\'" . markdown-mode))
              auto-mode-alist))
(eval-after-load 'markdown-mode
  '(when (require 'web-mode nil 'noerror)
     (define-key markdown-mode-map (kbd "C-c m") 'mmm-parse-buffer)))
