(global-unset-key (kbd "M-j"))
(global-set-key (kbd "M-j") 'jump-char-forward)
(global-set-key (kbd "M-J") 'jump-char-backward)

;; fix jump-char keymap
(eval-after-load 'jump-char
  '(progn
     (define-key jump-char-isearch-map (kbd jump-char-forward-key)  'jump-char-repeat-forward)
     (define-key jump-char-isearch-map (kbd jump-char-backward-key) 'jump-char-repeat-backward)
     ;;fix ace-jump-mode integration
     (eval-after-load 'ace-jump-mode
          '(progn
             (define-key jump-char-isearch-map (kbd "C-c C-c") 'jump-char-switch-to-ace)
             (define-key jump-char-isearch-map (kbd "M-/") 'jump-char-switch-to-ace)))))
