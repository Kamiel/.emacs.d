;; (global-unset-key (kbd "C-@"))
;; (global-set-key   (kbd "C-@") 'er/expand-region)
(global-set-key   (kbd "s-@") 'er/expand-region)

;; (global-unset-key (kbd "M-@"))
;; (global-unset-key (kbd "s-;"))
;; (global-unset-key (kbd "s-:"))
;; (global-unset-key (kbd "M-."))

                                        ; (global-set-key (kbd "M-[") 'er/mark-inside-pairs)
(global-set-key (kbd "s-{") 'er/mark-inside-pairs)
(global-set-key (kbd "M-]") 'er/mark-outside-pairs)
(global-set-key (kbd "s-B") 'er/mark-outside-pairs)
(global-set-key (kbd "s-b") 'er/mark-inside-pairs)
(global-set-key (kbd "s-M-@") 'er/mark-symbol)
(global-set-key (kbd "C-M-@") 'er/mark-symbol)
(global-set-key (kbd "M-@") 'er/mark-word)
(global-set-key (kbd "C-s-@") 'er/mark-symbol)

(global-set-key (kbd "s-P") 'mark-paragraph)
(global-set-key (kbd "C-x P") 'mark-paragraph)

(global-set-key (kbd "s-C-/") 'er/mark-comment)
(global-set-key (kbd "s-C-?") 'er/mark-comment-block)
(global-set-key (kbd "s-C-#") 'er/mark-comment)
(global-set-key (kbd "s-C-`") 'er/mark-comment-block)
(global-set-key (kbd "s-C-;") 'er/mark-comment)
(global-set-key (kbd "s-C-:") 'er/mark-comment-block)

(global-set-key (kbd "M-\"") 'er/mark-inside-quotes)
(global-set-key (kbd "M-C-\"") 'er/mark-outside-quotes)
(global-set-key (kbd "s-\"") 'er/mark-inside-quotes)
(global-set-key (kbd "s-C-\"") 'er/mark-outside-quotes)

;; er/mark-word
;; er/mark-symbol
;; er/mark-method-call
;; er/mark-comment
;; er/mark-comment-block
;; er/mark-inside-quotes
;; er/mark-outside-quotes
;; er/mark-inside-pairs
;; er/mark-outside-pairs
;; ;; sgml
;; er/mark-html-attribute
;; er/mark-inner-tag
;; er/mark-outer-tag

;; Ruby support ; ???: deprecated?
;; (eval-after-load 'enh-ruby-mode
;;   '(er/enable-mode-expansions 'enh-ruby-mode 'er/add-ruby-mode-expansions))
