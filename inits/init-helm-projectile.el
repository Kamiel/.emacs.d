(eval-after-load 'helm-mode
  '(eval-after-load 'projectile-mode
     '(progn
        (require 'helm-projectile)
        (helm-projectile-on))))

(eval-after-load 'helm-projectile
  '(progn
     (global-set-key (kbd "C-c h") 'helm-projectile)
     (global-set-key (kbd "s-h") 'helm-projectile)))
