;; Use `secondary-selection` (a builtin face) as background.
(setq objc-font-lock-background-face 'secondary-selection)
(add-hook 'objc-mode-hook 'objc-font-lock-mode)
