(defun company-sourcekit-init ()
  "Enable sourcekit backend for company."
  (eval-after-load 'company
    '(add-to-list
      (make-local-variable 'company-backends)
      'company-sourcekit)))

(add-hook 'swift-mode-hook 'company-sourcekit-init)
