(define-key global-map [remap delete-char] 'auto-indent-delete-char)
;; (define-key global-map [remap kill-line] 'auto-indent-kill-line)

(add-hook 'prog-mode-hook 'auto-indent-mode-on)

;; FIXME: add command to yank without indentation.
