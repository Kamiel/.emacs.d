;; show region immediately
(setq hrb-delay 0)

;; set different face for highlighting keywords
(setq hrb-highlight-keyword-face 'show-paren-match-face)

;; set different face for highlighting block
(setq hrb-highlight-block-face 'org-block-background-face)

;; highlight only keywords
(setq hrb-highlight-mode 'keywords)

;; highlight complete block
;; (setq hrb-highlight-mode 'complete)

;; highlight keywords if both are visible, highlight complete block otherwise
;; (setq hrb-highlight-mode 'mixed)

;; enable hrb-mode
(hrb-mode t)
