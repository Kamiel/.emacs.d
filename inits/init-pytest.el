(eval-after-load 'python
  '(progn
     (define-key python-mode-map (kbd "C-c t a") 'pytest-all)
     (define-key python-mode-map (kbd "C-c t m") 'pytest-module)
     (define-key python-mode-map (kbd "C-c t .") 'pytest-one)
     (define-key python-mode-map (kbd "C-c t d") 'pytest-directory)
     (define-key python-mode-map (kbd "C-c t p a") 'pytest-pdb-all)
     (define-key python-mode-map (kbd "C-c t p m") 'pytest-pdb-module)
     (define-key python-mode-map (kbd "C-c t p .") 'pytest-pdb-one)))
