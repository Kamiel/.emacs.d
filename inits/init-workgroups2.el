(defun init-workgroups-mode-hook ()
  (define-key workgroups-mode-map (kbd "s-m")                      wg-prefixed-map)
  (define-key wg-prefixed-map     (kbd "+")                       'wg-create-workgroup)
  (define-key wg-prefixed-map     (kbd "TAB")                     'wg-switch-to-workgroup)
  (define-key workgroups-mode-map (kbd "ESC ESC +")               'wg-create-workgroup)
  (define-key workgroups-mode-map (kbd "ESC ESC c")               'wg-create-workgroup)
  (define-key workgroups-mode-map (kbd "ESC ESC TAB")             'wg-switch-to-workgroup)
  (define-key workgroups-mode-map (kbd "<escape> <escape> <tab>") 'wg-switch-to-workgroup)
  (define-key workgroups-mode-map (kbd "<escape> <tab>")          'wg-switch-to-workgroup)
  (define-key workgroups-mode-map (kbd "C-<tab>")                 'wg-switch-to-workgroup)
  (define-key workgroups-mode-map (kbd "<escape> <escape> z")     'wg-switch-to-workgroup)
  (define-key workgroups-mode-map (kbd "ESC ESC z")               'wg-switch-to-workgroup)
  (define-key workgroups-mode-map (kbd "<escape> <escape> /")     'wg-switch-to-previous-workgroup)
  (define-key workgroups-mode-map (kbd "ESC ESC /")               'wg-switch-to-previous-workgroup))

(add-hook 'workgroups-mode-hook 'init-workgroups-mode-hook)

(eval-after-load 'workgroups2
  '(progn
     (add-hook 'auto-save-hook 'wg-save-session)

     (eval-after-load 'ivy
       '(progn
          (defun ivy-switch-to-workgroup ()
            (interactive)
            (ivy-read "Workgroup: " (wg-workgroup-names)
                      :action #'wg-switch-to-workgroup))
          (ivy-set-actions
           'ivy-switch-to-workgroup
           '(("c"
              (lambda (_) (wg-create-workgroup (wg-new-default-workgroup-name)))
              "create")
             ("k"
              wg-kill-workgroup
              "kill")))
          (global-set-key [remap wg-switch-to-workgroup] 'ivy-switch-to-workgroup)))))

(defun workgroups-mode-on ()
  "Turn on `workgroups-mode' mode."
  (interactive)
  (ignore-errors ;; Fix loading session
      (workgroups-mode 1))
  (workgroups-mode 1))

(add-hook 'after-init-hook 'workgroups-mode-on)

;; TODO: use "ESC ESC" as additional `wg-prefixed-map' prefix to "C-c z".
;; TODO: `counsel' instance with `wg-create-workgroup' and cetera functionality.
;; TODO: switch (preview) workgroups during selections in `counsel'.
;; TODO: named workgroups integration with `projectile' (name new wg like current
;;       project by default). And `projectile-switch-open-project' would prompt
;;       to create (or select if available) appropriate wg (in `counsel' - just editable
;;       name and list of current workgroups). There should be customizable function for
;;       project name -> workgroup name transformation. Or even list of such
;;       transformation: to let create for project: *-dashboard, *-playground, *-testing,
;;       *-investigation, *-hacking, *-setup, … (or no and provide editable prompt with
;;       guessed name it's just enough).
;;       Also there should be fullscreen-list of modes (like edbi, erc, jabber.el, w3m,
;;       ranger.el, sunrise commander ( if anybode use it yet at all :) )).
