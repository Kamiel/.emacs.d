;; (add-hook 'after-init-hook #'global-flycheck-mode)
(add-hook 'prog-mode-hook 'flycheck-mode)

;; ;; hacky disable flycheck timer to avoid automatically display at point
;; (defadvice flycheck-display-error-at-point-soon
;;     (after flycheck-display-error-at-point-soon-advice activate compile)
;;   (flycheck-cancel-error-display-error-at-point-timer))

;; (defun flycheck-display-error-at-point-soon ()
;;   "No-op placeholder.")

;; (defun init-flycheck-hook ()
;;   (remove-hook 'post-command-hook 'flycheck-display-error-at-point-soon 'local)
;;   (remove-hook 'focus-in-hook 'flycheck-display-error-at-point-soon 'local)
;;   (remove-hook 'after-change-functions 'flycheck-handle-change 'local)
;;   )

;; (add-hook 'flycheck-mode-hook 'init-flycheck-hook)

;; ;; temporary Fix

;; (defun flycheck-finish-current-syntax-check (errors working-dir)
;;   "Finish the current syntax-check in the current buffer with ERRORS.

;; ERRORS is a list of `flycheck-error' objects reported by the
;; current syntax check in `flycheck-current-syntax-check'.

;; Report all ERRORS and potentially start any next syntax checkers.

;; If the current syntax checker reported excessive errors, it is
;; disabled via `flycheck-disable-excessive-checker' for subsequent
;; syntax checks.

;; Relative file names in ERRORS will be expanded relative to
;; WORKING-DIR."
;;   (let* ((syntax-check flycheck-current-syntax-check)
;;          (checker (flycheck-syntax-check-checker syntax-check))
;;          (errors (flycheck-relevant-errors
;;                   (flycheck-fill-and-expand-error-file-names
;;                    (flycheck-filter-errors
;;                     (flycheck-assert-error-list-p errors) checker)
;;                    working-dir))))
;;     (unless (flycheck-disable-excessive-checker checker errors)
;;       (flycheck-report-current-errors errors))
;;     (let ((next-checker (flycheck-get-next-checker-for-buffer checker)))
;;       (if next-checker
;;           (flycheck-start-current-syntax-check next-checker)
;;         (setq flycheck-current-syntax-check nil)
;;         (flycheck-report-status 'finished)
;;         ;; Delete overlays only after the very last checker has run, to avoid
;;         ;; flickering on intermediate re-displays
;;         (flycheck-delete-marked-overlays)
;;         (flycheck-error-list-refresh)
;;         (run-hooks 'flycheck-after-syntax-check-hook)
;;         ;; (when (eq (current-buffer) (window-buffer))
;;         ;;   (flycheck-display-error-at-point))
;;         ;; Immediately try to run any pending deferred syntax check, which
;;         ;; were triggered by intermediate automatic check event, to make sure
;;         ;; that we quickly refine outdated error information
;;         (flycheck-perform-deferred-syntax-check)))))
