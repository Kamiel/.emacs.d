(add-hook 'haskell-mode-hook 'intero-mode)

(eval-after-load 'intero-mode
  '(progn
     ;; (define-key haskell-mode-map (kbd "C-c C-b") 'nil)
     ;; (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-file)
     ;; (define-key haskell-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
     ;; (define-key haskell-mode-map (kbd "C-c v c") 'haskell-cabal-visit-file)
     ;; (define-key haskell-mode-map (kbd "C-c C-t") 'haskell-process-do-type)
     ;; (define-key haskell-mode-map (kbd "C-c C-i") 'haskell-process-do-info)

     (define-key intero-mode-map (kbd "C-c C-t") 'intero-type-at)
     (define-key intero-mode-map (kbd "M-?") 'intero-uses-at)
     (define-key intero-mode-map (kbd "C-c C-i") 'intero-info)
     (define-key intero-mode-map (kbd "M-.") 'intero-goto-definition)
     (define-key intero-mode-map (kbd "C-c C-l") 'intero-repl-load)
     (define-key intero-mode-map (kbd "C-c C-c") 'intero-repl-eval-region)
     (define-key intero-mode-map (kbd "C-c C-z") 'intero-repl)
     (define-key intero-mode-map (kbd "C-c C-r") 'intero-apply-suggestions)
     (define-key intero-mode-map (kbd "C-c C-e") 'intero-expand-splice-at-point)))
