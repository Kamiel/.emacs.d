(add-hook 'sgml-mode-hook 'ac-emmet-html-setup)
(add-hook 'html-mode-hook 'ac-emmet-html-setup)
(add-hook 'css-mode-hook 'ac-emmet-css-setup)
(eval-after-load 'web-mode
  '(progn
     (nconc (assoc "html" web-mode-ac-sources-alist) '(ac-source-emmet-html-aliases
                                                       ac-source-emmet-html-snippets))
     (nconc (assoc "css" web-mode-ac-sources-alist) '(ac-source-css-property
                                                      ac-source-emmet-css-snippets))))
