(setq dictem-use-existing-buffer t)

(setq dictem-use-user-databases-only t)

;;; redefined function
(defun dictem-ensure-buffer ()
  "If current buffer is not a dictem buffer, create a new one."
  (let* ((dictem-buffer (get-buffer-create dictem-buffer-name))
         (dictem-window (get-buffer-window dictem-buffer))
         (window-configuration (current-window-configuration))
         (selected-window (frame-selected-window)))
    (if (window-live-p dictem-window)
        (select-window dictem-window)
      (switch-to-buffer-other-window dictem-buffer))

    (if (dictem-mode-p)
        (progn
		  (if dictem-use-content-history
			  (setq dictem-content-history
					(cons (list (buffer-substring
								 (point-min) (point-max))
								(point)) dictem-content-history)))
		  (setq buffer-read-only nil)
		  (erase-buffer))
      (progn
        (dictem-mode)

        (make-local-variable 'dictem-window-configuration)
        (make-local-variable 'dictem-selected-window)
        (make-local-variable 'dictem-content-history)
        (setq dictem-window-configuration window-configuration)
        (setq dictem-selected-window selected-window)))))

(setq dictem-server "dict.mova.org")
(setq dictem-exclude-databases '("ger-" "-ger" "fra-" "-fra"))

(dictem-initialize)

(add-hook 'dictem-postprocess-match-hook
		  'dictem-postprocess-match)

(add-hook 'dictem-postprocess-definition-hook
		  'dictem-postprocess-definition-separator)

(add-hook 'dictem-postprocess-definition-hook
		  'dictem-postprocess-definition-hyperlinks)

(add-hook 'dictem-postprocess-show-info-hook
		  'dictem-postprocess-definition-hyperlinks)

(add-hook 'dictem-postprocess-definition-hook
		  'dictem-postprocess-each-definition)

(setq dictem-user-databases-alist
	  '(("_en-ru"  . ("mueller7" "korolew_en-ru" "en-ru")); "dict://dict.org:2628/web1913"))
		("_en-en"  . ("foldoc" "gcide" "wn"))
		("_ru-ru"  . ("beslov" "ushakov" "ozhegov" "brok_and_efr"))
		("_ru-en" . ("ru-en"))
		("_unidoc" . ("susv3" "man" "info" "howto" "rfc"))
		))

(define-key dictem-mode-map [tab] 'dictem-next-link)
(define-key dictem-mode-map [(backtab)] 'dictem-previous-link)

;;; http://paste.lisp.org/display/89086
(defun dictem-run-define-at-point-with-query ()
  "Query the default dict server with the word read in within this function."
  (interactive)
  (let* ((default-word (thing-at-point 'symbol))
         (default-prompt (concat "Lookup Word "
                                 (if default-word
                                     (concat "(" default-word ")") nil)
                                 ": "))
         (dictem-query
          (funcall #'(lambda (str)
                       "Remove Whitespace from beginning and end of a string."
                       (replace-regexp-in-string "^[ \n\t]*\\(.*?\\)[ \n\t]*$"
                                                 "\\1"
                                                 str))
                   (read-string default-prompt nil nil default-word))))
    (if (= (length dictem-query) 0) nil
      (dictem-run 'dictem-base-search "_en-ru" dictem-query "."))))

(defun dictem-run-define-at-point ()
  "dictem look up for thing at point"
  (interactive)
  (let* ((default-word (thing-at-point 'symbol))
         (selected-window (frame-selected-window))
         (dictem-query
          (funcall #'(lambda (str)
                       "Remove Whitespace from beginning and end of a string."
                       (replace-regexp-in-string "^[ \n\t]*\\(.*?\\)[ \n\t]*$"
                                                 "\\1"
                                                 str))
                   default-word)))
    (if (= (length dictem-query) 0)
        nil
      (progn
        (dictem-run 'dictem-base-search "_en-ru" dictem-query ".")
        (select-window selected-window)))))

(global-set-key "\C-cT" 'dictem-run-define-at-point-with-query)
(global-set-key "\C-ct" 'dictem-run-define-at-point)
