(defun projectile-rails-robe-mode-on ()
  (interactive)
  (projectile-rails-robe-mode 1))

(add-hook 'projectile-rails-mode-hook 'projectile-rails-robe-mode-on)
