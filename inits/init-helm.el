;; like sublimetext's Ctrl-P file@symbol
(defun helm-goto-symbol ()
  "multi-occur in all buffers backed by files."
  (interactive)
  (helm-multi-occur
   (delq nil
         (mapcar (lambda (b)
                   (when (buffer-file-name b) (buffer-name b)))
                 (buffer-list)))))

;; keybindings for Helm
(global-set-key [remap occur]                    'helm-occur)
(global-set-key [remap execute-extended-command] 'helm-M-x)
(global-set-key [remap find-file]                'helm-find-files)
(global-set-key [remap recentf-open-files]       'helm-recentf)
(global-set-key [remap switch-to-buffer]         'helm-buffers-list)
(global-set-key (kbd "s-p")                      'helm-goto-symbol)
(global-set-key (kbd "C-x p")                    'helm-goto-symbol)

;; hooks
;; enable helm pcomplete
(defun helm-eshell-pcomplete-hook ()
  (define-key eshell-mode-map [remap eshell-pcomplete] 'helm-esh-pcomplete))

(defun helm-eshell-history-hook ()
  (define-key eshell-mode-map (kbd "M-p") 'helm-eshell-history))

(defun helm-dired-moccur-hook ()
  (local-set-key (kbd "O") 'helm-c-moccur-dired-do-moccur-by-moccur))

(add-hook 'eshell-mode-hook 'helm-eshell-pcomplete-hook)
(add-hook 'eshell-mode-hook 'helm-eshell-history-hook)
(add-hook 'dired-mode-hook  'helm-dired-moccur-hook)

(eval-after-load 'helm-mode
  '(progn
     (eval-after-load 'hacks
       '(ido-mode-off))
     (define-key helm-map (kbd "C-h") 'backward-delete-char)
     (define-key helm-map (kbd "C-g") 'helm-keyboard-quit)
     ;; Rebind TAB and C-z
     (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
     (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)
     (define-key helm-map (kbd "C-z") 'helm-select-action)))

;; kill ring summary
(global-set-key (kbd "s-C-M-V") 'helm-show-kill-ring)

;; zsh-like for files, see: http://emacsist.com/10641

(with-eval-after-load 'helm-files
  ;; Let helm support zsh-like path expansion.
  (defvar helm-ff-expand-valid-only-p t)
  (defvar helm-ff-sort-expansions-p t)
  (defun helm-ff-try-expand-fname (candidate)
    (lexical-let ((dirparts (split-string candidate "/"))
          valid-dir
          fnames)
      (catch 'break
        (while dirparts
          (if (file-directory-p (concat valid-dir (car dirparts) "/"))
              (setq valid-dir (concat valid-dir (pop dirparts) "/"))
            (throw 'break t))))
      (setq fnames (cons candidate (helm-ff-try-expand-fname-1 valid-dir dirparts)))
      (if helm-ff-sort-expansions-p
          (sort fnames
                (lambda (f1 f2) (or (file-directory-p f1)
                                    (not (file-directory-p f2)))))
        fnames)))

  (defun helm-ff-try-expand-fname-1 (parent children)
    (if children
        (if (equal children '(""))
            (and (file-directory-p parent) `(,(concat parent "/")))
          (when (file-directory-p parent)
            (apply 'nconc
                   (mapcar
                    (lambda (f)
                      (or (helm-ff-try-expand-fname-1 f (cdr children))
                          (unless helm-ff-expand-valid-only-p
                            (and (file-directory-p f)
                                 `(,(concat f "/" (mapconcat 'identity
                                                             (cdr children)
                                                             "/")))))))
                    (directory-files parent t (regexp-quote (car children)))))))
      `(,(concat parent (and (file-directory-p parent) "/")))))

  (defun qjp-helm-ff-try-expand-fname (orig-func &rest args)
    (lexical-let* ((candidate (car args))
           (collection (helm-ff-try-expand-fname candidate)))
      (if (and (> (length collection) 1)
               (not (file-exists-p candidate)))
          (with-helm-alive-p
            (when (helm-file-completion-source-p)
              (helm-exit-and-execute-action
               (lambda (_)
                 (helm-find-files-1
                  (helm-comp-read "Expand Path to: " collection))))))
        (apply orig-func args))))

  (advice-add 'helm-ff-kill-or-find-buffer-fname :around #'qjp-helm-ff-try-expand-fname))
