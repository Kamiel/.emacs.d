;; To avoid error font-info: Invalid face: linum
(add-hook 'nlinum-mode-hook (lambda () (require 'linum)))
;; <<< UDATE: this not solve the problem :( FIX 4 it needed

;; To precalculate the line number width to avoid horizontal jumps on scrolling
(add-hook 'nlinum-mode-hook
          (lambda ()
            (unless (boundp 'nlinum--width)
              (setq nlinum--width
                    (length (number-to-string
                             (count-lines (point-min) (point-max))))))))
(add-hook 'prog-mode-hook 'nlinum-mode)
;; (setq after-make-frame-functions nil)
