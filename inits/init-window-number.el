(add-hook 'after-init-hook 'window-number-mode-on)
(add-hook 'after-init-hook 'window-number-meta-mode-on)

(eval-after-load 'window-number
  '(define-key window-number-mode-map (kbd "C-x C-j") 'dired-jump))
