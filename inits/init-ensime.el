;; (defun ac-ensime-enable ()
;;   "Add `ac-source-ensime-completions' to `ac-sources' for this buffer."
;;   (make-local-variable 'ac-sources)
;;   (add-to-list 'ac-sources 'ac-source-ensime-completions))

;; (add-hook 'scala-mode-hook 'ac-ensime-enable)

;; (defun scala-hotfix ()
;;   "Turn off unnecessary minor modes for `scala-mode'."
;;   (company-mode -1)
;;   (aggressive-indent-mode -1))

;; (add-hook 'scala-mode-hook 'scala-hotfix)
