(defadvice text-scale-adjust (after re-enable-fci-mode activate)
  (while fci-mode
	(turn-off-fci-mode)
	(turn-on-fci-mode)))

(add-hook 'haskell-mode-hook 'turn-on-fci-mode)
