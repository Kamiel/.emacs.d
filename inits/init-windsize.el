(global-set-key (kbd "C-x <up>") 'windsize-up)
(global-set-key (kbd "C-x <down>") 'windsize-down)
(global-set-key (kbd "C-x <left>") 'windsize-left)
(global-set-key (kbd "C-x <right>") 'windsize-right)
