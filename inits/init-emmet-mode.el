(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'html-mode-hook 'emmet-mode)
(add-hook 'nxml-mode-hook  'emmet-mode)
(add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
(add-hook 'web-mode-hook  'emmet-mode)

;; see http://web-mode.org/

(defun init-setup-emmet-for-web ()
  "Check `emmet-mode' for CSS or HTML language and setup `emmet-mode'."
  (let ((web-mode-cur-language
         (web-mode-language-at-pos)))
    (if (string= web-mode-cur-language "css")
        (setq emmet-use-css-transform t)
      (setq emmet-use-css-transform nil))))

(eval-after-load 'web-mode
  '(add-hook 'web-mode-before-auto-complete-hooks 'init-setup-emmet-for-web))
