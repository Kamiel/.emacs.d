(global-set-key (kbd "C-+") 'column-highlight-mode)
(global-set-key (kbd "C-=") 'flash-column-highlight)

(add-hook 'haskell-mode-hook 'flash-column-highlight)
