;; theme
(defun powerline-netsu-theme ()
  "Setup custom mode-line."
  (interactive)
  (setq-default mode-line-format
                '("%e"
                  (:eval
                   (let* ((active (powerline-selected-window-active))
                          (mode-line (if active 'mode-line 'mode-line-inactive))
                          (face1 (if active 'powerline-active1 'powerline-inactive1))
                          (face2 (if active 'powerline-active2 'powerline-inactive2))
                          (separator-left (intern (format "powerline-%s-%s"
                                                          powerline-default-separator
                                                          (car powerline-default-separator-dir))))
                          (separator-right (intern (format "powerline-%s-%s"
                                                           powerline-default-separator
                                                           (cdr powerline-default-separator-dir))))
                          (lhs (append (when (boundp 'window-number-mode)
										 (list (funcall separator-left mode-line face1)
											   (powerline-raw
												(replace-regexp-in-string "[[:space:]\n-]*" ""
																		  (window-number-string)) face1)
											   (funcall separator-left face1 mode-line)))
									   (list (powerline-raw "%*" nil 'l)
											 (powerline-raw "%1p" nil 'l)
                                             (powerline-raw " ")
											 (funcall separator-left mode-line face2)
											 (powerline-raw "%1l" face2)
											 (powerline-raw ":" face2)
											 (powerline-raw "%1c" face2)
											 (funcall separator-left face2 mode-line)
											 (powerline-buffer-id nil 'l)
											 (powerline-raw " ")
											 (funcall separator-left mode-line face1)
											 (when (boundp 'erc-modified-channels-object)
											   (powerline-raw erc-modified-channels-object face1 'l))
											 (powerline-major-mode face1)
											 (funcall separator-left face1 mode-line)
											 (powerline-process mode-line)
											 (powerline-minor-modes mode-line 'l)
											 (powerline-narrow mode-line 'l)
                                             (powerline-raw " " mode-line)
											 (funcall separator-left mode-line face2)
											 (powerline-vc face2 'r))))
                          (rhs (list (powerline-raw global-mode-string face2 'r)
                                     (funcall separator-right face2 face1)
                                     (powerline-buffer-size face1)
									 (funcall separator-right face1 mode-line)
									 (powerline-raw " ")
                                     (powerline-raw mode-line-mule-info nil 'r)
                					 (powerline-hud face2 face1)))
						  (center (list (when (and (boundp 'which-func-mode) which-func-mode
												   (gethash
													(selected-window)
													which-func-table))
										  (powerline-raw which-func-format face2)))))
					 (concat (powerline-render lhs)
                             (powerline-fill-center face2 (/ (powerline-width center) 2.0))
                             (powerline-render center)
                             (powerline-fill face2 (powerline-width rhs))
							 (powerline-render rhs)))))))

(powerline-netsu-theme)
