
(eval-after-load 'markdown-mode
  '(progn

     (defun init-mmm-markdown-auto-class (lang &optional submode)
       "Define a mmm-mode class for LANG in `markdown-mode' and `gfm-mode' using SUBMODE.
If SUBMODE is not provided, use `LANG-mode' by default."
       (let ((markdown-class (intern (concat "markdown-" lang)))
             (gfm-class      (intern (concat "gfm-" lang)))
             (submode (or submode (intern (concat lang "-mode"))))
             (front (concat "^``` *" lang " *\\(\r\n\\|\n\\|\r\\)"))
             (back "^``` *$"))
         (mmm-add-classes (list (list gfm-class :submode submode :front front :back back)
                                (list markdown-class :submode submode :front front :back back)))
         (mmm-add-mode-ext-class 'markdown-mode nil markdown-class)
         (mmm-add-mode-ext-class 'gfm-mode nil gfm-class)))

     (eval-after-load 'mmm-mode
       '(progn
          (setq mmm-global-mode (quote maybe) nil (mmm-mode))
          (setq mmm-never-modes
                (quote
                 (help-mode Info-mode dired-mode comint-mode telnet-mode shell-mode eshell-mode forms-mode text-mode ediff-mode)))
          (mapc 'init-mmm-markdown-auto-class
                '("awk" "bibtex" "c" "cpp" "css" "haskell" "latex" "lisp" "makefile"
                  "markdown" "objc" "python" "r" "ruby" "sql" "stata" "swift" "xml"))

          (if (featurep 'web-mode)
              (init-mmm-markdown-auto-class "html" 'web-mode)
            (init-mmm-markdown-auto-class "html" 'html-mode))

          (init-mmm-markdown-auto-class "elisp" 'emacs-lisp-mode)
          (init-mmm-markdown-auto-class "fortran" 'f90-mode)
          (init-mmm-markdown-auto-class "objective-c" 'objc-mode)
          (init-mmm-markdown-auto-class "perl" 'cperl-mode)
          (init-mmm-markdown-auto-class "shell" 'shell-script-mode)))))
