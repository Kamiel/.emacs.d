(eval-after-load 'haskell-mode
  '(progn
     (define-key haskell-mode-map (kbd "C-c C-b") 'nil)
     (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-file)
     (define-key haskell-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
     (define-key haskell-mode-map (kbd "C-c v c") 'haskell-cabal-visit-file)
     (define-key haskell-mode-map (kbd "C-c C-t") 'haskell-process-do-type)
     (define-key haskell-mode-map (kbd "C-c C-i") 'haskell-process-do-info)))

(eval-after-load 'interactive-haskell-mode
  '(progn
     ;; GHCi-ng integration
     (define-key interactive-haskell-mode-map (kbd "M-.") 'haskell-mode-goto-loc)
     (define-key interactive-haskell-mode-map (kbd "C-c C-t") 'haskell-mode-show-type-at)))

(eval-after-load 'haskell-cabal
  '(define-key haskell-cabal-mode-map (kbd "C-c C-b") 'haskell-compile))

(defun init-haskell-mode-hook ()
  (when (require 'w3m nil 'noerror)
    (require 'w3m-haddock))
  (haskell-indent-mode 0)
  (haskell-indentation-mode 0)
  (haskell-decl-scan-mode 1)
  (eldoc-mode 0)
  ;; (eval-after-load 'aggressive-indent '(aggressive-indent-mode 0))
  (eval-after-load 'smart-newline '(smart-newline-mode 0))
  (haskell-doc-mode 0)
  ;; (eval-after-load 'nix-haskell-mode
  ;; 'nix-haskell-mode)
  (eval-after-load 'projectile
    '(progn
       (setq-local projectile-tags-command "haskdogs -- --ignore-close-implementation --etags")))
  (eval-after-load 'adaptive-wrap
    '(setq-local adaptive-wrap-extra-indent 2))
  ;; (interactive-haskell-mode 1) ; TODO: interactive-haskell-mode-map conflict with intero
  (setq-local outline-regexp "^[^    ].*\\|^.*[  ]+\\(where\\|of\\|do\\|in\\|if\\|then\\|else\\|let\\|module\\|import\\|deriving\\|instance\\|class\\)[
]")
  (setq-local outline-level 'hsk-outline-level))

(add-hook 'haskell-mode-hook 'init-haskell-mode-hook)

(remove-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(remove-hook 'haskell-mode-hook 'turn-on-haskell-indentation)

;; Customize PROMPT
