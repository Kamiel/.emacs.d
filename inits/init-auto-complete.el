;; (require 'auto-complete)
;; (require 'auto-complete-config)
(ac-config-default)

(ac-linum-workaround)
(ac-flyspell-workaround)
(define-key ac-completing-map (kbd "C-n") 'ac-next)
(define-key ac-completing-map (kbd "C-p") 'ac-previous)
(define-key ac-completing-map (kbd "M-/") 'ac-stop)
(global-set-key (kbd "M-?") 'ac-start)
(global-set-key (kbd "\t") 'ac-complete)

;; sources
(setq-default ac-sources (append '(ac-source-imenu
                                   ac-source-yasnippet) ac-sources))

(defun auto-complete-mode-on ()
  (interactive)
  (auto-complete-mode 1))

(add-hook 'text-mode-hook 'auto-complete-mode-on)
;; (add-hook 'prog-mode-hook 'auto-complete-mode-on)
;; (add-hook 'org-mode-hook 'auto-complete-mode-on)
;; (add-hook 'nxml-mode-hook 'auto-complete-mode-on)

;; haskell
(defun haskell-ac-ghc-mod-hook ()
  "Add `ac-source-ghc-mod' to `ac-sources' for this buffer."
  (make-local-variable 'ac-sources)
  (add-to-list 'ac-sources 'ac-source-ghc-mod))

(add-hook 'haskell-mode-hook 'haskell-ac-ghc-mod-hook)
