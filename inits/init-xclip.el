(defun init-xclip-hook ()
  (if (display-graphic-p)
      (progn
        (setq interprogram-cut-function
              'x-select-text)
        (setq interprogram-paste-function
              'x-selection-value))
    (turn-on-xclip)))

;; TODO: this not work differently for each client :(

;; (add-hook 'after-make-frame-functions 'init-xclip-hook)
;; (add-hook 'after-make-frame-functions 'server-switch-hook)
(add-hook 'after-init-hook 'init-xclip-hook)
