(defun init-exec-path-from-shell ()
  "Setup PATH on macOS."
  (when (eq system-type 'darwin)
    (exec-path-from-shell-initialize)
    (if-let ((ls-executable (executable-find "gls")))
        (setq insert-directory-program ls-executable)
      (progn
        (setq dired-use-ls-dired nil)
        (require 'ls-lisp)
        (setq ls-lisp-use-insert-directory-program nil)))))

(add-hook 'after-init-hook 'init-exec-path-from-shell)
