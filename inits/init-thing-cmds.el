(global-set-key (kbd "C-^")   'select-thing-near-point)
(global-set-key (kbd "s-^")   'select-thing-near-point)
(global-set-key (kbd "C-M-^") 'mark-enclosing-sexp)
(global-set-key (kbd "s-M-^") 'mark-enclosing-sexp)
