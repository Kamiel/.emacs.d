(global-set-key (kbd "C-*") 'highlight-symbol-next)
(global-set-key (kbd "C-#") 'highlight-symbol-prev)
(global-set-key (kbd "C-)") 'highlight-symbol-next)
(global-set-key (kbd "C-(") 'highlight-symbol-prev)
(global-set-key (kbd "M-s-e") 'highlight-symbol-query-replace)

(defun highlight-symbol-mode-on ()
  "Turn on `highlight-symbol-mode' mode."
  (interactive)
  (highlight-symbol-mode 1))

(add-hook 'prog-mode-hook 'highlight-symbol-mode-on) ; FIXME: perfomance issue
