(add-hook 'haskell-mode-hook 'turn-on-hi2)

(eval-after-load 'haskell-indent
  '(add-hook 'haskell-indent-hook 'init-setup-hi2))

(eval-after-load 'haskell-indentation
  '(add-hook 'haskell-indentation-mode-hook 'init-setup-hi2))

(eval-after-load 'hi2-mode
  '(add-hook 'hi2-mode-hook 'init-setup-hi2))

(defun init-setup-hi2 ()
  (kill-local-variable 'indent-line-function)
  (when (ignore-errors hi2-mode)
    (set (make-local-variable 'indent-line-function) 'hi2-indent-line)))
