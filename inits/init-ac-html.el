(defun init-setup-ac-for-html ()
  "Enable HTML sources for `auto-complete-mode'."
  (require 'ac-html)
  (require 'ac-html-default-data-provider)
  (ac-html-enable-data-provider 'ac-html-default-data-provider)
  (ac-html-setup)
  (make-local-variable 'ac-sources)
  (setq ac-sources (append '(ac-source-html-tag
                             ac-source-html-attr
                             ac-source-html-attrv)
                           ac-sources)))

(add-hook 'html-mode-hook 'init-setup-ac-for-html)

(defun init-setup-ac-for-haml ()
  "Enable HAML sources for `auto-complete-mode'."
  (require 'ac-haml)
  (require 'ac-html-default-data-provider)
  (ac-html-enable-data-provider 'ac-html-default-data-provider)
  (ac-haml-setup)
  (make-local-variable 'ac-sources)
  (setq ac-sources (append '(ac-source-haml-tag
                             ac-source-haml-attr
                             ac-source-haml-attrv)
                           ac-sources)))

(add-hook 'haml-mode-hook 'init-setup-ac-for-haml)

(defun init-setup-ac-for-slim ()
  "Enable SLIM sources for `auto-complete-mode'."
  (require 'ac-slim)
  (require 'ac-html-default-data-provider)
  (ac-html-enable-data-provider 'ac-html-default-data-provider)
  (ac-slim-setup)
  (make-local-variable 'ac-sources)
  (setq ac-sources (append '(ac-source-slim-tag
                             ac-source-slim-attr
                             ac-source-slim-attrv)
                           ac-sources)))

(add-hook 'slim-mode-hook 'init-setup-ac-for-slim)

(defun init-setup-ac-for-jade ()
  "Enable JADE sources for `auto-complete-mode'."
  (require 'ac-jade)
  (require 'ac-html-default-data-provider)
  (ac-html-enable-data-provider 'ac-html-default-data-provider)
  (ac-jade-setup)
  (make-local-variable 'ac-sources)
  (setq ac-sources (append '(ac-source-jade-tag
                             ac-source-jade-attr
                             ac-source-jade-attrv)
                           ac-sources)))

(add-hook 'jade-mode-hook 'init-setup-ac-for-jade)

;; `web-mode' setup

(defun init-setup-ac-for-web ()
  "Prepare `auto-complete-mode' for `web-mode'."
  (eval-after-load 'ac-html
    '(progn
       (require 'ac-html)
       (require 'ac-html-default-data-provider)
       (ac-html-enable-data-provider 'ac-html-default-data-provider)
       (ac-html-setup))))

(add-hook 'web-mode-hook 'init-setup-ac-for-web)

(eval-after-load 'web-mode
  '(nconc (assoc "html" web-mode-ac-sources-alist) '(ac-source-html-tag
                                                     ac-source-html-attr
                                                     ac-source-html-attrv)))
