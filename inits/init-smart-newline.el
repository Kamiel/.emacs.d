(defun init-smart-newline-hook ()
  (smart-newline-mode 1))
(add-hook 'prog-mode-hook 'init-smart-newline-hook)
