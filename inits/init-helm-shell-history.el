(defun helm-shell-history-hook ()
  (define-key term-raw-map (kbd "C-r") 'helm-shell-history))

(add-hook 'term-mode-hook 'helm-shell-history-hook)
