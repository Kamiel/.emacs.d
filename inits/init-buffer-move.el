(global-set-key (kbd "ESC ESC C-p") 'buf-move-up)
(global-set-key (kbd "ESC ESC C-n") 'buf-move-down)
(global-set-key (kbd "ESC ESC C-b") 'buf-move-left)
(global-set-key (kbd "ESC ESC C-f") 'buf-move-right)

(global-set-key (kbd "<escape> <escape> C-p") 'buf-move-up)
(global-set-key (kbd "<escape> <escape> C-n") 'buf-move-down)
(global-set-key (kbd "<escape> <escape> C-b") 'buf-move-left)
(global-set-key (kbd "<escape> <escape> C-f") 'buf-move-right)

(global-set-key (kbd "<escape> C-p") 'buf-move-up)
(global-set-key (kbd "<escape> C-n") 'buf-move-down)
(global-set-key (kbd "<escape> C-b") 'buf-move-left)
(global-set-key (kbd "<escape> C-f") 'buf-move-right)

(global-set-key (kbd "ESC ESC C-<up>")    'buf-move-up)
(global-set-key (kbd "ESC ESC C-<down>")  'buf-move-down)
(global-set-key (kbd "ESC ESC C-<left>")  'buf-move-left)
(global-set-key (kbd "ESC ESC C-<right>") 'buf-move-right)

(global-set-key (kbd "ESC M-C-<up>")    'buf-move-up)
(global-set-key (kbd "ESC M-C-<down>")  'buf-move-down)
(global-set-key (kbd "ESC M-C-<left>")  'buf-move-left)
(global-set-key (kbd "ESC M-C-<right>") 'buf-move-right)

(global-set-key (kbd "<escape> <escape> C-<up>")    'buf-move-up)
(global-set-key (kbd "<escape> <escape> C-<down>")  'buf-move-down)
(global-set-key (kbd "<escape> <escape> C-<left>")  'buf-move-left)
(global-set-key (kbd "<escape> <escape> C-<right>") 'buf-move-right)

(global-set-key (kbd "<escape> C-<up>")    'buf-move-up)
(global-set-key (kbd "<escape> C-<down>")  'buf-move-down)
(global-set-key (kbd "<escape> C-<left>")  'buf-move-left)
(global-set-key (kbd "<escape> C-<right>") 'buf-move-right)

;; terminal version support

(global-set-key (kbd "C-c C-p") 'buf-move-up)
(global-set-key (kbd "C-c C-n") 'buf-move-down)
(global-set-key (kbd "C-c C-b") 'buf-move-left)
(global-set-key (kbd "C-c C-f") 'buf-move-right)

(global-set-key (kbd "C-c C-<up>") 'buf-move-up)
(global-set-key (kbd "C-c C-<down>") 'buf-move-down)
(global-set-key (kbd "C-c C-<left>") 'buf-move-left)
(global-set-key (kbd "C-c C-<right>") 'buf-move-right)
