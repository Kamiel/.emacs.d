(with-eval-after-load 'persp-mode
  (with-eval-after-load 'ivy
    (add-hook 'ivy-ignore-buffers
              #'(lambda (b)
                  (when persp-mode
                    (let ((persp (get-current-persp)))
                      (if persp
                          (not (persp-contain-buffer-p b persp))
                        nil)))))

    (setq ivy-sort-functions-alist
          (append ivy-sort-functions-alist
                  '((persp-kill-buffer   . nil)
                    (persp-remove-buffer . nil)
                    (persp-add-buffer    . nil)
                    (persp-switch        . nil)
                    (persp-window-switch . nil)
                    (persp-frame-switch . nil))))))

(eval-after-load 'persp-mode
  '(progn
     (global-set-key (kbd "C-<tab>") 'persp-switch)
     (define-key persp-key-map (kbd "TAB") 'persp-switch)))
