(global-set-key (kbd "C-c M-g")                  'ivy-resume)
(global-set-key (kbd "C-c M-s")                  'ivy-resume)
(global-set-key (kbd "<f6>")                     'ivy-resume)
(global-set-key [remap execute-extended-command] 'counsel-M-x)
(global-set-key [remap find-file]                'counsel-find-file)
(global-set-key [remap recentf-open-files]       'counsel-recentf)
(global-set-key [remap switch-to-buffer]         'ivy-switch-buffer)
(global-set-key [remap yank-pop]                 'counsel-yank-pop)
(global-set-key [remap describe-function]        'counsel-describe-function)
(global-set-key [remap describe-variable]        'counsel-describe-variable)
(global-set-key [remap info]                     'counsel-info-lookup-symbol)
(global-set-key [remap insert-char]              'counsel-unicode-char)
(global-set-key [remap describe-face]            'counsel-describe-face)
(global-set-key [remap list-faces-display]       'counsel-faces)
(global-set-key (kbd "C-c g")                    'counsel-git)
(global-set-key (kbd "C-c j")                    'counsel-git-grep)
(global-set-key (kbd "C-c a")                    'counsel-ag)
(global-set-key (kbd "C-c i")                    'counsel-imenu)
(global-set-key [remap describe-bindings]        'counsel-descbinds)
(global-set-key (kbd "s-C-M-V") 'counsel-yank-pop)
(global-set-key [remap occur]                    'counsel-grep-or-swiper)
(global-set-key [remap multi-occur]              'swiper-multi)
(global-set-key (kbd "C-S-o")                    'swiper-all)
(global-set-key (kbd "C-x s-F")                  'swiper-all)
(global-set-key (kbd "C-c f")                    'swiper-all)
(global-set-key (kbd "s-F")                      'swiper-all)
(define-key read-expression-map [remap previous-history-element] 'counsel-expression-history)
(global-set-key (kbd "s-P")                      'counsel-find-symbol)
(global-set-key (kbd "C-c p")                    'counsel-find-symbol)

(eval-after-load 'helm-mode
  '(progn
     (global-set-key [remap helm-M-x]            'counsel-M-x)
     (global-set-key [remap helm-find-files]     'counsel-find-file)
     (global-set-key [remap helm-recentf]        'counsel-recentf)
     (global-set-key [remap helm-buffers-list]   'ivy-switch-buffer)
     (global-set-key [remap helm-show-kill-ring] 'counsel-yank-pop)
     (global-set-key [remap helm-occur]          'counsel-grep-or-swiper)
     (global-set-key [remap helm-goto-symbol]    'counsel-find-symbol)))

(eval-after-load 'helm-swoop
  '(progn
     (global-set-key [remap helm-swoop]       'swiper)
     (global-set-key [remap helm-multi-swoop] 'swiper-multi)))

(defun ivy-backward-directory ()
  "Forward to `kill-start-of-line'.
On error (read-only), call `ivy-on-del-error-function'."
  (interactive)
  (if (and ivy--directory (= (minibuffer-prompt-end) (point)))
      (progn
        (let ((old-dir (file-name-nondirectory
                        (directory-file-name ivy--directory)))
              idx)
          (ivy--cd (file-name-directory
                    (directory-file-name
                     (expand-file-name
                      ivy--directory))))
          (ivy--exhibit)
          (when (setq idx (cl-position
                           (file-name-as-directory old-dir)
                           ivy--old-cands
                           :test 'equal))
            (ivy-set-index idx))))
    (condition-case nil
        (kill-start-of-line)
      (error
       (when ivy-on-del-error-function
         (funcall ivy-on-del-error-function))))))

(eval-after-load 'ivy
  '(progn
     (define-key ivy-minibuffer-map (kbd "M-r")       'ivy-reverse-i-search)
     (define-key ivy-minibuffer-map (kbd "C-r")       'ivy-previous-line-or-history)
     (define-key ivy-minibuffer-map (kbd "s-<left>")  'ivy-backward-directory)
     (define-key ivy-minibuffer-map (kbd "s-<right>") 'ivy-alt-done)
     (define-key ivy-minibuffer-map (kbd "C-l")       'ivy-backward-directory)
     (define-key ivy-minibuffer-map (kbd "C-<tab>")   'ivy-partial-or-done)
     (define-key ivy-minibuffer-map (kbd "S-<tab>")   'ivy-partial-or-done)
     (define-key ivy-minibuffer-map (kbd "TAB")       'ivy-alt-done)
     ;; (global-set-key (kbd "<escape> <tab>")              'ivy-switch-view)
     (global-set-key (kbd "<escape> +")              'ivy-push-view)
     (global-set-key (kbd "<escape> -")              'ivy-pop-view)))

(eval-after-load 'helm-descbinds
  '(progn
     (global-set-key [remap helm-descbinds] 'counsel-descbinds)))

(defun projectile-project-find-function (dir)
  (let* ((root (projectile-project-root dir)))
    (and root (cons 'transient root))))

(with-eval-after-load 'project
  (add-to-list 'project-find-functions 'projectile-project-find-function))
