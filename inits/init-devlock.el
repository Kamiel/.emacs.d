(setq develock-keywords-alist
	  '((enh-ruby-mode ruby-font-lock-keywords-x
					   develock-ruby-font-lock-keywords)))

(plist-put develock-max-column-plist 'enh-ruby-mode
		   (plist-get develock-max-column-plist 'ruby-mode))
