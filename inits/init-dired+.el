;; reuse same buffer
(toggle-diredp-find-file-reuse-dir 1)

(defun dired-plus-dired-hook ()
  (set (make-local-variable 'font-lock-defaults)
       (cons '(dired-font-lock-keywords diredp-font-lock-keywords-1)
             (cdr font-lock-defaults))))

(add-hook 'dired-mode-hook 'dired-plus-dired-hook)
