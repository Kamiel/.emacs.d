;; (setq company-backends
;;       '((company-files
;;          company-keywords
;;          company-capf
;;          company-yasnippet)
;;         (company-abbrev
;;          company-dabbrev
;;          company-dabbrev-code)))

(setq company-echo-delay 0)

(setq company-backends
      '(company-files
        company-keywords
        company-capf
        company-yasnippet
        company-abbrev
        company-dabbrev
        company-dabbrev-code))

(eval-after-load 'company
  '(progn
     (define-key company-active-map (kbd "TAB") 'company-complete-common-or-cycle)
     (define-key company-active-map (kbd "<tab>") 'company-complete-common-or-cycle)
     (define-key company-active-map (kbd "S-TAB") 'company-select-previous)
     (define-key company-active-map (kbd "<backtab>") 'company-select-previous)))
