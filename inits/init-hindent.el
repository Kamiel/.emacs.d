(add-hook 'haskell-mode-hook 'hindent-mode)

;; (add-hook 'haskell-indent-hook 'init-setup-hindent)
;; (add-hook 'haskell-indentation-mode-hook 'init-setup-hindent)
;; (add-hook 'hi2-mode-hook 'init-setup-hindent)
;; (add-hook 'hindent-mode-hook 'init-setup-hindent)

(defun init-setup-hindent ()
  (kill-local-variable 'indent-region-function)
  (kill-local-variable 'normal-auto-fill-function)
  (when (ignore-errors hindent-mode)
    (set (make-local-variable 'indent-region-function) 'hindent-reformat-region)
    (set (make-local-variable 'normal-auto-fill-function) 'hindent-reformat-decl-or-fill)))
