;; Uncomment to enable:
;; (remove-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;; (remove-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;; (remove-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)
;; (add-hook 'haskell-mode-hook 'structured-haskell-mode)

(eval-after-load 'structured-haskell-mode
  '(progn
     (defun shm/forward-kill-word ()
       "Kill the word forwards."
       (interactive)
       (let ((to-be-deleted (save-excursion (forward-word)
                                            (point))))
         (save-excursion
           (shm-adjust-dependents (point) (* -1 (- (point) to-be-deleted))))
         (forward-kill-word 1)))
     ;; Insertion
     (define-key shm-map (kbd "s-/") 'shm/comment)
     (define-key shm-map (kbd "s-#") 'shm/comment)
     (define-key shm-map (kbd "s-;") 'shm/comment)
     ;; Indentation
     (define-key shm-map (kbd "s-<return>")     'shm/simple-indent-newline-indent)
     (define-key shm-map (kbd "S-s-<return>")   'shm/newline-indent-proxy) ;; TODO: open line forth/back
     (define-key shm-map (kbd "C-i")            'shm/tab)
     (define-key shm-map (kbd "C-S-i")          'shm/backtab)
     (define-key shm-map (kbd "C-c C-j")         nil)
     (define-key shm-map (kbd "C-c C-<return>") 'shm/swing-down)
     ;; Deletion
     (define-key shm-map (kbd "C-d")            'shm/del)
     (define-key shm-map (kbd "C-h")            'shm/delete)
     (define-key shm-map (kbd "M-d")            'shm/forward-kill-word)
     (define-key shm-map (kbd "M-h")            'shm/backward-kill-word)
     (define-key shm-map (kbd "C-<backspace>")  'subword-backward-kill)
     (define-key shm-map (kbd "C-<deletechar>") 'subword-kill)
     ;; Killing & yanking
     (define-key shm-map (kbd "s-x")   'shm/kill-region)
     (define-key shm-map (kbd "s-c")   'shm/copy-region)
     (define-key shm-map (kbd "s-v")   'shm/yank)
     (define-key shm-map (kbd "S-s-v") 'shm/yank-pop) ;; TODO: yank backward/forward
     ))
