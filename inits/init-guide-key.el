(defun guide-key-dired-hook ()
  (guide-key/add-local-guide-key-sequence "?")
  (guide-key/add-local-guide-key-sequence "%"))

(add-hook 'dired-mode-hook 'guide-key-dired-hook)
