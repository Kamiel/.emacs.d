(global-set-key [remap occur]       'helm-swoop)
(global-set-key [remap multi-occur] 'helm-multi-swoop)

(eval-after-load 'helm-mode
  '(progn
     (global-set-key [remap helm-occur] 'helm-swoop)))
