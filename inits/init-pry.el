(defun run-pry-remote (&rest args)
  "Rut `pry-remote'. You should have this gem installed already."
  (interactive)
  (let ((buffer (apply 'make-comint "pry-remote" "pry-remote" nil args)))
    (switch-to-buffer buffer)
    (setq-local comint-process-echoes t)))

(eval-after-load 'projectile-rails
  '(define-key projectile-rails-mode-map (kbd "C-c r d")'run-pry-remote))
