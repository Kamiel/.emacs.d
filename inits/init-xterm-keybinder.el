(add-hook
 'tty-setup-hook
 '(lambda ()
    (cl-case (assoc-default 'terminal-initted (terminal-parameters))
      (terminal-init-rxvt
       ;; (when (getenv "COLORTERM" (selected-frame))
         (urxvt-keybinder-setup)))))
;; )
