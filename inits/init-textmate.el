(textmate-mode)

(define-key *textmate-mode-map* [(meta return)]          'nil)
(define-key *textmate-mode-map* [(control c)(control t)] 'nil)
(define-key *textmate-mode-map* [(control c)(control a)] 'nil)
(define-key *textmate-mode-map* [(control tab)]          'nil)
(define-key *textmate-mode-map* [(control shift tab)]    'nil)
(define-key *textmate-mode-map* [(control c)(control k)] 'nil)
(define-key *textmate-mode-map* [(meta t)]               'nil)
(define-key *textmate-mode-map* [(meta shift l)]         'nil)
(define-key *textmate-mode-map* [(meta shift t)]         'nil)
(define-key *textmate-mode-map* [(alt up)]               'nil)
(define-key *textmate-mode-map* [(alt down)]             'nil)
(define-key *textmate-mode-map* [(alt shift up)]         'nil)
(define-key *textmate-mode-map* [(alt shift down)]       'nil)

(define-key *textmate-mode-map* [(super meta t)]      'textmate-clear-cache)
(define-key *textmate-mode-map* [(super meta \])]     'align)
(define-key *textmate-mode-map* [(super meta \[)]     'indent-according-to-mode)
(define-key *textmate-mode-map* [(super \])]          'textmate-shift-right)
(define-key *textmate-mode-map* [(super \[)]          'textmate-shift-left)
(define-key *textmate-mode-map* [(super shift l)]     'textmate-select-line)
(define-key *textmate-mode-map* [(meta up)]           'textmate-column-up)
(define-key *textmate-mode-map* [(meta down)]         'textmate-column-down)
(define-key *textmate-mode-map* [(meta shift up)]     'textmate-column-up-with-select)
(define-key *textmate-mode-map* [(meta shift down)]   'textmate-column-down-with-select)
