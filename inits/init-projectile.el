(projectile-global-mode)

(add-hook 'projectile-switch-project-hook 'update-projectile-dirtree)

(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
