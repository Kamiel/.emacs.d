(defun helm-C-x-b ()
  "This command is designed to be a drop-in replacement for switch to buffer.."
  (interactive)
  ;; (require 'helm-files)
  ;; (require 'helm-command)
  (helm
   :sources (append '(
                      ;; helm-c-source-ffap-line
                      ;; helm-c-source-ffap-guesser
                      helm-source-buffers-list
                      ;; helm-c-source-session
                      helm-c-source-files-in-current-dir
                      helm-c-source-recentf
                      helm-c-source-bookmarks
                      ;; helm-c-source-cmd-t
                      helm-c-source-locate
                      helm-c-source-buffer-not-found
                      ;; helm-M-x
                      )
                    (if (boundp 'helm-c-source-ls-git)
                        '(helm-c-source-ls-git)
                      nil))
   :candidate-number-limit 32
   :buffer "*helm-cmd-t:*"))

(global-unset-key (kbd "s-t"))
(global-set-key   (kbd "s-t")   'helm-C-x-b)
(global-set-key   (kbd "C-c b") 'helm-C-x-b)
(global-unset-key (kbd "s-T"))
(global-set-key   (kbd "s-T")   'helm-cmd-t)
(global-set-key   (kbd "C-c f") 'helm-cmd-t)

;; fix collision with textmate-mode
(eval-after-load 'textmate
  '(progn
     (define-key *textmate-mode-map* (kbd "s-t") 'helm-C-x-b)
     (define-key *textmate-mode-map* (kbd "s-T") 'helm-cmd-t)))
