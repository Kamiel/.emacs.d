;; kill speedbar window if killed parent frame
(defadvice delete-other-windows (after my-sr-speedbar-delete-other-window-advice activate)
  "Check whether we are in speedbar, if it is, jump to next window."
  (when (and (sr-speedbar-window-exist-p sr-speedbar-window)
			 (eq sr-speedbar-window (selected-window)))
	(other-window 1)))

(ad-enable-advice 'delete-other-windows 'after 'my-sr-speedbar-delete-other-window-advice)
(ad-activate 'delete-other-windows)
