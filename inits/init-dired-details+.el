(setq-default dired-omit-mode t)
;; toggle omit mode C-o
(define-key dired-mode-map (kbd "C-o") 'dired-omit-mode)
