(defun init-rsense-hook ()
	(setq ac-sources (append '(ac-source-rsense-method
							   ac-source-rsense-constant)
							 ac-sources))))

(add-hook 'ruby-mode-hook 'init-rsense-hook)
