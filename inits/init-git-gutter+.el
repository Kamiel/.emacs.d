(defun git-gutter+-mode-on ()
  (interactive)
  (global-git-gutter+-mode 1))

(add-hook 'after-init-hook 'git-gutter+-mode-on)

(global-set-key (kbd "C-x g") 'git-gutter+-mode)
(global-set-key (kbd "C-x G") 'global-git-gutter+-mode)

(eval-after-load 'git-gutter+
  '(progn
     (define-key git-gutter+-mode-map (kbd "C-x v *") 'git-gutter+-show-hunk)
     (define-key git-gutter+-mode-map (kbd "C-x p")   'git-gutter+-previous-hunk)
     (define-key git-gutter+-mode-map (kbd "C-x n")   'git-gutter+-next-hunk)
     (define-key git-gutter+-mode-map (kbd "C-x v s") 'git-gutter+-stage-hunks)
     (define-key git-gutter+-mode-map (kbd "C-x v r") 'git-gutter+-revert-hunks)
     (define-key git-gutter+-mode-map (kbd "C-x c")   'git-gutter+-commit)
     (define-key git-gutter+-mode-map (kbd "C-x C")   'git-gutter+-stage-and-commit)
     (define-key git-gutter+-mode-map (kbd "C-x C-y") 'git-gutter+-stage-and-commit-whole-buffer)
     (define-key git-gutter+-mode-map (kbd "C-x U")   'git-gutter+-unstage-whole-buffer)))
