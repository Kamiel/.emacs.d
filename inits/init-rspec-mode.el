(eval-after-load 'yasnippet
  '(eval-after-load 'rspec-mode
     '(rspec-install-snippets)))

;; fix zsh vs rvm compatibility
(defadvice rspec-compile (around rspec-compile-around)
  "Use BASH shell for running the specs because of ZSH issues."
  (let ((shell-file-name "/bin/bash"))
    ad-do-it))
(ad-activate 'rspec-compile)

(add-hook 'ruby-mode-hook '(lambda ()
                             (when (and buffer-file-name (string-match "_spec.rb$" buffer-file-name))
                               (rspec-mode))))
