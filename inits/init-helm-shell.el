(add-hook 'shell-mode-hook 'pcomplete-shell-setup)
(add-hook 'shell-mode-hook
          (lambda ()
			(define-key shell-mode-map [remap pcomplete] 'helm-shell-pcomplete)))
