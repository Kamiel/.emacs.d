(global-undo-tree-mode)

(defalias 'redo 'undo-tree-redo)
(global-set-key (kbd "s-z") 'undo)   ; 【⌘Z】
(global-set-key (kbd "s-S-z") 'redo) ; 【⌘⇧Z】
(global-set-key (kbd "S-s-z") 'redo) ; 【⌘⇧Z】
(global-set-key (kbd "s-Z") 'redo)   ; 【⌘⇧Z】
