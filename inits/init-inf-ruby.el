(add-hook 'after-init-hook 'inf-ruby-switch-setup)

(eval-after-load 'rspec-mode
  '(eval-after-load 'yasnippet
     '(rspec-install-snippets)))
