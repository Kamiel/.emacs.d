(add-hook 'python-mode-hook 'pipenv-mode)
(eval-after-load 'pipenv
  '(define-key pipenv-mode-map (kbd "C-c C-p p") 'run-python))
