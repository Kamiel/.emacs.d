(defadvice terminal-init-screen
    ;; The advice is named `xterm-extras', and is run before `terminal-init-screen' runs.
    (before xterm-extras activate)
  ;; Docstring.  This describes the advice and is made available inside emacs;
  ;; for example when doing C-h f terminal-init-screen RET
  "Enable `xterm-extras' package features."
  ;; This is the elisp code that is run before `terminal-init-screen'.
  ;; (when (string-match "^xterm" (getenv "TERM")
  ;; Use the xterm color initialization code
  (xterm-extra-keys))
