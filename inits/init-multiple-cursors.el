(global-set-key (kbd "C-|")  'mc/edit-lines)
(global-set-key (kbd "s-\\") 'mc/edit-lines)
(eval-after-load 'objc-mode
  '(progn
     (define-key objc-mode-map (kbd "C-|") 'mc/edit-lines)
     (define-key ruby-mode-map (kbd "C-|") 'mc/edit-lines)))
;; TODO: load on mode hook
;; (define-key python-mode-map (kbd "C-c C-c") 'mc/edit-lines)

(setq lobal-set-keylobal-set-keymc/list-file (concat
                    (file-name-as-directory user-emacs-directory) "mc-lists.el"))

(global-set-key (kbd "C-{") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-}") 'mc/mark-next-like-this)

(global-unset-key (kbd "M-<down-mouse-1>"))
(global-set-key (kbd "M-<mouse-1>") 'mc/add-cursor-on-click)
(global-set-key (kbd "s-<mouse-1>") 'mc/add-cursor-on-click)

(defvar mc-mode-map (make-sparse-keymap)
  "Keymap for `multiple-cursors'.")
(global-set-key         (kbd "C-$")   'mc-mode-map)
(define-key mc-mode-map (kbd "a")     'mc/mark-all-like-this)
(define-key mc-mode-map (kbd "m")     'mc/mark-more-like-this-extended)
(define-key mc-mode-map (kbd "+")     'mc/mark-more-like-this-extended)
(define-key mc-mode-map (kbd "r")     'mc/mark-all-in-region)
;; From active region to multiple cursors:
(define-key mc-mode-map (kbd "C-e")   'mc/edit-ends-of-lines)
(define-key mc-mode-map (kbd "C-a")   'mc/edit-beginnings-of-lines)
