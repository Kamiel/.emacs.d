(global-set-key (kbd "C-ESC") 'ace-window)
(global-set-key (kbd "C-<escape>") 'ace-window)
(global-set-key (kbd "<escape> <escape> g") 'ace-window)
(global-set-key (kbd "ESC ESC g") 'ace-window)
(eval-after-load 'ace-window
  '(setq aw-keys '(?i ?d ?u ?h ?e ?t ?o ?n ?a ?s)))
