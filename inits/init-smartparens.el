;; (require 'smartparens)
;; (require 'smartparens-config)
;; (require 'smartparens-ruby)

(eval-after-load 'smartparens
  '(progn
     (require 'smartparens-config)
     (define-key smartparens-mode-map (kbd "C-M-<down>") 'sp-down-sexp)
     (define-key smartparens-mode-map (kbd "C-M-<up>") 'sp-up-sexp)
     (define-key smartparens-mode-map (kbd "C-M-d") 'sp-down-sexp)
     (define-key smartparens-mode-map (kbd "C-M-u") 'sp-up-sexp)))

;; ("C-M-k" . sp-kill-sexp-with-a-twist-of-lime)
;; ("C-M-f" . sp-forward-sexp)
;; ("C-M-b" . sp-backward-sexp)
;; ("C-M-n" . sp-up-sexp)
;; ("C-M-d" . sp-down-sexp)
;; ("C-M-u" . sp-backward-up-sexp)
;; ("C-M-p" . sp-backward-down-sexp)
;; ("C-M-w" . sp-copy-sexp)
;; ("M-s" . sp-splice-sexp)
;; ("M-r" . sp-splice-sexp-killing-around)
;; ("C-)" . sp-forward-slurp-sexp)
;; ("C-}" . sp-forward-barf-sexp)
;; ("C-(" . sp-backward-slurp-sexp)
;; ("C-{" . sp-backward-barf-sexp)
;; ("M-S" . sp-split-sexp)
;; ("M-J" . sp-join-sexp)
;; ("C-M-t" . sp-transpose-sexp)

;; (eval-after-load 'ruby-mode
;;   '(require 'smartparens-ruby))
