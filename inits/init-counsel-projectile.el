(counsel-projectile-mode 1)

(eval-after-load 'swiper
  '(progn
     (global-set-key (kbd "s-f") 'counsel-projectile-find-file)
     (global-set-key (kbd "C-x s-F") 'counsel-projectile-ag)
     (global-set-key (kbd "s-F")     'counsel-projectile-ag)))
