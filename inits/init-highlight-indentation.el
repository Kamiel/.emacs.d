(defun highlight-indentation-current-column-mode-on ()
  "Turn on `highlight-indentation-current-column-mode' mode."
  (interactive)
  (highlight-indentation-current-column-mode 1))

;; (add-hook 'ruby-mode-hook 'highlight-indentation-current-column-mode-on)

;; (eval-after-load 'haskell-mode
;;   '(add-hook 'haskell-mode-hook 'highlight-indentation-current-column-mode-on))

(add-hook 'prog-mode-hook 'highlight-indentation-current-column-mode-on)
