# The EMACS configuration directory
+ lightweight
+ modular
+ full-featured
***
## configured for
 * Python for Data Science
 * Clojure development
 * Common Lisp hacking
 * Ruby hacking
 * Ruby on Rails development
 * Clang Objective-C iOS development
 * SuperCollider algorithmic audiosynthesis
 * Haskell hacking
***
 ![Screenshot](http://i.imgur.com/0IvEYcP.png)

## References
### used fonts
- [Inconsolata LGC](https://github.com/MihailJP/Inconsolata-LGC#readme)
- [Source Code Pro](https://github.com/adobe-fonts/source-code-pro#readme)
- [Source Sans Pro](https://github.com/adobe-fonts/source-sans-pro#readme)

TODO: Add to README required system packages.
